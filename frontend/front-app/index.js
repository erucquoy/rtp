import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import { Provider } from 'react-redux';
import { createStore, compose, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';

import { loginJWTUser } from './actions';

import RTPApp from './reducers';

import MainContainer from './containers/layout/MainContainer';
import HomeContainer from './containers/HomeContainer';
import TemoignageContainer from './containers/TemoignageContainer';
import ServiceContainer from './containers/ServiceContainer';
import OffreContainer from './containers/OffreContainer';
import ArticleContainer from './containers/ArticleContainer';
import LoginContainer from './containers/LoginContainer';
import RegisterContainer from './containers/RegisterContainer';
import DashBoardContainer from './containers/DashBoardContainer';
import SchoolContainer from './containers/SchoolContainer';
import AddLessonContainer from './containers/AddLessonContainer';
import MonitorContainer from './containers/MonitorContainer';
import LessonContainer from './containers/LessonContainer';


import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';

import 'jquery';
import 'bootstrap';

import './lib/gridline.min';

import './style/bootstrap_custom.scss';
import './style/style.scss';

const finalCreateStore = compose(
  applyMiddleware(thunkMiddleware),
  window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore);

let store = finalCreateStore(RTPApp);

if (localStorage.getItem('tokenJWT') != undefined) {
  store.dispatch(loginJWTUser(localStorage.getItem('tokenJWT')));
}

const App = () => (
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={MainContainer}>
        <IndexRoute component={HomeContainer} />
        <Route path="/temoignages" component={TemoignageContainer} />
        <Route path="/services" component={ServiceContainer} />
        <Route path="/offres" component={OffreContainer} />
        <Route path="/articles" component={ArticleContainer} />
        <Route path="/connexion" component={LoginContainer} />
        <Route path="/inscription" component={RegisterContainer} />
        <Route path="/tableau(/:id)" component={DashBoardContainer} />
        <Route path="/ecole" component={SchoolContainer} />
        <Route path="/ecole/lecon/ajout" component={AddLessonContainer} />
        <Route path="/ecole/lecon/edition/:id(/:return)" component={AddLessonContainer} />
        <Route path="/moniteur" component={MonitorContainer} />
        <Route path="/moniteur/lecon/:id" component={LessonContainer} />
      </Route>
    </Router>
  </Provider>
);

ReactDOM.render(<App />, document.getElementById('app-rtp'));
