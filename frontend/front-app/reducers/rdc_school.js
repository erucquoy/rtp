import {
  RECEIVE_DATA_SCHOOL,
  REQUEST_DATA_SCHOOL,
  RECEIVE_ERROR_SCHOOL,
} from '../actions/act_school';

const school = (state = { isFetching: false, data: {} }, action = {}) => {
  switch (action.type) {
    case REQUEST_DATA_SCHOOL:
      return {
        ...state,
        isFetching: true,
      };
    case RECEIVE_DATA_SCHOOL:
      return {
        ...state,
        isFetching: false,
        data: action.payload,
        error: '',
      };
    case RECEIVE_ERROR_SCHOOL:
      return {
        ...state,
        isFetching: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default school;
