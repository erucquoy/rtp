import { combineReducers } from 'redux';
import ui from './rdc_main_ui';
import user from './rdc_user';
import temoignage from './rdc_temoignage';
import article from './rdc_article';
import lesson from './rdc_lesson';
import school from './rdc_school';
import { reducer as formReducer } from 'redux-form';

const RTPApp = combineReducers({
  ui,
  user,
  temoignage,
  article,
  lesson,
  school,
  form: formReducer,
});

export default RTPApp;
