import {
  RECEIVE_DATA_LESSON,
  REQUEST_DATA_LESSON,
  RECEIVE_ERROR_LESSON,
  RECEIVE_SELECTED_LESSON,
} from '../actions/act_lesson';

const lesson = (state = { isFetching: false, data: {}, error: [] }, action = {}) => {
  switch (action.type) {
    case REQUEST_DATA_LESSON:
      return {
        ...state,
        isFetching: true,
      };
    case RECEIVE_DATA_LESSON:
      return {
        ...state,
        isFetching: false,
        data: action.payload,
      };
    case RECEIVE_ERROR_LESSON:
      return {
        ...state,
        isFetching: false,
        error: action.payload,
      };
    case RECEIVE_SELECTED_LESSON:
      return {
        ...state,
        isFetching: false,
        selected: action.payload,
      };
    default:
      return state;
  }
};

export default lesson;
