import {
  RECEIVE_DATA_TEMOIGNAGE,
  REQUEST_DATA_TEMOIGNAGE,
} from '../actions/act_temoignage';

const temoignage = (state = { isFetching: false, data: {} }, action = {}) => {
  switch (action.type) {
    case REQUEST_DATA_TEMOIGNAGE:
      return {
        ...state,
        isFetching: true,
      };
    case RECEIVE_DATA_TEMOIGNAGE:
      return {
        ...state,
        isFetching: false,
        data: action.payload,
      };
    default:
      return state;
  }
};

export default temoignage;
