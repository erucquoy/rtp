import {
  RECEIVE_DATA_ARTICLE,
  REQUEST_DATA_ARTICLE,
} from '../actions/act_article';

const article = (state = { isFetching: false, data: {} }, action = {}) => {
  switch (action.type) {
    case REQUEST_DATA_ARTICLE:
      return {
        ...state,
        isFetching: true,
      };
    case RECEIVE_DATA_ARTICLE:
      return {
        ...state,
        isFetching: false,
        data: action.payload,
      };
    default:
      return state;
  }
};

export default article;
