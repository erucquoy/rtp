import {
  RECEIVE_DATA_PAGE,
  REQUEST_DATA_PAGE,
  SET_PAGE_TITLE,
} from '../actions/act_main_ui';

const ui = (state = { isFetching: false, page: {} }, action = {}) => {
  switch (action.type) {
    case REQUEST_DATA_PAGE:
      return {
        ...state,
        isFetching: true,
      };
    case RECEIVE_DATA_PAGE:
      return {
        ...state,
        isFetching: false,
        page: action.payload,
      };
    case SET_PAGE_TITLE:
      return {
        ...state,
        pageTitle: action.payload,
      };
    default:
      return state;
  }
};

export default ui;
