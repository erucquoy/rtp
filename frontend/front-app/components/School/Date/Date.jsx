import React, { Component, PropTypes } from 'react';
import { DateField, DatePicker } from 'react-date-picker';
import moment from 'moment';
import { Field } from 'redux-form';

import 'react-date-picker/index.css';

class Date extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(date, { dateMoment }) {
    this.props.input.onChange(dateMoment.toISOString());
  }

  render() {
    if (this.props.input.value == '') {
      this.props.input.value = new Date();
    }

    return (
      <div>
        <DateField
          dateFormat="YYYY-MM-DD HH:mm:ss"
          defaultValue={moment(this.props.input.value).valueOf()}
          onChange={this.handleChange}
        >
          <DatePicker
            {...this.props.input}
            locale="fr"
            highlightToday
            weekNumbers
            weekStartDay={0}
          />
        </DateField>
        {/* <DatePicker
          {...input}
          className="form-control"
          id={`input${this.props.name}`}
          placeholder={placeholder}
          dateFormat="YYYY-MM-DD"
          selected={input.value ? moment(input.value, 'YYYY-MM-DD') : null}
          onChange={this.handleChange}
          disabled={this.props.disabled}
          required={this.props.required}
        />
        {touched && error && <span>{error}</span>}*/}
      </div>
    );
  }
}

Date.propTypes = {
  input: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
  }).isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.bool,
  }),
  placeholder: PropTypes.string,
  name: PropTypes.string,
  display: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
};

Date.defaultProps = {
  placeholder: '',
};

const DateTime = ({ name, display, disabled }) => (
  <div className="form-group is-empty">
    <label htmlFor={`input${name}`} className="control-label">{display}</label>
    <Field
      component={Date}
      name={name}
      type="text"
      id={`input${name}`}
      required
      disabled={disabled}
    />
  </div>
);

DateTime.propTypes = {
  name: PropTypes.string,
  display: PropTypes.string,
  disabled: PropTypes.bool,
};

export default DateTime;
