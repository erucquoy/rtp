import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';

import 'react-big-calendar/lib/css/react-big-calendar.css';

import userImg from '../../../img/laura.png';

BigCalendar.momentLocalizer(moment);

const Lessons = ({ user, events, handlerSelect, onChangeMonth }) => (
  <div className="container-fluid sec1">
    <div className="row" data-columns="14">
      <div className="col-xs-3 col-xs-offset-1 blueDiv">
        <br />
        <center><img className="img-circle img-responsive" width="125" src={userImg} alt="avatar" /></center>
        <br />
        <b>Informations personelles</b><br /><br />
        Nom: {user.profile.lastName}<br /><br />
        Prénom: {user.profile.firstName}<br /><br />
        Auto-école: {user.school ? user.school.name : ''}<br /><br />
      </div>

      <div className="col-xs-6 ">
        <div className="row">
          {user && user.drivingSchool ? <Link to="/ecole/lecon/ajout" className="btn btn-success pull-right">Ajouter une leçon</Link> : ''}
        </div>
        <br />
        <div className="calendar">
          <BigCalendar
            events={events}
            culture="fr"
            startAccessor="start"
            endAccessor="end"
            onSelectEvent={handlerSelect}
            onNavigate={onChangeMonth}
            messages={{
              next: 'Suivant',
              previous: 'Précédent',
              today: 'Aujourd\'hui',
              month: 'Mois',
              week: 'Semaine',
              day: 'Jour',
              agenda: 'Mon planning',
            }}
          />
        </div>
      </div>
    </div>

    <br />

  </div>
);

Lessons.propTypes = {
  user: PropTypes.object,
  events: PropTypes.array,
  handlerSelect: PropTypes.func,
  onChangeMonth: PropTypes.func,
};

export default Lessons;

