import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import '../../../style/client.scss';

const Student = ({ students }) => (
  <div className="container-fluid sec2">
    <div className="row">
      <div className="col-xs-10 col-xs-offset-1">
        <div className="table-responsive">
          <br />
          <br />
          <table className="table tbl-db table-bordered">
            <tbody>
              <tr>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Adresse e-mail</th>
                <th>Action</th>
              </tr>

              {students.map((t, i) => (
                <tr key={`student${i}`}>
                  <td>{t.profile.lastName}</td>
                  <td>{t.profile.firstName}</td>
                  <td>{t.email}</td>
                  <td><Link to={`/moniteur/lecon/${t['_id']}`}>Voir les rapports</Link></td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
);

Student.propTypes = {
  students: PropTypes.array,
};

export default Student;

