import React, { PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form';
import Date from '../Date';

const AddLecon = (props) => (
  <div className="container-fluid signUpBG">
    <form onSubmit={props.handleSubmit(props.onSubmitHandler)}>
      <div className="signup-panel signup-panel-pad ">

        <center>
          <h1>Leçon</h1>
        </center>

        {(() => {
          if (props.lesson.error && props.lesson.error.length > 0) {
            return (
              <div className="alert alert-danger">
                <ul>
                  {props.lesson.error.map((t, i) => (<li key={`error${i}`}>{t.msg}</li>))}
                </ul>
              </div>
            );
          }
          return (<div></div>);
        })()}

        {(() => {
          if (props.user && props.user.drivingSchool && props.edit) {
            return (
              <div className="row">
                <button className="btn btn-danger pull-right" onClick={props.handleDelete}>
                  Supprimer définitivement
                </button>
              </div>
            );
          }
          return (<div></div>);
        })()}

        <br />
        <br />

        <Field
          component="input"
          name="school"
          type="hidden"
          required
          autoFocus
        />


        <label htmlFor="inputtime" className="control-label">Durée (en h)</label>
        <Field
          component="input"
          name="time"
          type="number"
          className="form-control"
          id="inputtime"
          placeholder="Durée (en h)"
          step="0.01"
          required
          autoFocus
        />

        <br />


        <Date name="date" display="Date" disabled={false} required={false} />

        <br />

        <div className="form-group is-empty">
          <label htmlFor="inputmonitor" className="control-label">Moniteur</label>
          <Field
            component="select"
            name="monitor"
            className="form-control"
            id="inputmonitor"
            disabled={!(props.user && props.user.drivingSchool)}
            required
          >
            {props.school.monitors.map((t, i) => (<option value={t['_id']} key={`monitor${i}`}>{`${t.profile.firstName} ${t.profile.lastName}`}</option>))}
          </Field>
        </div>

        <br />

        <div className="form-group is-empty">
          <label htmlFor="inputstudent" className="control-label">Elève</label>
          <Field
            component="select"
            name="student"
            className="form-control"
            id="inputstudent"
            disabled={!(props.user && props.user.drivingSchool)}
            required
          >
            {props.school.students.map((t, i) => (<option value={t['_id']} key={`monitor${i}`}>{`${t.profile.firstName} ${t.profile.lastName}`}</option>))}
          </Field>
        </div>

        <br />

        <label htmlFor="inputmark" className="control-label">Note</label>
        <Field
          component="input"
          name="mark"
          type="number"
          className="form-control"
          id="inputmark"
          placeholder="Note"
          min="0"
          max="5"
        />

        <br />

        <label htmlFor="inputcontent" className="control-label">Commentaire</label>
        <Field
          component="textarea"
          name="content"
          type="text"
          className="form-control"
          id="inputcontent"
          placeholder="Commentaire"
        />

        <br />

        <label htmlFor="inputachievements" className="control-label">Acquis (un par ligne)</label>
        <Field
          component="textarea"
          name="achievements"
          type="text"
          className="form-control"
          id="inputachievements"
          placeholder="Acquis (un par ligne)"
        />

        <br />

        <label htmlFor="inputtowork" className="control-label">A retravaillé (un par ligne)</label>
        <Field
          component="textarea"
          name="toWork"
          type="text"
          className="form-control"
          id="inputtowork"
          placeholder="A retravaillé (un par ligne)"
        />

        <br /><br />

        <center>
          <button type="submit" className="btn btnSignUp btnSQGreen">Valider</button>
        </center>

        <br /><br />

      </div>
    </form>
  </div>
);

AddLecon.propTypes = {
  handleFacebook: PropTypes.func,
  slug: PropTypes.string,
  handleSubmit: PropTypes.func,
  onSubmitHandler: PropTypes.func,
  user: PropTypes.object,
  fields: PropTypes.array,
  lesson: PropTypes.object,
  school: PropTypes.object,
  edit: PropTypes.bool,
  handleDelete: PropTypes.func,
};

export default reduxForm({
  fields: ['school', 'time', 'date', 'monitor', 'student', 'mark', 'content', 'achievements', 'toWork'],
  form: 'addlecon',
})(AddLecon);

