import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import moment from 'moment';

import '../../../style/client.scss';

const computeStar = (mark) => {
  if (mark == undefined || mark == '') {
    return 'Non noté';
  }

  const stars = [];

  for (let i = 0; i < 5; i++) {
    if (i < mark) {
      stars.push(<i className="fa fa-star" ariaHidden="true"></i>);
    } else {
      stars.push(<i className="fa fa-star-o" ariaHidden="true"></i>);
    }
  }

  return stars;
};

function computeMinutes(n) {
  return n > 9 ? `${n}` : `0${n}`;
}

function computeToWork(lessons) {
  const toWork = [];

  for (let i = 0; i < lessons.length && toWork.length < 3; i++) {
    if (typeof lessons[i].toWork == 'object') {
      lessons[i].toWork = lessons[i].toWork[0].split('\n');
      for (let j = 0; j < lessons[i].toWork.length && toWork.length < 3; j++) {
        if (lessons[i].toWork[j].length > 0 && toWork.indexOf(lessons[i].toWork[j]) == -1) {
          toWork.push(lessons[i].toWork[j]);
        }
      }
    }
  }

  return toWork;
}

function computeAchievements(lessons) {
  const achievements = [];

  for (let i = 0; i < lessons.length && achievements.length < 3; i++) {
    if (typeof lessons[i].achievements == 'object') {
      lessons[i].achievements = lessons[i].achievements[0].split('\n');
      for (let j = 0; j < lessons[i].achievements.length && achievements.length < 3; j++) {
        if (lessons[i].achievements[j].length > 0 && achievements.indexOf(lessons[i].achievements[j]) == -1) {
          achievements.push(lessons[i].achievements[j]);
        }
      }
    }
  }

  return achievements;
}

const Lesson = ({ lessons, notAdmin }) => (
  <div className="container-fluid sec2">
    <div className="row">
      <div className="col-xs-8 col-xs-offset-2">
        <div className="row">
          <div className="col-xs-5 nbpanel nbpanel-green nbpanel-bigheader">
            <div className="panel-header">Acquis</div>
            <div className="panel-body">
              <ul className="text-left">
                {computeAchievements(lessons).map((t, i) => (<li key={`toWork${i}`}>{t}</li>))}
              </ul>
            </div>
          </div>
          <div className="col-xs-5 col-xs-offset-2 nbpanel nbpanel-red nbpanel-bigheader">
            <div className="panel-header">À retravailler</div>
            <div className="panel-body">
              <ul className="text-left">
                {computeToWork(lessons).map((t, i) => (<li key={`toWork${i}`}>{t}</li>))}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="row">
      <div className="col-xs-10 col-xs-offset-1">
        <div className="table-responsive">
          <br />
          <br />
          <table className="table tbl-db table-bordered">
            <tbody>
              <tr>
                <th>Date</th>
                <th>Durée</th>
                <th>Moniteur</th>
                <th>Évaluation globale</th>
                {notAdmin ? '' : <th>Action</th>}
              </tr>

              {lessons.map((t, i) => (
                <tr key={`student${i}`}>
                  <td>{moment(t.date).format('DD-MM-YYYY HH:mm:ss')}</td>
                  <td>{Math.floor(t.time)}h{computeMinutes(Math.round((t.time - Math.floor(t.time)) * 60))}</td>
                  <td>{`${t.monitor.profile.lastName} ${t.monitor.profile.firstName}`}</td>
                  <td>{computeStar(t.mark)}</td>
                  {notAdmin ? '' : <td><Link to={`/ecole/lecon/edition/${t['_id']}/!moniteur!lecon!${t.student}`}>Voir le rapport</Link></td>}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
);

Lesson.propTypes = {
  lessons: PropTypes.array,
  notAdmin: PropTypes.bool,
};

export default Lesson;

