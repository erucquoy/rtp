import React, { PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form';

import '../../../style/client.scss';

const Monitor = ({ monitors, handleDeleteMonitor, handleSubmit, handleAddMonitor, errorDisplay }) => (
  <div className="container-fluid sec2">
    <div className="row">
      <div className="col-xs-10 col-xs-offset-1">
        <div className="table-responsive">
          <form onSubmit={handleSubmit(handleAddMonitor)}>
            {(() => {
              if (errorDisplay && errorDisplay.length > 0) {
                return (
                  <div className="alert alert-danger">
                      {errorDisplay}
                  </div>
                );
              }
              return (<div></div>);
            })()}
            <div className="form-group is-empty form-monitor-add">
              <label htmlFor="inputemail" className="control-label">Adresse e-mail de moniteur à ajouter</label>
              <Field
                component="input"
                name="email"
                type="text"
                className="form-control"
                id="inputemail"
                placeholder="Adresse e-mail de moniteur à ajouter"
                required
              />
              <button type="submit" className="btn">Ajouter</button>
            </div>
          </form>
          <br />
          <br />
          <table className="table tbl-db table-bordered">
            <tbody>
              <tr>
                <th>Nom</th>
                <th>Prénom</th>
                <th>Adresse e-mail</th>
                <th>Action</th>
              </tr>

              {monitors.map((t, i) => (
                <tr key={`monitor${i}`}>
                  <td>{t.profile.lastName}</td>
                  <td>{t.profile.firstName}</td>
                  <td>{t.email}</td>
                  <td><a onClick={() => handleDeleteMonitor(t['_id'])}>Supprimer comme moniteur</a></td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
);

Monitor.propTypes = {
  monitors: PropTypes.array,
  handleDeleteMonitor: PropTypes.func,
  handleSubmit: PropTypes.func,
  handleAddMonitor: PropTypes.func,
  errorDisplay: PropTypes.string,
};

export default reduxForm({
  fields: ['email'],
  form: 'addmonitor',
})(Monitor);

