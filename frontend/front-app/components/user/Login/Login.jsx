import React, { PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form';

const Login = (props) => (
  <div className="container-fluid signUpBG">
    <form onSubmit={props.handleSubmit(props.onSubmitHandler)}>
      <div className="signup-panel signup-panel-pad ">

        <center>
          <h1>Connexion</h1>
        </center>

        {(() => {
          if (props.user && props.user.fail) {
            return (
              <div className="alert alert-danger">
                <p>{props.user.message}</p>
              </div>
            );
          }
          return (<div></div>);
        })()}

        <br />
        <br />

        <label htmlFor="inputEmail" className="sr-only">Email</label>
        <Field
          component="input"
          name="email"
          type="text"
          className="form-control"
          id="inputEmail"
          placeholder="E-mail"
          required
          autoFocus
        />
        <br />

        <label htmlFor="inputPassword" className="sr-only">Mot de passe</label>
        <Field
          component="input"
          name="password"
          type="password"
          className="form-control"
          id="inputPassword"
          placeholder="Mot de passe"
          required
        />

        <br /><br />

        <center>
          <button type="submit" className="btn btnSignUp btnSQGreen">Connexion</button>
        </center>

        <br /><br />

      </div>
    </form>
  </div>
);

Login.propTypes = {
  handleFacebook: PropTypes.func,
  slug: PropTypes.string,
  handleSubmit: PropTypes.func,
  onSubmitHandler: PropTypes.func,
  user: PropTypes.object,
  fields: PropTypes.array,
};

export default reduxForm({
  fields: ['email', 'password'],
  form: 'login',
})(Login);

