import React, { PropTypes } from 'react';

const Budget = ({}) => (
  <div className="container-fluid sec3">
    <div className="row" data-columns="14">
      <div className="col-xs-3 col-xs-offset-1 blueDiv">
        <br />
        <center><img className="img-circle img-responsive" width="125" src="./assets/img/laura.png"/></center>
        <br />
        <b>Informations personelles</b><br /><br />
        Nom: Road<br /><br />
        Prénom: Laura<br /><br />
        Auto-école: SEPEFREI<br /><br />
      </div>
      <div className="col-xs-6 ">
        <center className="fontSize">Prochaines heures de conduite			<br /><br /><br />
          CALENDRIER
        </center>
      </div>
      <div className="col-xs-3 ">
        <h3>
          &nbsp;
        </h3>
        <div className="nbpanel nbpanel-grey">
          <div className="panel-header">Nombre d'heures de conduite</div>
          <div className="panel-body">12</div>
        </div>
        <br />
        <h3>
          &nbsp;
        </h3>
        <div className="nbpanel nbpanel-red">
          <div className="panel-header">Jours restants avant l'examen</div>
          <div className="panel-body">122</div>
        </div>
      </div>
    </div>
    <br />
    <div className="row">
      <br />
      <div className="blueDiv container-fluid servicesHeader">
        <center>
          <h2>Dernières leçons de conduite</h2>
        </center>
      </div>
      <div className="container">
        <div className="row" data-columns="22">
          <div className="col-xs-1 col-xs-offset-2">
            <i className="fa fa-caret-left caret-left" aria-hidden="true"></i>
          </div>
          <div className="col-xs-4">
            <div className="dblesson dbl-green">
              <div className="dblesson-title">
                20/07/2017 <span className="dblesson-timer">1h30</span>
              </div>
              <div className="dblesson-name"><i className="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Mô Niteur</div>
              <div className="dblesson-lowpoints">À retravailler</div>
              <div className="dblesson-lowpoints-lst">
                <ul>
                  <li>Point 1</li>
                  <li>Point 1</li>
                  <li>Point 1</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="col-xs-4 col-xd-offset-2">
            <div className="dblesson dbl-orange">
              <div className="dblesson-title">
                20/07/2017 <span className="dblesson-timer">1h30</span>
              </div>
              <div className="dblesson-name"><i className="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Mô Niteur</div>
              <div className="dblesson-lowpoints">À retravailler</div>
              <div className="dblesson-lowpoints-lst">
                <ul>
                  <li>Point 1</li>
                  <li>Point 1</li>
                  <li>Point 1</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="col-xs-4 col-xd-offset-2">
            <div className="dblesson dbl-red">
              <div className="dblesson-title">
                20/07/2017 <span className="dblesson-timer">1h30</span>
              </div>
              <div className="dblesson-name"><i className="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Mô Niteur</div>
              <div className="dblesson-lowpoints">À retravailler</div>
              <div className="dblesson-lowpoints-lst">
                <ul>
                  <li>Point 1</li>
                  <li>Point 1</li>
                  <li>Point 1</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="col-xs-1">
            <i className="fa fa-caret-right caret-right" aria-hidden="true" ></i>
          </div>
        </div>
      </div>
    </div>
  </div>
);

Budget.propTypes = {
};

export default Budget;

