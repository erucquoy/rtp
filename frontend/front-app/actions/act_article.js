import axios from 'axios';
import { baseUrlApi } from '../config/config';

export const REQUEST_DATA_ARTICLE = 'REQUEST_DATA_ARTICLE';
export const RECEIVE_DATA_ARTICLE = 'RECEIVE_DATA_ARTICLE';

export const requestData = () => ({
  type: REQUEST_DATA_ARTICLE,
});

export const receiveData = (data) => ({
  type: RECEIVE_DATA_ARTICLE,
  payload: data,
});

export const getArticle = () => (
  dispatch => {
    dispatch(requestData());
    axios.get(
      `${baseUrlApi}article`
    ).then(response => {
      dispatch(receiveData({
        data: response.data.data,
      }));
    });
  }
);

