import axios from 'axios';
import { baseUrlApi } from '../config/config';

export const REQUEST_DATA_TEMOIGNAGE = 'REQUEST_DATA_TEMOIGNAGE';
export const RECEIVE_DATA_TEMOIGNAGE = 'RECEIVE_DATA_TEMOIGNAGE';

export const requestData = () => ({
  type: REQUEST_DATA_TEMOIGNAGE,
});

export const receiveData = (data) => ({
  type: RECEIVE_DATA_TEMOIGNAGE,
  payload: data,
});

export const getTemoignage = () => (
  dispatch => {
    dispatch(requestData());
    axios.get(
      `${baseUrlApi}temoignage`
    ).then(response => {
      dispatch(receiveData({
        data: response.data.data,
      }));
    });
  }
);

