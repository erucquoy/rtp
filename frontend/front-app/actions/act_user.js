import axios from 'axios';
import { baseUrlApi } from '../config/config';

export const LOG_IN = 'LOG_IN';
export const LOG_OUT = 'LOG_OUT';
export const REQUEST_USER_DATA = 'REQUEST_USER_DATA';
export const RECEIVE_USER_DATA = 'RECEIVE_USER_DATA';

export const logIn = () => ({
  type: LOG_IN,
});

export const logOut = () => ({
  type: LOG_OUT,
});

export const receiveUserData = (data) => ({
  type: RECEIVE_USER_DATA,
  payload: data,
});

export const isConnected = () => (
  dispatch => {
    axios.get(
      `${baseUrlApi}token`
    ).then(response => {
      if (response.error) {
        dispatch(receiveUserData({}));
      }
    }).catch(() => dispatch(receiveUserData({})));
  }
);

export const getUserData = () => (
  dispatch => {
    axios.get(
      `${baseUrlApi}user`
    ).then(response => {
      if (!response.data.error) {
        dispatch(receiveUserData(response.data.message));
      }
    }).catch(() => dispatch(receiveUserData({})));
  }
);

export const loginUser = (email, password) => (
  dispatch => {
    dispatch(logIn());

    axios.post(
      `${baseUrlApi}authenticate`,
      {
        email,
        password,
      }
    ).then(response => {
      if (response.data.error == false) {
        axios.interceptors.request.use((config) => {
          config.headers.Authorization = response.data.message.access_token;
          return config;
        });
        localStorage.setItem('tokenJWT', response.data.message.access_token);
        dispatch(receiveUserData({ fail: false, message: '' }));
        dispatch(getUserData());
      } else {
        dispatch(receiveUserData({
          fail: true,
          message: 'Mauvais identifiants',
        }));
      }
    }).catch(() =>
      dispatch(receiveUserData({
        fail: true,
        message: 'Mauvais identifiants',
      }))
    );
  }
);

export const logoutUser = () => (
  dispatch => {
    axios.interceptors.request.use((config) => {
      config.headers.Authorization = '';
      return config;
    });
    localStorage.removeItem('tokenJWT');
    dispatch(logOut());
  }
);

export const loginJWTUser = (jwtToken) => (
  dispatch => {
    dispatch(logIn());

    if (jwtToken != undefined) {
      axios.interceptors.request.use((config) => {
        config.headers.Authorization = localStorage.getItem('tokenJWT');
        return config;
      });
      dispatch(getUserData());
    }
  }
);

export const registerUser = (value) => (
  dispatch => {
    axios.post(
      `${baseUrlApi}account`,
      value
    ).then(response => {
      if (response.data.error == false) {
        dispatch(loginUser(value.email, value.password));
      } else {
        dispatch(receiveUserData({
          fail: true,
          message: 'Vérifiez les champs',
        }));
      }
    }).catch(() =>
      dispatch(receiveUserData({
        fail: true,
        message: 'Vérifiez les champs',
      }))
    );
  }
);
