import axios from 'axios';
import { baseUrlApi } from '../config/config';

export const REQUEST_DATA_LESSON = 'REQUEST_DATA_LESSON';
export const RECEIVE_DATA_LESSON = 'RECEIVE_DATA_LESSON';
export const RECEIVE_SELECTED_LESSON = 'RECEIVE_SELECTED_LESSON';
export const RECEIVE_ERROR_LESSON = 'RECEIVE_ERROR_LESSON';

export const requestData = () => ({
  type: REQUEST_DATA_LESSON,
});

export const receiveData = (data) => ({
  type: RECEIVE_DATA_LESSON,
  payload: data,
});

export const receiveSelected = (data) => ({
  type: RECEIVE_SELECTED_LESSON,
  payload: data,
});

export const receiveError = (data) => ({
  type: RECEIVE_ERROR_LESSON,
  payload: data,
});

export const getLessonUser = (id) => (
  dispatch => {
    dispatch(requestData());
    axios.get(
      `${baseUrlApi}fiche/user/${id}`
    ).then(response => {
      dispatch(receiveData({
        data: response.data.data,
      }));
    });
  }
);

export const getLesson = () => (
  dispatch => {
    dispatch(requestData());
    axios.get(
      `${baseUrlApi}fiche`
    ).then(response => {
      dispatch(receiveData({
        data: response.data.data,
      }));
    });
  }
);

export const getLessonId = (id) => (
  dispatch => {
    dispatch(requestData());
    axios.get(
      `${baseUrlApi}fiche/${id}`
    ).then(response => {
      dispatch(receiveSelected({
        data: response.data,
      }));
    });
  }
);

export const addLesson = (add, callback) => (
  dispatch => {
    console.log(add);
    axios.post(
      `${baseUrlApi}fiche`,
      {
        ...add,
      }
    ).then(() => {
      callback();
    }).catch((error) => {
      dispatch(receiveError(error.response.data.message));
    });
  }
);

export const updateLesson = (id, update, callback) => (
  () => {
    axios.put(
      `${baseUrlApi}fiche/${id}`,
      {
        ...update,
      }
    ).then(() => {
      callback();
    });
  }
);

export const deleteLesson = (id, callback) => (
  () => {
    axios.delete(
      `${baseUrlApi}fiche/${id}`
    ).then(() => {
      callback();
    });
  }
);
