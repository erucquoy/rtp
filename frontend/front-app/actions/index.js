import {
  setPageTitle,
  getPage,
} from './act_main_ui';

import {
  loginUser,
  loginJWTUser,
  logoutUser,
  isConnected,
  getUserData,
  registerUser,
} from './act_user';

import {
  getTemoignage,
} from './act_temoignage';

import {
  getArticle,
} from './act_article';

import {
  getLesson,
  getLessonId,
  getLessonUser,
  addLesson,
  updateLesson,
  deleteLesson,
} from './act_lesson';

import {
  getSchool,
  addMonitor,
  deleteMonitor,
} from './act_school';

export {
  setPageTitle,
  getPage,
  loginUser,
  loginJWTUser,
  logoutUser,
  isConnected,
  getUserData,
  getTemoignage,
  getArticle,
  getLesson,
  getLessonId,
  getLessonUser,
  addLesson,
  updateLesson,
  deleteLesson,
  getSchool,
  addMonitor,
  deleteMonitor,
  registerUser,
};
