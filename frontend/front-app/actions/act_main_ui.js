import axios from 'axios';
import { baseUrlApi } from '../config/config';

export const REQUEST_DATA_PAGE = 'REQUEST_DATA_PAGE';
export const RECEIVE_DATA_PAGE = 'RECEIVE_DATA_PAGE';
export const SET_PAGE_TITLE = 'SET_PAGE_TITLE';

export const setPageTitle = (pageTitle) => {
  document.title = pageTitle;

  return {
    type: SET_PAGE_TITLE,
    payload: pageTitle,
  };
};

export const requestData = () => ({
  type: REQUEST_DATA_PAGE,
});

export const receiveData = (data) => ({
  type: RECEIVE_DATA_PAGE,
  payload: data,
});

export const getPage = () => (
  dispatch => {
    dispatch(requestData());
    axios.get(
      `${baseUrlApi}page`
    ).then(response => {
      dispatch(receiveData({
        data: response.data.data,
      }));
    });
  }
);
