import axios from 'axios';
import { baseUrlApi } from '../config/config';

export const REQUEST_DATA_SCHOOL = 'REQUEST_DATA_SCHOOL';
export const RECEIVE_DATA_SCHOOL = 'RECEIVE_DATA_SCHOOL';
export const RECEIVE_ERROR_SCHOOL = 'RECEIVE_ERROR_SCHOOL';

export const requestData = () => ({
  type: REQUEST_DATA_SCHOOL,
});

export const receiveData = (data) => ({
  type: RECEIVE_DATA_SCHOOL,
  payload: data,
});

export const receiveError = (data) => ({
  type: RECEIVE_ERROR_SCHOOL,
  payload: data,
});

export const getSchool = (date) => (
  dispatch => {
    dispatch(requestData());
    axios.get(
      `${baseUrlApi}school${date ? `?date=${date}` : ''}`
    ).then(response => {
      dispatch(receiveData({
        data: response.data.data,
      }));
    });
  }
);

export const addMonitor = (email, callback) => (
  dispatch => {
    axios.post(
      `${baseUrlApi}school/monitor`,
      {
        email,
      }
    ).then(() => {
      console.log('b');
      dispatch(getSchool());
      callback();
    }).catch((error) => {
      dispatch(receiveError(error.response.data.message));
    });
  }
);

export const deleteMonitor = (id, callback) => (
  dispatch => {
    axios.delete(
      `${baseUrlApi}school/monitor/${id}`
    ).then(() => {
      dispatch(getSchool());
      callback();
    });
  }
);

