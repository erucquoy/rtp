import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Timeline } from 'react-twitter-widgets';
import { baseUrlApi } from '../../config/config';

import { getArticle } from '../../actions';


class ArticleContainer extends Component {
  componentDidMount() {
    if (this.props.articles.data == undefined) {
      this.props.getArticle();
    }
  }

  render() {
    if (this.props.articles.data == undefined) {
      return (<div></div>);
    }

    return (
      <div>
        <div className="container">
          <div className="col-xs-8">
            <div className="row">
              {(() => {
                const temp = [];
                for (let i = 0; i < this.props.articles.data.length; i++) {
                  temp.push(
                    <div className="col-xs-6">
                      <div id={`myCarousel${i}`} className="carousel slide" data-ride="carousel">
                        <div className="carousel-inner image-article" role="listbox">
                          {(() => {
                            const temp2 = [];
                            const temp3 = this.props.articles.data[i].images;
                            for (let j = 0; j < temp3.length; j++) {
                              temp2.push(
                                <div className={`item ${j == 0 ? 'active' : ''}`} key={`slideshow${j}`}>
                                  <img src={`${baseUrlApi}image/${temp3[j]}`} alt="..." />
                                </div>
                              );
                            }
                            return temp2;
                          })()}
                        </div>

                        <a className="left carousel-control" href={`#myCarousel${i}`} role="button" data-slide="prev">
                          <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                          <span className="sr-only">Previous</span>
                        </a>
                        <a className="right carousel-control" href={`#myCarousel${i}`} role="button" data-slide="next">
                          <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                          <span className="sr-only">Next</span>
                        </a>
                      </div>

                      <b>{this.props.articles.data[i].title}</b><br />
                      {this.props.articles.data[i].content}
                    </div>
                  );
                }
                return temp;
              })()}

            </div>
          </div>

          <div className="col-xs-4">
            <Timeline
              dataSource={{
                sourceType: 'profile',
                screenName: 'twitterdev',
              }}
              options={{
                username: 'TwitterDev',
                height: '400',
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

ArticleContainer.contextTypes = {
  router: PropTypes.object,
};

ArticleContainer.propTypes = {
  getArticle: PropTypes.func,
  articles: PropTypes.object,
};

const mapStateToProps = (state) => ({
  articles: state.article.data,
});

const mapDispatchToProps = (dispatch) => ({
  getArticle: () => dispatch(getArticle()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ArticleContainer);
