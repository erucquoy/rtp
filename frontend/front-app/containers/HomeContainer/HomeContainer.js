import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { baseUrlApi } from '../../config/config';

import { getTemoignage, getPage } from '../../actions';

import icona from '../../img/icona.png';
import iconb from '../../img/iconb.png';
import iconc from '../../img/iconc.png';


class HomeContainer extends Component {
  componentDidMount() {
    if (this.props.temoignages.data == undefined) {
      this.props.getTemoignage();
    }
    if (this.props.page.data == undefined) {
      this.props.getPage();
    }
  }

  dataPage(title) {
    if (this.props.page.data == undefined) {
      return '';
    }

    for (let i = 0; i < this.props.page.data.length; i++) {
      if (this.props.page.data[i].title == title) {
        return this.props.page.data[i].content;
      }
    }
    return '';
  }

  dataPageImage(title) {
    if (this.props.page.data == undefined) {
      return '';
    }

    for (let i = 0; i < this.props.page.data.length; i++) {
      if (this.props.page.data[i].title == title) {
        return this.props.page.data[i].images;
      }
    }
    return '';
  }

  render() {
    if (this.props.temoignages.data == undefined || this.props.page.data == undefined) {
      return (<div></div>);
    }

    return (
      <div>
        <div className="container-fluid rtp-carousel rtp-indexitem">
          <div id="carousel-example-generic" className="carousel slide" data-ride="carousel">
            <ol className="carousel-indicators">
              {(() => {
                const temp = [];
                const temp2 = this.dataPageImage('homepage-slideshow');
                for (let i = 0; i < temp2.length; i++) {
                  temp.push(
                    <li data-target="#carousel-example-generic" data-slide-to={i} className={i == 0 ? 'active' : ''} key={`selector${i}`}></li>
                  );
                }
                return temp;
              })()}
            </ol>

            <div className="carousel-inner" role="listbox">
              {(() => {
                const temp = [];
                const temp2 = this.dataPageImage('homepage-slideshow');
                for (let i = 0; i < temp2.length; i++) {
                  temp.push(
                    <div className={`item ${i == 0 ? 'active' : ''}`} key={`slideshow${i}`}>
                      <img src={`${baseUrlApi}image/${temp2[i]}`} alt="..." />
                      <div className="carousel-caption">
                      </div>
                    </div>
                  );
                }
                return temp;
              })()}
            </div>

            <a className="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
              <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span className="sr-only">Previous</span>
            </a>
            <a className="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
              <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span className="sr-only">Next</span>
            </a>
          </div>
        </div>

        {/* Orange button + 2 side text */}
        <div className="container-fluid rtp-indexitem">
          <div className="row" data-columns="17">
            <div className="col-lg-offset-1 col-lg-5 pubtext">
              <div className="container-fluid txtLbl">
                <div className="col-xs-3">
                  <div className="row">
                    <i className="fa fa-android txtIcons" aria-hidden="true"></i>
                  </div>

                  <div className="row">
                    <i className="fa fa-mobile txtIcons" aria-hidden="true"></i>
                  </div>
                </div>
                <div className="col-xs-14">
                  {this.dataPage('homepage-app-left')}
                </div>
              </div>
            </div>

            <div className="col-lg-4">
              <center>
                <button className="btn btnTry btnOrange">Tester RoadToPermis<br />Gratuitement</button>
              </center>
            </div>

            <div className="col-lg-5 pubtext">
              <div className="container-fluid txtLbl">
                <div className="col-xs-14">
                  {this.dataPage('homepage-app-right')}
                </div>
                <div className="col-xs-3">
                  <div className="row">
                    <i className="fa fa-star txtIcons" aria-hidden="true"></i>
                  </div>

                  <div className="row">
                    <i className="fa fa-android txtIcons" aria-hidden="true"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Nos services  */}
        <div className="blueDiv container-fluid rtp-indexitem">
          <center><h1>Nos services</h1></center>

          <div className="col-lg-6 col-md-6 col-sm-6">
            <center>
              <h2>Apprentis conducteurs</h2>

              <br />

              <ul className="serviceLst">
                <li>{this.dataPage('homepage-apprentis-1')}</li>
                <li>{this.dataPage('homepage-apprentis-2')}</li>
                <li>{this.dataPage('homepage-apprentis-3')}</li>
                <li>{this.dataPage('homepage-apprentis-4')}</li>
              </ul>
            </center>
          </div>

          <div className="col-lg-6 col-md-6 col-sm-6">
            <center>
              <h2>Auto-école</h2>

              <br />

              <ul className="serviceLst">
                <li>{this.dataPage('homepage-autoecole-1')}</li>
                <li>{this.dataPage('homepage-autoecole-2')}</li>
                <li>{this.dataPage('homepage-autoecole-3')}</li>
                <li>{this.dataPage('homepage-autoecole-4')}</li>
              </ul>
            </center>
            <br />
            <br />
          </div>

          <center><button className="btn btnGreen">Inscrivez-vous gratuitement</button></center>
        </div>


        {/* Témoignages de nos utilisateurs (text carousel) */}
        <center>
          <h1 className="color-grey">Témoignages de nos utilisateurs</h1>
        </center>

        <br />

        <div className="container-fluid rtp-indexitem color-grey">

          <div id="carousel-example-te" className="carousel slide" data-ride="carousel">
            <ol className="carousel-indicators">
              <li data-target="#carousel-example-te" data-slide-to="0" className="active"></li>
              <li data-target="#carousel-example-te" data-slide-to="1"></li>
              <li data-target="#carousel-example-te" data-slide-to="2"></li>
            </ol>

            <div className="carousel-inner" role="listbox">
              {(() => {
                if (this.props.temoignages.data == undefined) {
                  return (<div></div>);
                }

                const temp = [];
                for (let i = 0; i < this.props.temoignages.data.length; i += 3) {
                  temp.push(
                    <div className={`item ${i == 0 ? 'active' : ''}`} key={`temoignage${i}`}>
                      <div className="row" data-columns="18">
                        <div className="container-fluid">
                          <div className="col-xs-offset-2 col-xs-4">
                            <center>{this.props.temoignages.data[i].content}</center>
                            <br />
                            <span className="pull-right">{this.props.temoignages.data[i].firstName}</span>
                          </div>

                          <div className="col-xs-4">
                            <center>{this.props.temoignages.data[i + 1] ? this.props.temoignages.data[i + 1].content : ''}</center>
                            <br />
                            <span className="pull-right">{this.props.temoignages.data[i + 1] ? this.props.temoignages.data[i + 1].firstName : ''}</span>
                          </div>

                          <div className="col-xs-4">
                            <center>{this.props.temoignages.data[i + 2] ? this.props.temoignages.data[i + 2].content : ''}</center>
                            <br />
                            <span className="pull-right">{this.props.temoignages.data[i + 2] ? this.props.temoignages.data[i + 2].firstName : ''}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                }
                return temp;
              })()}
            </div>

            <a className="left carousel-control" href="#carousel-example-te" role="button" data-slide="prev">
              <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span className="sr-only">Previous</span>
            </a>
            <a className="right carousel-control" href="#carousel-example-te" role="button" data-slide="next">
              <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span className="sr-only">Next</span>
            </a>
          </div>

        </div>

        {/* 3 horizontal icons */}
        <div className="container-fluid rtp-indexitem">
          <div className="row" data-columns="32">
            <div className="col-xs-offset-1 col-xs-9">
              <center>
                <img src={icona} className="bigIcons" role="presentation" /><br />
                <span className="featuresLabel">Service client disponible 6j/7 de 8h30 à 19h00.</span>
              </center>
            </div>

            <div className="col-xs-9">
              <center>
                <img src={iconb} className="bigIcons" role="presentation" /><br />
                <span className="featuresLabel">Satisfait ou remboursé</span>
              </center>
            </div>

            <div className="col-xs-9">
              <center>
                <img src={iconc} className="bigIcons" role="presentation" /><br />
                <span className="featuresLabel">Résiliable à tout moment</span>
              </center>
            </div>
          </div>
        </div>

        {/* Green panel */}
        <div className="greenPanel container-fluid feedbackPanel">

          <h1>Ils nous soutiennent et parlent de RoadToPermis</h1>

          <br />

          <div className="col-lg-4 col-md-4 col-sm-4">
            <span className="feedback">{this.dataPage('homepage-parle-1')}</span>
            <br /><br />
          </div>

          <div className="col-lg-4 col-md-4 col-sm-4">
            <span className="feedback">{this.dataPage('homepage-parle-2')}</span>
            <br /><br />
          </div>

          <div className="col-lg-4 col-md-4 col-sm-4">
            <span className="feedback">{this.dataPage('homepage-parle-3')}</span>
            <br /><br />
          </div>
        </div>
      </div>
    );
  }
}

HomeContainer.contextTypes = {
  router: PropTypes.object,
};

HomeContainer.propTypes = {
  getTemoignage: PropTypes.func,
  temoignages: PropTypes.object,
  getPage: PropTypes.func,
  page: PropTypes.object,
};

const mapStateToProps = (state) => ({
  temoignages: state.temoignage.data,
  page: state.ui.page,
});

const mapDispatchToProps = (dispatch) => ({
  getTemoignage: () => dispatch(getTemoignage()),
  getPage: () => dispatch(getPage()),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
