import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Timeline } from 'react-twitter-widgets';
import { baseUrlApi } from '../../config/config';

import { getTemoignage } from '../../actions';


class TemoignageContainer extends Component {
  componentDidMount() {
    if (this.props.temoignages.data == undefined) {
      this.props.getTemoignage();
    }
  }

  render() {
    if (this.props.temoignages.data == undefined) {
      return (<div></div>);
    }

    return (
      <div>
        <div className="container-fluid temoignages-heading">
          <div className="row">
            <center>
              <h1 className="temoignages-header">Ils ont utilisé RoadToPermis et ils vous en parlent</h1>
            </center>
          </div>
        </div>

        <br /><br /><br />

        <div className="container-fluid">
          <div className="col-lg-9">

            {(() => {
              const temp = [];
              for (let i = 0; i < this.props.temoignages.data.length; i++) {
                if (i % 2 == 0) {
                  temp.push(
                    <div className="row  temoignage" key={`temoignage${i}`}>
                      <div className="container-fluid">
                        <div className="col-xs-4">
                          <center>
                            <img
                              className="img-circle img-responsive"
                              width="150"
                              src={`${baseUrlApi}image/${this.props.temoignages.data[i].images && this.props.temoignages.data[i].images.length > 0 ? this.props.temoignages.data[i].images[0] : ''}`}
                              alt="face"
                            />
                            <br />
                            <span className="name">{this.props.temoignages.data[i].firstName}</span>
                          </center>
                        </div>
                        <div className="col-xs-8">
                          <center className="temoignage-text">
                            {this.props.temoignages.data[i].content}
                          </center>
                        </div>
                      </div>
                    </div>
                  );
                } else {
                  temp.push(
                    <div className="row  temoignage temoignage-blue" key={`temoignage${i}`}>
                      <div className="container-fluid">
                        <div className="col-xs-8">
                          <center className="temoignage-text">
                            {this.props.temoignages.data[i].content}
                          </center>
                        </div>
                        <div className="col-xs-4">
                          <center>
                            <img
                              className="img-circle img-responsive"
                              width="150"
                              src={`${baseUrlApi}image/${this.props.temoignages.data[i].images && this.props.temoignages.data[i].images.length > 0 ? this.props.temoignages.data[i].images[0] : ''}`}
                              alt="face"
                            />
                            <br />
                            <span className="name">{this.props.temoignages.data[i].firstName}</span>
                          </center>
                        </div>
                      </div>
                    </div>
                  );
                }
              }
              return temp;
            })()}

          </div>

          <div className="col-lg-3">
            <Timeline
              dataSource={{
                sourceType: 'profile',
                screenName: 'twitterdev',
              }}
              options={{
                username: 'TwitterDev',
                height: '400',
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

TemoignageContainer.contextTypes = {
  router: PropTypes.object,
};

TemoignageContainer.propTypes = {
  getTemoignage: PropTypes.func,
  temoignages: PropTypes.object,
};

const mapStateToProps = (state) => ({
  temoignages: state.temoignage.data,
});

const mapDispatchToProps = (dispatch) => ({
  getTemoignage: () => dispatch(getTemoignage()),
});

export default connect(mapStateToProps, mapDispatchToProps)(TemoignageContainer);
