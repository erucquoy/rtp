import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import AddLesson from '../../components/School/AddLesson';

import {
  setPageTitle,
  addLesson,
  updateLesson,
  getLessonId,
  deleteLesson,
} from '../../actions';

class AddLessonContainer extends Component {
  componentWillMount() {
    this.props.onSetPageTitle('RoadToPermis');
    if (!this.props.connected) {
      this.context.router.replace({
        pathname: this.props.params.return ? this.props.params.return.split('!').join('/') : '/ecole',
      });
    }

    if (this.props.params.id) {
      this.props.getLessonId(this.props.params.id);
    }
  }

  componentWillReceiveProps(next) {
    if (!next.connected) {
      this.context.router.replace({
        pathname: this.props.params.return ? this.props.params.return.split('!').join('/') : '/ecole',
      });
    }
  }

  render() {
    if (!this.props.school.data || (this.props.params.id && !this.props.lesson.selected) || this.props.lesson.isFetching) {
      return (<div></div>);
    }

    const initalData = this.props.lesson.selected ? this.props.lesson.selected.data : {};

    if (typeof initalData.toWork == 'object' && (initalData.toWork.length > 0 || initalData.achievements.length > 0)) {
      initalData.toWork = initalData.toWork.join('\n');
      initalData.achievements = initalData.achievements.join('\n');
    }

    let handler;

    if (this.props.params.id) {
      handler = (values) => this.props.onSubmitHandlerUpdate(this.props.params.id, values, () => {
        this.context.router.replace({
          pathname: this.props.params.return ? this.props.params.return.split('!').join('/') : '/ecole',
        });
      });
    } else {
      handler = (values) => this.props.onSubmitHandler(values, () => {
        this.context.router.replace({
          pathname: this.props.params.return ? this.props.params.return.split('!').join('/') : '/ecole',
        });
      });
    }

    return (
      <AddLesson
        user={this.props.user}
        lesson={this.props.lesson}
        school={this.props.school.data}
        edit={this.props.params.id != undefined}
        initialValues={{
          school: this.props.user.school ? this.props.user.school['_id'] : '',
          monitor: this.props.user.school && this.props.school.data.monitors.length > 0 ? this.props.school.data.monitors[0]['_id'] : '',
          student: this.props.user.school && this.props.school.data.students.length > 0 ? this.props.school.data.students[0]['_id'] : '',
          ...initalData,
        }}
        onSubmitHandler={handler}
        handleDelete={() => {
          const confirmation = confirm('Voulez-vous supprimer définitivement cette leçon et ses données ? Il n\'est pas possible de restaurer les données après cela.');
          if (confirmation == true) {
            this.props.onDeleteHandler(this.props.params.id, () => {
              this.context.router.replace({
                pathname: this.props.params.return ? this.props.params.return.split('!').join('/') : '/ecole',
              });
            });
          }
        }}
      />
    );
  }
}

AddLessonContainer.contextTypes = {
  router: PropTypes.object,
};

AddLessonContainer.propTypes = {
  onSetPageTitle: PropTypes.func,
  onSubmitHandler: PropTypes.func,
  params: PropTypes.object,
  user: PropTypes.object,
  connected: PropTypes.bool,
  lesson: PropTypes.object,
  school: PropTypes.object,
  getLessonId: PropTypes.func,
  onSubmitHandlerUpdate: PropTypes.func,
  onDeleteHandler: PropTypes.func,
};

const mapStateToProps = (state) => ({
  user: state.user.data,
  connected: Object.keys(state.user.data).length !== 0 && state.user.data.fail != true,
  lesson: state.lesson,
  school: state.school.data,
});

const mapDispatchToProps = (dispatch) => ({
  onSetPageTitle: (title) => dispatch(setPageTitle(title)),
  onSubmitHandler: (values, callback) => dispatch(addLesson(values, callback)),
  onSubmitHandlerUpdate: (id, values, callback) => dispatch(updateLesson(id, values, callback)),
  onDeleteHandler: (id, callback) => dispatch(deleteLesson(id, callback)),
  getLessonId: (id) => dispatch(getLessonId(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddLessonContainer);
