import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Lesson from '../../components/School/Lesson';

import '../../style/client.scss';

import {
  getLessonUser,
} from '../../actions';

class LessonContainer extends Component {
  componentDidMount() {
    this.props.getLessonUser(this.props.params.id);
    if (!this.props.connected) {
      return this.context.router.replace({
        pathname: '/',
      });
    }
    /* if (!this.props.user.monitor && !this.props.user.drivingSchool) {
      return this.context.router.replace({
        pathname: '/',
      });
    } */

    return '';
  }

  componentWillReceiveProps(next) {
    if (!next.connected) {
      return this.context.router.replace({
        pathname: '/',
      });
    }
    /* if (!this.props.user.monitor && !this.props.user.drivingSchool) {
      return this.context.router.replace({
        pathname: '/',
      });
    } */

    return '';
  }

  render() {
    if (!this.props.connected || !this.props.lessons.data) {
      return (<div></div>);
    }

    return (
      <div>
        <div className="blueDiv container-fluid servicesHeader">
          <center>
            <h2>Rapports</h2>
          </center>
        </div>
        <br />

        <Lesson
          lessons={this.props.lessons.data}
          notAdmin={!this.props.user.monitor && !this.props.user.drivingSchool}
        />


      </div>
    );
  }
}

LessonContainer.contextTypes = {
  router: PropTypes.object,
};

LessonContainer.propTypes = {
  user: PropTypes.object,
  connected: PropTypes.bool,
  lessons: PropTypes.array,
  getLessonUser: PropTypes.func,
  params: PropTypes.object,
};

const mapStateToProps = (state) => ({
  user: state.user.data,
  connected: Object.keys(state.user.data).length !== 0 && state.user.data.fail != true,
  lessons: state.lesson.data,
});

const mapDispatchToProps = (dispatch) => ({
  getLessonUser: (id) => dispatch(getLessonUser(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LessonContainer);
