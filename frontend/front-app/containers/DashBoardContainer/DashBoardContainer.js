import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import '../../style/client.scss';

// import General from '../../components/DashBoard/General';
// import Lesson from '../LessonContainer';


class DashBoardContainer extends Component {
  constructor() {
    super();
    this.state = { show: 'general' };
  }

  componentDidMount() {
    if (!this.props.connected) {
      this.context.router.replace({
        pathname: '/connexion',
      });
    }
    if (!this.props.params || !this.props.params.id || this.props.params.id.length == 0) {
      return this.context.router.replace({
        pathname: `/moniteur/lecon/${this.props.user.id}`,
      });
    }
    return (<div></div>);
  }

  changeShow(title) {
    this.setState({ show: title });
  }

  render() {
    if (!this.props.connected) {
      return (<div></div>);
    }

    console.log(this.props.user.id);

    return (<div></div>);

    /*
    return (
      <div>
        <div className="blueDiv container-fluid servicesHeader">
          <center>
            <h2>Tableau de bord</h2>
          </center>
        </div>
        <div className="pills-container">
          <div className={`pill ${this.state.show == 'general' ? 'active' : ''}`} onClick={() => this.changeShow('general')}>
           Général
          </div>
          <div className={`pill pill2 ${this.state.show == 'formation' ? 'active' : ''}`} onClick={() => this.changeShow('formation')}>
           Formation
          </div>
          <div className={`pill pill3 ${this.state.show == 'budget' ? 'active' : ''}`} onClick={() => this.changeShow('budget')}>
           Budget
          </div>
        </div>
        <br />
        {(() => {
          switch (this.state.show) {
            case 'general':
              return (<General user={this.props.user} />);
            default:
              return (<General user={this.props.user} />);
          }
        })()}

      </div>
    );
    */
  }
}

DashBoardContainer.contextTypes = {
  router: PropTypes.object,
};

DashBoardContainer.propTypes = {
  params: PropTypes.object,
  user: PropTypes.object,
  connected: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  user: state.user.data,
  connected: Object.keys(state.user.data).length !== 0 && state.user.data.fail != true,
});

const mapDispatchToProps = () => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(DashBoardContainer);
