import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import '../../style/client.scss';

import Lessons from '../../components/School/Lessons';
import Student from '../../components/School/Student';

import {
  getSchool,
} from '../../actions';

class MonitorContainer extends Component {
  constructor() {
    super();
    this.state = { show: 'lessons' };
  }

  componentDidMount() {
    this.props.getSchool();
    if (!this.props.connected) {
      return this.context.router.replace({
        pathname: '/',
      });
    }
    if (!this.props.user.monitor) {
      return this.context.router.replace({
        pathname: '/',
      });
    }

    return '';
  }

  componentWillReceiveProps(next) {
    if (!next.connected) {
      return this.context.router.replace({
        pathname: '/',
      });
    }
    if (!next.user.monitor) {
      return this.context.router.replace({
        pathname: '/',
      });
    }

    return '';
  }

  changeShow(title) {
    this.setState({ show: title });
  }

  addHours(date, h) {
    date.setTime(date.getTime() + (h * 60 * 60 * 1000));
    return date;
  }

  render() {
    if (!this.props.connected || !this.props.school.data) {
      return (<div></div>);
    }

    for (let i = 0; i < this.props.school.data.events.length; i++) {
      this.props.school.data.events[i].start = new Date(this.props.school.data.events[i].date);
      this.props.school.data.events[i].end = this.addHours(new Date(this.props.school.data.events[i].date), this.props.school.data.events[i].time);
      this.props.school.data.events[i].title = `${this.props.school.data.events[i].student.profile.firstName} ${this.props.school.data.events[i].student.profile.lastName} - ${this.props.school.data.events[i].monitor.profile.firstName} ${this.props.school.data.events[i].monitor.profile.lastName}`;
    }

    return (
      <div>
        <div className="blueDiv container-fluid servicesHeader">
          <center>
            <h2>Tableau de bord</h2>
          </center>
        </div>
        <div className="pills-container-2">
          <div className={`pill ${this.state.show == 'lessons' ? 'active' : ''}`} onClick={() => this.changeShow('lessons')}>
           Leçons
          </div>
          <div className={`pill pill3 ${this.state.show == 'student' ? 'active' : ''}`} onClick={() => this.changeShow('student')}>
           Elèves
          </div>
        </div>
        <br />
        {(() => {
          switch (this.state.show) {
            case 'lessons':
              return (<Lessons
                user={this.props.user}
                events={this.props.school.data.events}
                handlerSelect={(event) => {
                  this.context.router.replace({
                    pathname: `/ecole/lecon/edition/${event['_id']}`,
                  });
                }}
                onChangeMonth={(date) => this.props.getSchool(date)}
              />);
            case 'student':
              return (<Student
                students={this.props.school.data.students}
              />);
            default:
              return (<Lessons
                user={this.props.user}
                events={this.props.school.data.events}
                handlerSelect={(event) => {
                  this.context.router.replace({
                    pathname: `/ecole/lecon/edition/${event['_id']}`,
                  });
                }}
              />);
          }
        })()}

      </div>
    );
  }
}

MonitorContainer.contextTypes = {
  router: PropTypes.object,
};

MonitorContainer.propTypes = {
  user: PropTypes.object,
  connected: PropTypes.bool,
  school: PropTypes.object,
  getSchool: PropTypes.func,
};

const mapStateToProps = (state) => ({
  user: state.user.data,
  connected: Object.keys(state.user.data).length !== 0 && state.user.data.fail != true,
  school: state.school.data,
});

const mapDispatchToProps = (dispatch) => ({
  getSchool: (date) => dispatch(getSchool(date)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MonitorContainer);
