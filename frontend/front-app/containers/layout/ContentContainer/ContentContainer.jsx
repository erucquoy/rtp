import React, { PropTypes } from 'react';

const ContentContainer = ({ children }) => (
  <div className="content-app-container">
    {/* Content */}
    {children}
  </div>
);

ContentContainer.propTypes = {
  children: PropTypes.node,
};


export default ContentContainer;

