import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import NavBarContainer from '../NavBarContainer';
import ContentContainer from '../ContentContainer';
import FooterContainer from '../FooterContainer';

class MainContainer extends Component {
  componentWillMount() {
  }

  componentWillReceiveProps() {
  }

  render() {
    return (
      <div id="mainContainer">
        {/* Top Nav Bar */}
        <NavBarContainer />
        {/* Content */}
        <ContentContainer>
          {this.props.children}
        </ContentContainer>
        {/* Footer */}
        <FooterContainer />
      </div>
    );
  }
}

MainContainer.contextTypes = {
  router: PropTypes.object,
};

MainContainer.propTypes = {
  children: PropTypes.node,
  params: PropTypes.object,
};

const mapStateToProps = (state) => ({
  userData: state.user,
});

export default connect(mapStateToProps)(MainContainer);
