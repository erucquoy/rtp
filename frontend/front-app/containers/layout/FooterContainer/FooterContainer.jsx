import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import Facebook from '../../../img/social/Facebook.png';
import Linkedin from '../../../img/social/Linkedin.png';
import Twitter from '../../../img/social/Twitter.png';
import Google from '../../../img/social/Google+.png';

import {
  logoutUser,
} from '../../../actions';

const FooterContainer = () => (
  <div className="blueDiv container-fluid">
    <br />
    <div className="pull-right motto">Suivez-nous sur les réseaux sociaux et rejoignez la communauté des apprentis conducteurs.</div><br /> <br />
    <div className="pull-right social-icons">
      <img src={Facebook} width="40" alt="facebook" />
      <img src={Linkedin} width="40" alt="linkedin" />
      <img src={Twitter} width="40" alt="twitter" />
      <img src={Google} width="40" alt="google+" />
    </div>

    <br /><br /><br /><br />

    <a className="footerLinks" href="./">Qui sommes-nous ?</a>
    <a className="footerLinks" href="./">Mentions légales ?</a>
    <a className="footerLinks" href="./">CGV</a>

    <div className="pull-right copyright">&copy; 2017 RoadToPermis</div>

    <br /><br />
  </div>
);

FooterContainer.propTypes = {
  pageTitle: PropTypes.string,
  logout: PropTypes.func,
};

const mapStateToProps = (state) => ({
  pageTitle: state.ui.pageTitle,
});

const mapDispatchToProps = (dispatch) => ({
  logout: () =>
    dispatch(logoutUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(FooterContainer);
