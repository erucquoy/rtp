import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import homeImg from '../../../img/home.png';
import brandImg from '../../../img/brand_logo.png';

import {
  logoutUser,
} from '../../../actions';

const NavBarContainer = ({ connected, user, logout }) => (
  <nav className="navbar navbar-default navbar-fixed-top">
    <div className="container test">
      {/* Brand and toggle get grouped for better mobile display */}
      <div className="navbar-header">
        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span className="sr-only">Toggle navigation</span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
        </button>
        <Link className="navbar-brand" to="/"><img src={brandImg} height="55" className="brand-logo" role="presentation" /></Link>
      </div>


      <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul className="nav-center nav navbar-nav navbar-right">
          <li><Link to="/"><img src={homeImg} role="presentation" /></Link></li>
          {connected && !user.drivingSchool && !user.admin && !user.monitor ? <li className="navbar-dividers"><Link className="barlink" to="/tableau">TABLEAU DE BORD</Link></li> : ''}
          {connected && user.drivingSchool ? <li className="navbar-dividers"><Link className="barlink" to="/ecole">AUTO-ECOLE</Link></li> : ''}
          {connected && !user.drivingSchool && user.monitor ? <li className="navbar-dividers"><Link className="barlink" to="/moniteur">MONITEUR</Link></li> : ''}
          <li className="navbar-dividers"><Link className="barlink" to="/articles">ACTUALITÉS</Link></li>
          <li className="navbar-dividers"><Link className="barlink" to="/services">SERVICES</Link></li>
          <li className="navbar-dividers"><Link className="barlink" to="/offres">OFFRES</Link></li>
          <li className="navbar-dividers"><Link className="barlink" to="/temoignages">TÉMOIGNAGES</Link></li>
          <li className="navbar-dividers"><a className="barlink" href="">CONTACT</a></li>
          {(() => {
            if (connected) {
              return (
                <li key="logout"><a className="barlink" onClick={logout}><button className="btn btnOrange">Déconnexion</button></a></li>
              );
            }
            return [
              <li key="register"><Link className="barlink" to="/inscription"><button className="btn btnGreen">Inscription</button></Link></li>,
              <li key="login"><Link className="barlink" to="/connexion"><button className="btn btnOrange">Connexion</button></Link></li>,
            ];
          })()}
        </ul>
      </div>
    </div>
  </nav>
);

NavBarContainer.propTypes = {
  pageTitle: PropTypes.string,
  logout: PropTypes.func,
  connected: PropTypes.bool,
  user: PropTypes.object,
};

const mapStateToProps = (state) => ({
  pageTitle: state.ui.pageTitle,
  connected: Object.keys(state.user.data).length !== 0 && state.user.data.fail != true,
  user: state.user.data,
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logoutUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(NavBarContainer);
