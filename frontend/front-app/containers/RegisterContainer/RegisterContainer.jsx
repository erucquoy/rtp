import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Register from '../../components/user/Register';

import {
  registerUser,
  loginJWTUser,
  setPageTitle,
} from '../../actions';

class LoginContainer extends Component {
  componentWillMount() {
    this.props.onSetPageTitle('RoadToPermis');

    if (localStorage.getItem('tokenJWT') != undefined) {
      this.props.loginJWTUser(this.props.params.jwtToken);

      if (this.props.connected) {
        this.context.router.replace({
          pathname: '/',
        });
      }
    }
  }

  componentWillReceiveProps(next) {
    if (next.connected) {
      this.context.router.replace({
        pathname: '/',
      });
    }
  }

  render() {
    return (
      <Register
        user={this.props.user}
        onSubmitHandler={this.props.onSubmitHandler}
      />
    );
  }
}

LoginContainer.contextTypes = {
  router: PropTypes.object,
};

LoginContainer.propTypes = {
  onSetPageTitle: PropTypes.func,
  onSubmitHandler: PropTypes.func,
  params: PropTypes.object,
  loginJWTUser: PropTypes.func,
  user: PropTypes.object,
  connected: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  user: state.user.data,
  connected: Object.keys(state.user.data).length !== 0 && state.user.data.fail != true,
});

const mapDispatchToProps = (dispatch) => ({
  onSetPageTitle: (title) =>
    dispatch(setPageTitle(title)),
  onSubmitHandler: (value) =>
    dispatch(registerUser(value)),
  loginJWTUser: (jwtToken) =>
    dispatch(loginJWTUser(jwtToken)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
