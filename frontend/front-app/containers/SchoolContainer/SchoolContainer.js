import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import '../../style/client.scss';

import Lessons from '../../components/School/Lessons';
import Monitor from '../../components/School/Monitor';
import Student from '../../components/School/Student';

import {
  getSchool,
  addMonitor,
  deleteMonitor,
} from '../../actions';

class SchoolContainer extends Component {
  constructor() {
    super();
    this.state = { show: 'lessons' };
  }

  componentDidMount() {
    this.props.getSchool();
    if (!this.props.connected) {
      return this.context.router.replace({
        pathname: '/',
      });
    }
    if (!this.props.user.drivingSchool) {
      return this.context.router.replace({
        pathname: '/moniteur',
      });
    }

    return '';
  }

  componentWillReceiveProps(next) {
    if (!next.connected) {
      return this.context.router.replace({
        pathname: '/',
      });
    }
    if (!next.user.drivingSchool) {
      return this.context.router.replace({
        pathname: '/moniteur',
      });
    }

    return '';
  }

  changeShow(title) {
    this.setState({ show: title });
  }

  addHours(date, h) {
    date.setTime(date.getTime() + (h * 60 * 60 * 1000));
    return date;
  }

  render() {
    if (!this.props.connected || !this.props.school.data) {
      return (<div></div>);
    }

    for (let i = 0; i < this.props.school.data.events.length; i++) {
      this.props.school.data.events[i].start = new Date(this.props.school.data.events[i].date);
      this.props.school.data.events[i].end = this.addHours(new Date(this.props.school.data.events[i].date), this.props.school.data.events[i].time);
      this.props.school.data.events[i].title = `${this.props.school.data.events[i].student.profile.firstName} ${this.props.school.data.events[i].student.profile.lastName} - ${this.props.school.data.events[i].monitor.profile.firstName} ${this.props.school.data.events[i].monitor.profile.lastName}`;
    }

    return (
      <div>
        <div className="blueDiv container-fluid servicesHeader">
          <center>
            <h2>Tableau de bord</h2>
          </center>
        </div>
        <div className="pills-container">
          <div className={`pill ${this.state.show == 'lessons' ? 'active' : ''}`} onClick={() => this.changeShow('lessons')}>
           Leçons
          </div>
          <div className={`pill pill2 ${this.state.show == 'monitor' ? 'active' : ''}`} onClick={() => this.changeShow('monitor')}>
           Moniteurs
          </div>
          <div className={`pill pill3 ${this.state.show == 'student' ? 'active' : ''}`} onClick={() => this.changeShow('student')}>
           Elèves
          </div>
        </div>
        <br />
        {(() => {
          switch (this.state.show) {
            case 'lessons':
              return (<Lessons
                user={this.props.user}
                events={this.props.school.data.events}
                handlerSelect={(event) => {
                  this.context.router.replace({
                    pathname: `/ecole/lecon/edition/${event['_id']}`,
                  });
                }}
                onChangeMonth={(date) => this.props.getSchool(date)}
              />);
            case 'monitor':
              return (<Monitor
                monitors={this.props.school.data.monitors}
                handleDeleteMonitor={(id) => this.props.deleteMonitor(id, () => {})}
                handleAddMonitor={value => this.props.addMonitor(value.email, () => {})}
                errorDisplay={this.props.schoolAll.error ? this.props.schoolAll.error : ''}
              />);
            case 'student':
              return (<Student
                students={this.props.school.data.students}
              />);
            default:
              return (<Lessons
                user={this.props.user}
                events={this.props.school.data.events}
                handlerSelect={(event) => {
                  this.context.router.replace({
                    pathname: `/ecole/lecon/edition/${event['_id']}`,
                  });
                }}
              />);
          }
        })()}

      </div>
    );
  }
}

SchoolContainer.contextTypes = {
  router: PropTypes.object,
};

SchoolContainer.propTypes = {
  user: PropTypes.object,
  connected: PropTypes.bool,
  school: PropTypes.object,
  getSchool: PropTypes.func,
  deleteMonitor: PropTypes.func,
  addMonitor: PropTypes.func,
  schoolAll: PropTypes.object,
};

const mapStateToProps = (state) => ({
  user: state.user.data,
  connected: Object.keys(state.user.data).length !== 0 && state.user.data.fail != true,
  school: state.school.data,
  schoolAll: state.school,
});

const mapDispatchToProps = (dispatch) => ({
  getSchool: (date) => dispatch(getSchool(date)),
  deleteMonitor: (id, callback) => dispatch(deleteMonitor(id, callback)),
  addMonitor: (email, callback) => dispatch(addMonitor(email, callback)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SchoolContainer);
