import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import YouTube from 'react-youtube';

import { baseUrlApi } from '../../config/config';

import { getPage } from '../../actions';

import googlePlayImg from '../../img/google-play-badge.png';
import appStoreImg from '../../img/app-store-badge.svg';


class OffreContainer extends Component {
  componentDidMount() {
    if (this.props.page.data == undefined) {
      this.props.getPage();
    }
  }

  dataPage(title) {
    if (this.props.page.data == undefined) {
      return '';
    }

    for (let i = 0; i < this.props.page.data.length; i++) {
      if (this.props.page.data[i].title == title) {
        return this.props.page.data[i].content;
      }
    }
    return '';
  }

  dataPageImage(title) {
    if (this.props.page.data == undefined) {
      return '';
    }

    for (let i = 0; i < this.props.page.data.length; i++) {
      if (this.props.page.data[i].title == title) {
        return this.props.page.data[i].images;
      }
    }
    return '';
  }

  render() {
    if (this.props.page.data == undefined) {
      return (<div></div>);
    }
    const regexYoutube = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&\?]*).*/;
    const matchYoutube = this.dataPage('services-youtube-link').match(regexYoutube);

    return (
      <div id="services">
        <div className="blueDiv container-fluid servicesHeader">
          <center>
            <h2>Optimise tes chances au permis de conduire avec RoadToPermis</h2>
          </center>
        </div>

        <br />

        <div className="container-fluid">
          <div className="col-lg-7">
            <div className="embed-responsive embed-responsive-16by9">
              <YouTube
                videoId={(matchYoutube && matchYoutube[7].length == 11) ? matchYoutube[7] : false}
                opts={{
                  height: '315',
                  width: '560',
                }}
              />
            </div>
          </div>
          <div className="col-lg-5 why-use">
            <h3>Pourquoi utiliser RoadToPermis ?</h3>

            <br />

            <ul className="pourquoirtp">
              <li>{this.dataPage('services-why-1')}</li>
              <li>{this.dataPage('services-why-2')}</li>
              <li>{this.dataPage('services-why-3')}</li>
              <li>{this.dataPage('services-why-4')}</li>
              <li>{this.dataPage('services-why-5')}</li>
            </ul>
          </div>
        </div>

        <br />

        <div className="blueDiv container-fluid servicesHeader">
          <center>
            <h2>Les services offerts par RoadToPermis</h2>
          </center>
        </div>

        <br />

        <div className="container-fluid">
          <div className="col-lg-6">
            <center>
              <h3>Apprentis conducteurs</h3>

              <br />

              <ul className="services-list servicesgreen">
                <li>{this.dataPage('services-apprentis-1')}</li>
                <li>{this.dataPage('services-apprentis-2')}</li>
                <li>{this.dataPage('services-apprentis-3')}</li>
                <li>{this.dataPage('services-apprentis-4')}</li>
              </ul>
            </center>
          </div>

          <div className="col-lg-6">
            <center>
              <h3>Auto-école et moniteurs</h3>

              <br />

              <ul className="services-list servicesorange">
                <li>{this.dataPage('services-autoecole-1')}</li>
                <li>{this.dataPage('services-autoecole-2')}</li>
                <li>{this.dataPage('services-autoecole-3')}</li>
                <li>{this.dataPage('services-autoecole-4')}</li>
              </ul>
            </center>
          </div>

          <center className="btnContainers">
            <br />			<br />			<br />
            <button className="btnGreen btn">Inscription</button>
            <button className="btnGrey btn">Contactez-nous</button>
          </center>
        </div>

        <br />

        <div className="blueDiv container-fluid servicesHeader">
          <center>
            <h2>Une application mobile simple à utiliser</h2>
          </center>
        </div>

        <br />

        <div className="container-fluid">
          <div className="col-lg-offset-1 col-lg-7">
            <div id="myCarousel" className="carousel slide" data-ride="carousel">


              <div className="carousel-inner" role="listbox">
                {(() => {
                  const temp = [];
                  const temp2 = this.dataPageImage('homepage-slideshow');
                  for (let i = 0; i < temp2.length; i++) {
                    temp.push(
                      <div className={`item ${i == 0 ? 'active' : ''}`} key={`slideshow${i}`}>
                        <img src={`${baseUrlApi}image/${temp2[i]}`} alt="..." />
                      </div>
                    );
                  }
                  return temp;
                })()}
              </div>

              <a className="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span className="sr-only">Previous</span>
              </a>
              <a className="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span className="sr-only">Next</span>
              </a>
            </div>
          </div>

          <div className="col-lg-3 mobile-markets-links">
            <center>
              <a href=""><img className="img-responsive google-play" src={googlePlayImg} alt="google play" /></a>
              <a href=""><img className="img-responsive store" src={appStoreImg} alt="app store" /></a>
            </center>
          </div>
        </div>

        <br />

        <br />

        <div className="container-fluid plansBG">
          <center className="row offres-header">
            <h2>Découvrez nos offres d'abonnement et d'essai</h2>
          </center>

          <br /><br />

          <div className="container-fluid">

            <div className="row" data-columns="52">

              <div className="col-lg-6"></div>
              <div className="plan plan1 col-lg-12  col-sm-12 col-xs-12">
                <br />

                <b><center>OFFRE D'ESSAI 3 JOURS</center></b>

                <br />

                <center>
                  <span className="pricePlan">1€</span>
                  <span className="planTime">pour 3 jours</span>

                  <br />

                  <div className="planCaret"> </div>
                </center>

                <br />

                <ul>
                  <li>{this.dataPage('offer-3days-1')}</li>
                  <li>{this.dataPage('offer-3days-2')}</li>
                  <li>{this.dataPage('offer-3days-3')}</li>
                  <li>{this.dataPage('offer-3days-4')}</li>
                </ul>

                <br /><br />

                <div className="hCaret"> </div>

                <br />

                <center>
                  <a href="../inscription/confirmer" className="btn btnPickPlan">J'en profite</a>
                </center>


                <br /><br />
              </div>
              <div className="col-lg-2"></div>
              <div className="plan plan2 col-lg-12 col-sm-12 col-xs-12">
                <br />

                <b><center>ABONNEMENT PREMIUM 1 MOIS</center></b>

                <br />

                <center>
                  <span className="pricePlan">9€90</span>
                  <span className="planTime">/mois</span>

                  <br />

                  <div className="planCaret"> </div>
                </center>

                <br />

                <ul>
                  <li>{this.dataPage('offer-1month-1')}</li>
                  <li>{this.dataPage('offer-1month-2')}</li>
                  <li>{this.dataPage('offer-1month-3')}</li>
                  <li>{this.dataPage('offer-1month-4')}</li>
                </ul>

                <br /><br />

                <div className="hCaret"> </div>

                <br />

                <center>
                  <a href="../inscription/confirmer" className="btn btnPickPlan">J'en profite</a>
                </center>


                <br /><br />
              </div>
              <div className="col-lg-2"></div>
              <div className="plan plan3 col-lg-12 col-sm-12 col-xs-12">
                <br />

                <b><center>ABONNEMENT PREMIUM 6 MOIS</center></b>

                <br />

                <center>
                  <span className="pricePlan">7€99</span>
                  <span className="planTime">/mois</span>

                  <br />

                  <div className="planCaret"> </div>
                </center>

                <br />

                <ul>
                  <li>{this.dataPage('offer-6months-1')}</li>
                  <li>{this.dataPage('offer-6months-2')}</li>
                  <li>{this.dataPage('offer-6months-3')}</li>
                  <li>{this.dataPage('offer-6months-4')}</li>
                </ul>

                <br /><br />

                <div className="hCaret"> </div>

                <br />

                <center>
                  <a href="../inscription/confirmer" className="btn btnPickPlan">J'en profite</a>
                </center>


                <br /><br />
              </div>

            </div>

          </div>

          <br /><br />

          <center className="row offres-header">
            <h2>Vous êtes une auto-école et êtes intéréssé par notre solution?</h2>
          </center>

          <br /><br />

          <div className="row">
            <div className="offres-panel col-xs-8 col-xs-offset-2">
              <center>
                <ul className="plans-list">
                  <li>{this.dataPage('offer-solution-1')}</li>
                  <li>{this.dataPage('offer-solution-2')}</li>
                  <li>{this.dataPage('offer-solution-3')}</li>
                  <li>{this.dataPage('offer-solution-4')}</li>
                </ul>


                <div className="container-fluid">
                  <div className="col-xs-5 col-xs-offset-1">
                    <button className="btn btnGrey form-control">Téléchargez notre brochure</button>
                  </div>
                  <div className="col-xs-5">
                    <button className="btn btnGreen form-control">Contactez-nous</button>
                  </div>
                </div>

              </center>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

OffreContainer.contextTypes = {
  router: PropTypes.object,
};

OffreContainer.propTypes = {
  getPage: PropTypes.func,
  page: PropTypes.object,
};

const mapStateToProps = (state) => ({
  page: state.ui.page,
});

const mapDispatchToProps = (dispatch) => ({
  getPage: () => dispatch(getPage()),
});

export default connect(mapStateToProps, mapDispatchToProps)(OffreContainer);
