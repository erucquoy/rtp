import {
  SET_PAGE_TITLE,
} from '../actions/act_main_ui';

const ui = (state = {}, action = {}) => {
  switch (action.type) {
    case SET_PAGE_TITLE:
      return {
        ...state,
        pageTitle: action.payload,
      };
    default:
      return state;
  }
};

export default ui;
