import { combineReducers } from 'redux';
import ui from './rdc_main_ui';
import user from './rdc_user';
import data from './rdc_data';
import { reducer as formReducer } from 'redux-form';

const backOfficeApp = combineReducers({
  ui,
  user,
  data,
  form: formReducer,
});

export default backOfficeApp;
