import {
  RECEIVE_DATA,
  REQUEST_DATA,
  RECEIVE_PAGE,
  RECEIVE_ERROR,
  SELECT_DATA,
  SELECT_PAGE,
  SELECT_MODEL,
  SELECT_NESTED,
} from '../actions/act_data';

const data = (state = { selectPage: 1, nested: [] }, action = {}) => {
  switch (action.type) {
    case REQUEST_DATA:
      return {
        ...state,
        isFetching: true,
        select: {},
        error: [],
      };
    case RECEIVE_DATA:
      return {
        ...state,
        isFetching: false,
        data: action.payload,
        error: [],
      };
    case RECEIVE_PAGE:
      return {
        ...state,
        isFetching: false,
        page: action.payload,
        error: [],
      };
    case RECEIVE_ERROR:
      return {
        ...state,
        isFetching: false,
        select: {},
        error: action.payload,
      };
    case SELECT_DATA:
      return {
        ...state,
        isFetching: false,
        select: action.payload,
        error: [],
      };
    case SELECT_PAGE:
      return {
        ...state,
        isFetching: false,
        selectPage: action.payload,
        error: [],
      };
    case SELECT_MODEL:
      return {
        ...state,
        model: action.payload,
        error: [],
      };
    case SELECT_NESTED:
      return {
        ...state,
        nested: action.payload,
        error: [],
      };
    default:
      return state;
  }
};

export default data;
