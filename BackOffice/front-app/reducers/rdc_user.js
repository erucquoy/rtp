import {
  LOG_OUT,
  RECEIVE_USER_DATA,
  REQUEST_USER_DATA,
} from '../actions/act_user';

const user = (state = {}, action = {}) => {
  switch (action.type) {
    case LOG_OUT:
      return {
      };
    case REQUEST_USER_DATA:
      return {
        ...state,
        isFetching: true,
      };
    case RECEIVE_USER_DATA:
      return {
        ...state,
        isFetching: false,
        data: action.payload,
      };
    default:
      return state;
  }
};

export default user;
