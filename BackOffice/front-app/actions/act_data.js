import axios from 'axios';
import { baseUrlApi, nbPerPage, computeLimit } from '../config/config';
import struct from '../config/struct';

export const REQUEST_DATA = 'REQUEST_EVENT_DATA';
export const RECEIVE_DATA = 'RECEIVE_EVENT_DATA';
export const RECEIVE_ERROR = 'RECEIVE_EVENT_ERROR';
export const RECEIVE_PAGE = 'RECEIVE_EVENT_PAGE';
export const SELECT_DATA = 'SELECT_DATA';
export const SELECT_PAGE = 'SELECT_PAGE';
export const SELECT_MODEL = 'SELECT_MODEL';
export const SELECT_NESTED = 'SELECT_NESTED';

export const requestData = () => ({
  type: REQUEST_DATA,
});

export const receiveData = (data) => ({
  type: RECEIVE_DATA,
  payload: data,
});

export const receivePage = (page) => ({
  type: RECEIVE_PAGE,
  payload: page,
});

export const receiveError = (data) => ({
  type: RECEIVE_ERROR,
  payload: data,
});

export const selectData = (data) => ({
  type: SELECT_DATA,
  payload: data,
});

export const selectPage = (page) => ({
  type: SELECT_PAGE,
  payload: page,
});

export const selectModel = (model) => ({
  type: SELECT_MODEL,
  payload: model,
});

export const selectNested = (data) => ({
  type: SELECT_NESTED,
  payload: data,
});

export const setView = (id) => (
  (dispatch, getState) => {
    const model = getState().data.model;
    dispatch(requestData());
    axios.get(
      `${baseUrlApi}${struct[model].api}/${id}`
    ).then(response => {
      dispatch(selectData(response.data));
    });
  }
);

export const getData = (model, page, callback) => (
  (dispatch, getState) => {
    dispatch(selectModel(model));
    dispatch(requestData());
    dispatch(selectPage(page));
    let temp = getState().data.nested[getState().data.nested.length - 1];
    temp = struct[model].nested == true ? `&${temp.name}=${temp.id}` : '';
    axios.get(
      baseUrlApi + struct[model].api + computeLimit(page, nbPerPage) + temp
    ).then(response => {
      dispatch(receivePage(Math.ceil(response.data.count / nbPerPage)));
      dispatch(receiveData(response.data.data));
      callback();
    });
  }
);

export const addData = (add, callback) => (
  (dispatch, getState) => {
    const model = getState().data.model;
    axios.post(
      baseUrlApi + struct[model].api,
      {
        ...add,
      }
    ).then(() => {
      callback();
    }).catch((error) => {
      dispatch(receiveError(error.response.data.message));
    });
  }
);

export const updateData = (update, callback) => (
  (dispatch, getState) => {
    const model = getState().data.model;
    axios.put(
      `${baseUrlApi}${struct[model].api}/${update[struct[model].id]}`,
      {
        ...update,
      }
    ).then(() => {
      callback();
    });
  }
);

export const deleteData = (id, callback) => (
  (dispatch, getState) => {
    const model = getState().data.model;
    axios.delete(
      `${baseUrlApi}${struct[model].api}/${id}`
    ).then(() => {
      callback();
    });
  }
);
