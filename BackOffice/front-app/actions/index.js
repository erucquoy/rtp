import {
  setPageTitle,
} from './act_main_ui';

import {
  loginUser,
  loginJWTUser,
  logoutUser,
  isConnected,
} from './act_user';

import {
  getData,
  setView,
  addData,
  updateData,
  selectModel,
  deleteData,
  selectNested,
} from './act_data';

export {
  setPageTitle,
  loginUser,
  loginJWTUser,
  logoutUser,
  isConnected,
  getData,
  setView,
  addData,
  updateData,
  selectModel,
  deleteData,
  selectNested,
};
