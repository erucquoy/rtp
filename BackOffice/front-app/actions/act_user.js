import axios from 'axios';
import { baseUrlApi, userUriLogin, userUriCheckConnected } from '../config/config';

export const LOG_IN = 'LOG_IN';
export const LOG_OUT = 'LOG_OUT';
export const REQUEST_USER_DATA = 'REQUEST_USER_DATA';
export const RECEIVE_USER_DATA = 'RECEIVE_USER_DATA';

export const logIn = () => ({
  type: LOG_IN,
});

export const logOut = () => ({
  type: LOG_OUT,
});

export const receiveUserData = (data) => ({
  type: RECEIVE_USER_DATA,
  payload: data,
});

export const isConnected = (callback) => {
  axios.get(baseUrlApi + userUriCheckConnected).then(response => callback(!response.error)).catch(() => callback(false));
};

export const loginUser = (email, password) => (
  dispatch => {
    dispatch(logIn());

    axios.post(
      baseUrlApi + userUriLogin,
      {
        email,
        password,
      }
    ).then(response => {
      if (response.data.error == false) {
        axios.interceptors.request.use((config) => {
          config.headers.Authorization = response.data.message.access_token;
          return config;
        });
        localStorage.setItem('tokenJWT', response.data.message.access_token);
        dispatch(receiveUserData({ fail: false, message: '' }));
      } else {
        dispatch(receiveUserData({
          fail: true,
          message: 'Mauvais identifiants',
        }));
      }
    }).catch(() =>
      dispatch(receiveUserData({
        fail: true,
        message: 'Mauvais identifiants',
      }))
    );
  }
);

export const logoutUser = () => (
  dispatch => {
    axios.interceptors.request.use((config) => {
      config.headers.Authorization = '';
      return config;
    });
    localStorage.removeItem('tokenJWT');
    dispatch(logOut());
  }
);

export const loginJWTUser = (jwtToken) => (
  dispatch => {
    dispatch(logIn());

    if (jwtToken != undefined) {
      axios.interceptors.request.use((config) => {
        config.headers.Authorization = localStorage.getItem('tokenJWT');
        return config;
      });
      isConnected((connected) => {
        if (connected) {
          dispatch(receiveUserData({ fail: false, message: '' }));
        }
      });
    }
  }
);
