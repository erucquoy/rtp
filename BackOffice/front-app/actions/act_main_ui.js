export const SET_PAGE_TITLE = 'SET_PAGE_TITLE';

export const setPageTitle = (pageTitle) => {
  document.title = pageTitle;

  return {
    type: SET_PAGE_TITLE,
    payload: pageTitle,
  };
};
