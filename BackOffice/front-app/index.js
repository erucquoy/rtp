import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import { Provider } from 'react-redux';
import { createStore, compose, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import axios from 'axios';

import BackofficeApp from './reducers';

import LoginContainer from './containers/LoginContainer';
import MainContainer from './containers/layout/MainContainer';
import HomeContainer from './containers/HomeContainer';
import ListContainer from './containers/ListContainer';
import ViewContainer from './containers/ViewContainer';
import EditContainer from './containers/EditContainer';
import AddContainer from './containers/AddContainer';


import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-material-design/dist/css/bootstrap-material-design.css';
import 'bootstrap-material-design/dist/css/ripples.css';
import 'font-awesome/css/font-awesome.css';

import 'jquery';
import 'bootstrap';
import 'bootstrap-material-design/dist/js/material';
import 'bootstrap-material-design/dist/js/ripples';

$.material.init();

const finalCreateStore = compose(
  applyMiddleware(thunkMiddleware),
  window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore);

let store = finalCreateStore(BackofficeApp);

if (localStorage.getItem('tokenJWT') != undefined) {
  axios.interceptors.request.use((config) => {
    config.headers.Authorization = localStorage.getItem('tokenJWT');
    return config;
  });
}

const App = () => (
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/login" component={LoginContainer} />
      <Route path="/" component={MainContainer}>
        <IndexRoute component={HomeContainer} />
        <Route path="/:model" component={ListContainer} />
        <Route path="/:model/view" component={ViewContainer} />
        <Route path="/:model/edit" component={EditContainer} />
        <Route path="/:model/add" component={AddContainer} />
      </Route>
    </Router>
  </Provider>
);

ReactDOM.render(<App />, document.getElementById('app-backoffice'));
