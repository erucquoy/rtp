module.exports = {
  baseUrlApi: 'http://delta.roadtopermis.com:9982/api/v1/', // http://vps2.bastien-robert.xyz:10000/api/v1/',
  imageUriApi: 'admin/image',
  userUriLogin: 'authenticate',
  userUriCheckConnected: 'token',
  nbPerPage: 10,
  computeLimit: (page, nbPerPage) => `?limit=${nbPerPage}&offset=${(page - 1) * nbPerPage}`,
};
