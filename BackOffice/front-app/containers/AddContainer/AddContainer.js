import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { reduxForm } from 'redux-form';

import Form from '../../components/Form';

import { setPageTitle, addData, getData, setView, selectNested } from '../../actions';
import struct from '../../config/struct';

class AddContainer extends Component {
  componentDidMount() {
    this.props.setPageTitle('');
    this.props.getData(this.props.params.model);
  }

  componentWillReceiveProps(props) {
    props.setPageTitle('');
  }

  handleForm(value) {
    this.props.addData(value, () => {
      this.props.getData(this.props.params.model, () => {});
      this.context.router.replace({
        pathname: `/${this.props.params.model}/`,
      });
    });
  }

  handleClickNested(name, id) {
    const temp = this.props.nested;
    for (let i = 0; i < temp.length; i++) {
      if (name == temp[i].name) {
        temp.splice(i, temp.length - i);
      }
    }
    this.props.selectNested(temp);

    if (id) {
      this.props.getData(name, 1);
      this.props.setView(id);
    }

    this.context.router.replace({
      pathname: !id ? `/${name}/` : `/${name}/view`,
    });
  }

  computeBreadCrumb() {
    return (
      <ol className="breadcrumb">
        <li><Link to="/">Administration</Link></li>
        {
          this.props.nested.map(t => [
            <li><a onClick={() => this.handleClickNested(t.name)}>{t.name}</a></li>,
            <li><a onClick={() => this.handleClickNested(t.name, t.id)}>{t.title}</a></li>,
          ])
        }
        <li><Link to={`/${this.props.params.model}/`}>{struct[this.props.params.model].name}</Link></li>
        <li className="active">Ajout</li>
      </ol>
    );
  }

  render() {
    const dataStruct = struct[this.props.params.model];

    return (
      <div>
        {this.computeBreadCrumb()}

        <h1>Ajout d'{`${dataStruct.un} ${dataStruct.name}`}</h1>

        <div className="well bs-component">
          <form
            className="form-horizontal"
            onSubmit={this.props.handleSubmit(this.handleForm.bind(this))}
          >
            <fieldset>

              <legend>Formulaire d'ajout d'{`${dataStruct.un} ${dataStruct.name}`}</legend>

              <Form dataStruct={dataStruct.struct} />

            </fieldset>
            <div className="text-center">
              <button type="submit" className="btn btn-primary btn-raised">Ajouter</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

AddContainer.contextTypes = {
  router: PropTypes.object,
};

AddContainer.propTypes = {
  setPageTitle: PropTypes.func,
  getData: PropTypes.func,
  params: PropTypes.object,
  handleSubmit: PropTypes.func,
  addData: PropTypes.func,
  error: PropTypes.array,
  nested: PropTypes.array,
  selectNested: PropTypes.func,
  setView: PropTypes.func,
};

const mapStateToProps = (state) => ({
  nested: state.data.nested,
});

const mapDispatchToProps = (dispatch) => ({
  setPageTitle: (title) => dispatch(setPageTitle(title)),
  addData: (update, callback) => dispatch(addData(update, callback)),
  getData: (update, callback) => dispatch(getData(update, callback)),
  setView: (id) => dispatch(setView(id)),
  selectNested: (nested) => dispatch(selectNested(nested)),
});

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
  form: 'add',
})(AddContainer));
