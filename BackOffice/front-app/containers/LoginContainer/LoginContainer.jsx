import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Login from '../../components/user/Login';

import {
  loginUser,
  loginJWTUser,
  isConnected,
  setPageTitle,
} from '../../actions';

class LoginContainer extends Component {
  componentWillMount() {
    this.props.onSetPageTitle('');

    if (localStorage.getItem('tokenJWT') != undefined) {
      this.props.loginJWTUser(this.props.params.jwtToken);

      isConnected((connected) => {
        if (connected) {
          this.context.router.replace({
            pathname: '/',
          });
        }
      });
    }
  }

  componentWillReceiveProps() {
    isConnected((connected) => {
      if (connected) {
        this.context.router.replace({
          pathname: '/',
        });
      }
    });
  }

  render() {
    return (
      <Login
        user={this.props.user}
        onSubmitHandler={this.props.onSubmitHandler}
      />
    );
  }
}

LoginContainer.contextTypes = {
  router: PropTypes.object,
};

LoginContainer.propTypes = {
  onSetPageTitle: PropTypes.func,
  onSubmitHandler: PropTypes.func,
  params: PropTypes.object,
  loginJWTUser: PropTypes.func,
  user: PropTypes.object,
};

const mapStateToProps = (state) => ({
  user: state.user.data,
});

const mapDispatchToProps = (dispatch) => ({
  onSetPageTitle: (title) =>
    dispatch(setPageTitle(title)),
  onSubmitHandler: (value) =>
    dispatch(loginUser(value.email, value.password)),
  loginJWTUser: (jwtToken) =>
    dispatch(loginJWTUser(jwtToken)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
