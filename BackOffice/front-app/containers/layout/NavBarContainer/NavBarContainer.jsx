import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import struct from '../../../config/struct';

import {
  logoutUser,
} from '../../../actions';

const NavBarContainer = (props) => (
  <nav className="navbar navbar-inverse navbar-fixed-top">
    <div className="container">
      <div className="navbar-header">
        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span className="sr-only">Toggle navigation</span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
        </button>
        <Link className="navbar-brand" to="/">Administration</Link>
      </div>
      <div id="navbar" className="collapse navbar-collapse">
        <ul className="nav navbar-nav">
          <li className="active"><Link to="/">Accueil</Link></li>
          {Object.keys(struct).map((t, i) => {
            if (!struct[t].nested) {
              return (
                <li key={`menu${i}`}>
                  <Link to={`/${t}/`}>{struct[t].names.charAt(0).toUpperCase() + struct[t].names.slice(1)}</Link>
                </li>
              );
            }
            return (<li key={`item${i}`}></li>);
          })}
          <li>
            <a
              onClick={() => {
                props.logout();
                props.redirect.replace({
                  pathname: '/login',
                });
              }}
            >
              Déconnexion
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
);

NavBarContainer.propTypes = {
  pageTitle: PropTypes.string,
  logout: PropTypes.func,
  redirect: PropTypes.object,
};

const mapStateToProps = (state) => ({
  pageTitle: state.ui.pageTitle,
});

const mapDispatchToProps = (dispatch) => ({
  logout: () =>
    dispatch(logoutUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(NavBarContainer);
