import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

const computeHeader = (pageTitle) => {
  if (pageTitle == '' || pageTitle == undefined) {
    return (<div></div>);
  }

  return (
    <div className="content-head">
      <h3>{pageTitle}</h3>
      <div className="overlay dark-overlay"></div>
    </div>
  );
};

const ContentContainer = ({ children, pageTitle }) => (
  <div className="container master-container">
    {computeHeader(pageTitle)}
    {/* Content */}
    {children}
  </div>
);

ContentContainer.propTypes = {
  children: PropTypes.node,
  pageTitle: PropTypes.string,
};

const mapStateToProps = (state) => ({
  pageTitle: state.ui.pageTitle,
});

export default connect(mapStateToProps)(ContentContainer);

