import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import NavBarContainer from '../NavBarContainer';
import ContentContainer from '../ContentContainer';

import {
  isConnected,
} from '../../../actions';

class MainContainer extends Component {
  componentWillMount() {
    isConnected((connected) => {
      if (!connected) {
        this.context.router.replace({
          pathname: '/login',
        });
      }
    });
  }

  componentWillReceiveProps() {
    isConnected((connected) => {
      if (!connected) {
        this.context.router.replace({
          pathname: '/login',
        });
      }
    });
  }

  render() {
    return (
      <div id="mainContainer">
        {/* Top Nav Bar */}
        <NavBarContainer redirect={this.context.router} />
        {/* Content */}
        <ContentContainer>
          {this.props.children}
        </ContentContainer>
      </div>
    );
  }
}

MainContainer.contextTypes = {
  router: PropTypes.object,
};

MainContainer.propTypes = {
  children: PropTypes.node,
  nested: PropTypes.array,
  params: PropTypes.object,
};

const mapStateToProps = (state) => ({
  userData: state.user,
  nested: state.data.nested,
});

export default connect(mapStateToProps)(MainContainer);
