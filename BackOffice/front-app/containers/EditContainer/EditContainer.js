import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { reduxForm } from 'redux-form';

import Form from '../../components/Form';

import { setPageTitle, getData, updateData, setView, selectNested } from '../../actions';
import struct from '../../config/struct';

class EditContainer extends Component {
  componentDidMount() {
    this.props.setPageTitle('');
    if (!this.props.isFetching && !this.props.initialValues) {
      this.context.router.replace({
        pathname: '/',
      });
    }
  }

  componentWillReceiveProps(props) {
    props.setPageTitle('');
  }

  handleForm(value) {
    this.props.updateData(value, () => {
      this.props.setView(this.props.initialValues[struct[this.props.params.model].id]);
      this.context.router.replace({
        pathname: `/${this.props.params.model}/view`,
      });
    });
  }

  handleClickNested(name, id) {
    const temp = this.props.nested;
    for (let i = 0; i < temp.length; i++) {
      if (name == temp[i].name) {
        temp.splice(i, temp.length - i);
      }
    }
    this.props.selectNested(temp);

    if (id) {
      this.props.getData(name, 1);
      this.props.setView(id);
    }

    this.context.router.replace({
      pathname: !id ? `/${name}/` : `/${name}/view`,
    });
  }

  computeBreadCrumb() {
    return (
      <ol className="breadcrumb">
        <li><Link to="/">Administration</Link></li>
        {
          this.props.nested.map(t => [
            <li><a onClick={() => this.handleClickNested(t.name)}>{t.name}</a></li>,
            <li><a onClick={() => this.handleClickNested(t.name, t.id)}>{t.title}</a></li>,
          ])
        }
        <li><Link to={`/${this.props.params.model}/`}>{struct[this.props.params.model].name}</Link></li>
        <li><Link to={`/${this.props.params.model}/view`}>{this.props.initialValues ? this.props.initialValues[struct[this.props.params.model].title] : ''}</Link></li>
        <li className="active">Edition</li>
      </ol>
    );
  }

  render() {
    const dataStruct = struct[this.props.params.model];

    return (
      <div>
        {this.computeBreadCrumb()}

        <h1>Edition de {this.props.initialValues ? this.props.initialValues[dataStruct.title] : ''}</h1>

        <div className="well bs-component">
          <form
            className="form-horizontal"
            onSubmit={this.props.handleSubmit(this.handleForm.bind(this))}
          >
            <fieldset>

              <legend>Formulaire d'edition d'{`${dataStruct.un} ${dataStruct.name}`}</legend>

              <Form dataStruct={dataStruct.struct} />

            </fieldset>
            <div className="text-center">
              <button type="submit" className="btn btn-primary btn-raised">Modifier</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

EditContainer.contextTypes = {
  router: PropTypes.object,
};

EditContainer.propTypes = {
  setPageTitle: PropTypes.func,
  getData: PropTypes.func,
  initialValues: PropTypes.object,
  isFetching: PropTypes.bool,
  params: PropTypes.object,
  handleSubmit: PropTypes.func,
  updateData: PropTypes.func,
  setView: PropTypes.func,
  selectNested: PropTypes.func,
  nested: PropTypes.array,
};

const mapStateToProps = (state) => ({
  initialValues: state.data.select,
  isFetching: state.data.isFetching,
  nested: state.data.nested,
});

const mapDispatchToProps = (dispatch) => ({
  setPageTitle: (title) => dispatch(setPageTitle(title)),
  getData: (model) => dispatch(getData(model)),
  updateData: (update, callback) => dispatch(updateData(update, callback)),
  setView: (id) => dispatch(setView(id)),
  selectNested: (nested) => dispatch(selectNested(nested)),
});

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
  form: 'edit',
})(EditContainer));
