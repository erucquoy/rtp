import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Pagination } from 'react-bootstrap';
import axios from 'axios';

import FormatData from '../../components/FormatData';

import { setPageTitle, getData, setView, selectNested } from '../../actions';
import struct from '../../config/struct';
import { baseUrlApi } from '../../config/config';

class ListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = { dataStruct: struct[this.props.params.model] };
  }
  componentDidMount() {
    this.props.setPageTitle('');
    this.props.getData(this.props.params.model, 1);
    if (!this.props.params.model || !struct[this.props.params.model]) {
      this.context.router.replace({
        pathname: '/',
      });
    }
  }

  componentWillReceiveProps(props) {
    props.setPageTitle('');
    this.setState({ dataStruct: struct[props.params.model] });
    if (this.props.params.model != props.params.model) {
      props.getData(props.params.model, props.params.selectPage);
    }
  }

  handleClickNested(name, id) {
    const temp = this.props.nested;
    for (let i = 0; i < temp.length; i++) {
      if (name == temp[i].name) {
        temp.splice(i, temp.length - i);
      }
    }
    this.props.selectNested(temp);

    if (id) {
      this.props.getData(name, 1);
      this.props.setView(id);
    }

    this.context.router.replace({
      pathname: !id ? `/${name}/` : `/${name}/view`,
    });
  }

  computeBreadCrumb() {
    return (
      <ol className="breadcrumb">
        <li><Link to="/">Administration</Link></li>
        {
          this.props.nested.map(t => [
            <li><a onClick={() => this.handleClickNested(t.name)}>{t.name}</a></li>,
            <li><a onClick={() => this.handleClickNested(t.name, t.id)}>{t.title}</a></li>,
          ])
        }
        <li className="active">{struct[this.props.params.model].name}</li>
      </ol>
    );
  }

  render() {
    let head = [];
    let line;
    const nameVar = [];
    let show = true;
    let dataStruct;

    if (this.state && this.state.dataStruct && this.state.dataStruct.struct && this.state.dataStruct.struct.length > 0) {
      dataStruct = this.state.dataStruct;
    } else {
      dataStruct = { struct: [] };
      show = false;
    }

    for (let i = 0; i < dataStruct.struct.length; i++) {
      if (dataStruct.struct[i].type == 'select' && typeof dataStruct.struct[i].values == 'string') {
        show = false;
        axios.get(
          baseUrlApi + dataStruct.struct[i].values
        ).then(response => {
          const temp = dataStruct;
          temp.struct[i].values = response.data.data;
          for (let j = 0; j < temp.struct[i].values.length; j++) {
            temp.struct[i].values[j] = {
              display: temp.struct[i].values[j][dataStruct.struct[i].data.display],
              value: temp.struct[i].values[j][dataStruct.struct[i].data.value],
            };
          }
          this.setState({ dataStruct: temp });
        });
      }

      if (dataStruct.struct[i].showTable) {
        head.push(<th key={`head${i}`}>{dataStruct.struct[i].display}</th>);
        nameVar.push(dataStruct.struct[i]);
      }
    }

    if (!show) {
      return (<div></div>);
    }

    if (this.props.data != undefined) {
      line = this.props.data.map((i, t) => (
        <tr
          key={`line${t}`}
          onClick={() => {
            this.props.setView(i[dataStruct.id]);
            this.context.router.replace({
              pathname: `/${this.props.params.model}/view`,
            });
          }}
        >
          {nameVar.map(k => (<td key={`column${k.name}line${t}`}><FormatData data={i[k.name]} struct={k} /></td>))}

        </tr>
      ));
    } else {
      line = (<tr></tr>);
    }

    return (
      <div>
        {this.computeBreadCrumb()}
        {/* <ol className="breadcrumb">
          <li><Link to="/">Administration</Link></li>
          <li className="active">{dataStruct.name}</li>
        </ol> */}

        <h1>Gestion des {dataStruct.names}</h1>

        {dataStruct.add ? <Link to={`/${this.props.params.model}/add`} className="btn btn-primary btn-raised">Ajouter {`${dataStruct.un} ${dataStruct.name}`}</Link> : <span></span>}

        <hr />

        <table className="table table-striped">
          <thead>
            <tr>
              {head}
            </tr>
          </thead>

          <tbody>
            {line}
          </tbody>

        </table>

        <Pagination
          className=""
          prev
          next
          first
          last
          ellipsis
          items={this.props.page}
          activePage={this.props.selectPage}
          onSelect={(page) => this.props.getData(this.props.params.model, page)}
        />
      </div>
    );
  }
}

ListContainer.contextTypes = {
  router: PropTypes.object,
};

ListContainer.propTypes = {
  setPageTitle: PropTypes.func,
  getData: PropTypes.func,
  setView: PropTypes.func,
  data: PropTypes.array,
  params: PropTypes.object,
  selectPage: PropTypes.number,
  page: PropTypes.number,
  nested: PropTypes.array,
  selectNested: PropTypes.func,
};

const mapStateToProps = (state) => ({
  data: state.data.data,
  selectPage: state.data.selectPage,
  page: state.data.page,
  nested: state.data.nested,
});

const mapDispatchToProps = (dispatch) => ({
  setPageTitle: (title) => dispatch(setPageTitle(title)),
  getData: (model, page) => dispatch(getData(model, page)),
  setView: (id) => dispatch(setView(id)),
  selectNested: (nested) => dispatch(selectNested(nested)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ListContainer);
