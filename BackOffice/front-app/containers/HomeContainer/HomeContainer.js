import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import { setPageTitle } from '../../actions';

import struct from '../../config/struct';

class HomeContainer extends Component {
  componentDidMount() {
    this.props.setPageTitle('');
  }

  componentWillReceiveProps(props) {
    props.setPageTitle('');
  }

  render() {
    return (
      <div>
        <ol className="breadcrumb">
          <li><a href="/admin/">Administration</a></li>
        </ol>

        <h1>Administration</h1>

        <hr />

        <div className="col-md-6 col-md-offset-3 text-center">
          <div className="btn-group-vertical text-center">
            {Object.keys(struct).map((t, i) => {
              if (!struct[t].nested) {
                return (<Link key={`item${i}`} to={`/${t}/`} className="btn btn-primary btn-raised">{`Gestion des ${struct[t].names}`}</Link>);
              }
              return (<span key={`item${i}`}></span>);
            })}
          </div>
        </div>

      </div>
    );
  }
}

HomeContainer.contextTypes = {
  router: PropTypes.object,
};

HomeContainer.propTypes = {
  setPageTitle: PropTypes.func,
};

const mapStateToProps = () => ({
});

const mapDispatchToProps = (dispatch) => ({
  setPageTitle: (title) => dispatch(setPageTitle(title)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
