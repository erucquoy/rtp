import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import FormatData from '../../components/FormatData';

import { setPageTitle, getData, deleteData, selectNested, setView } from '../../actions';
import struct from '../../config/struct';

class ViewContainer extends Component {
  componentDidMount() {
    this.props.setPageTitle('');
    if (!this.props.isFetching && !this.props.data) {
      this.context.router.replace({
        pathname: '/',
      });
    }
  }

  componentWillReceiveProps(props) {
    props.setPageTitle('');
  }

  handleDelete() {
    this.props.deleteData(this.props.data[struct[this.props.params.model].id], () => {
      this.props.getData(this.props.params.model, () => {});
      this.context.router.replace({
        pathname: `/${this.props.params.model}/`,
      });
    });
  }

  handleNested(e) {
    const temp = this.props.nested;
    temp.push({ name: this.props.params.model, id: this.props.data[struct[this.props.params.model].id], title: this.props.data[struct[this.props.params.model].title] });
    this.props.selectNested(temp);
    this.context.router.replace({
      pathname: `/${e}/`,
    });
  }

  handleClickNested(name, id) {
    const temp = this.props.nested;
    for (let i = 0; i < temp.length; i++) {
      if (name == temp[i].name) {
        temp.splice(i, temp.length - i);
      }
    }
    this.props.selectNested(temp);

    if (id) {
      this.props.getData(name, 1);
      this.props.setView(id);
    }

    this.context.router.replace({
      pathname: !id ? `/${name}/` : `/${name}/view`,
    });
  }

  computeBreadCrumb() {
    return (
      <ol className="breadcrumb">
        <li><Link to="/">Administration</Link></li>
        {
          this.props.nested.map(t => [
            <li><a onClick={() => this.handleClickNested(t.name)}>{t.name}</a></li>,
            <li><a onClick={() => this.handleClickNested(t.name, t.id)}>{t.title}</a></li>,
          ])
        }
        <li><Link to={`/${this.props.params.model}/`}>{struct[this.props.params.model].name}</Link></li>
        <li className="active">{this.props.data ? this.props.data[struct[this.props.params.model].title] : ''}</li>
      </ol>
    );
  }

  render() {
    let line;
    const dataStruct = struct[this.props.params.model];

    if (this.props.data != undefined) {
      line = dataStruct.struct.map((i, t) => (
        <div key={`line${t}`}>
          <dt>{i.display}</dt>
          <dd><FormatData data={this.props.data[i.name]} struct={i} /></dd>
        </div>
      ));
    } else {
      line = (<div></div>);
    }

    return (
      <div>
        {this.computeBreadCrumb()}

        <h1>Gestion de {this.props.data ? this.props.data[dataStruct.title] : ''}</h1>

        <div className="row">
          <div className="col-md-6">
            <h2>Informations générales</h2>
            {dataStruct.editable == false ? '' : <Link to={`/${this.props.params.model}/edit`} className="btn btn-primary btn-raised">Éditer</Link>}
            {dataStruct.delete ? <div className="btn btn-danger btn-raised" onClick={() => this.handleDelete()}>Supprimer</div> : <span></span>}
            <hr />
            <dl>
              {line}
            </dl>
          </div>
          <div className="col-md-6">
            {dataStruct.nestedStruct.map((t, i) => (
              <div key={`item${i}`} onClick={() => this.handleNested(t)} className="btn btn-primary btn-raised">{`Gestion des ${struct[t].names}`}</div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

ViewContainer.contextTypes = {
  router: PropTypes.object,
};

ViewContainer.propTypes = {
  setPageTitle: PropTypes.func,
  deleteData: PropTypes.func,
  getData: PropTypes.func,
  data: PropTypes.object,
  isFetching: PropTypes.bool,
  params: PropTypes.object,
  nested: PropTypes.array,
  setView: PropTypes.func,
  selectNested: PropTypes.func,
};

const mapStateToProps = (state) => ({
  data: state.data.select,
  isFetching: state.data.isFetching,
  nested: state.data.nested,
});

const mapDispatchToProps = (dispatch) => ({
  setPageTitle: (title) => dispatch(setPageTitle(title)),
  deleteData: (model, callback) => dispatch(deleteData(model, callback)),
  getData: (update, callback) => dispatch(getData(update, callback)),
  setView: (id) => dispatch(setView(id)),
  selectNested: (nested) => dispatch(selectNested(nested)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewContainer);
