import React, { PropTypes } from 'react';
import { Field } from 'redux-form';

const CheckBox = ({ name, display, disabled, required }) => (
  <div className="form-group is-empty">
    <label htmlFor={`input${name}`} className="control-label">{display}</label>
    <Field
      component="input"
      name={name}
      type="checkbox"
      className="form-control"
      id={`input${name}`}
      disabled={disabled}
      required={required}
    />
  </div>
);

CheckBox.propTypes = {
  name: PropTypes.string,
  display: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
};

export default CheckBox;

