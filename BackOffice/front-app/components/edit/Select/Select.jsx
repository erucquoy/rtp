import React, { PropTypes } from 'react';
import { Field } from 'redux-form';

const Select = ({ name, display, values, disabled, required }) => (
  <div className="form-group is-empty">
    <label htmlFor={`input${name}`} className="control-label">{display}</label>
    <Field
      component="select"
      name={name}
      className="form-control"
      id={`input${name}`}
      required={required}
      disabled={disabled}
    >
      {values.map((t, i) => (<option value={t.value} key={name + i}>{t.display}</option>))}
    </Field>
  </div>
);

Select.propTypes = {
  name: PropTypes.string,
  display: PropTypes.string,
  values: PropTypes.array,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
};

export default Select;

