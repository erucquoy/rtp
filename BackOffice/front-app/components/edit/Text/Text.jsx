import React, { PropTypes } from 'react';
import { Field } from 'redux-form';

const String = ({ name, display, disabled, required }) => (
  <div className="form-group is-empty">
    <label htmlFor={`input${name}`} className="control-label">{display}</label>
    <Field
      component="textarea"
      name={name}
      type="text"
      className="form-control"
      id={`input${name}`}
      required={required}
      disabled={disabled}
    />
  </div>
);

String.propTypes = {
  name: PropTypes.string,
  display: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
};

export default String;

