import React, { Component, PropTypes } from 'react';
import axios from 'axios';
import DropzoneComponent from 'react-dropzone-component';
import { Field } from 'redux-form';

import { baseUrlApi, imageUriApi } from '../../../config/config';

class FileInput extends Component {
  constructor(props) {
    super(props);

    this.djsConfig = {
      addRemoveLinks: true,
      acceptedFiles: 'image/jpeg,image/png,image/gif',
      autoProcessQueue: true,
      paramName: 'file',
      maxFilesize: 5,
      dictDefaultMessage: 'Déposez ici vos images pour upload',
      dictRemoveFile: 'Supprimer',
      init: function init() {
        for (let i = 0; i < props.input.value.length; i++) {
          const thisDropzone = this;
          const mockFile = { name: props.input.value[i].split('/').pop(), size: 0 };
          thisDropzone.options.addedfile.call(thisDropzone, mockFile);
          thisDropzone.options.thumbnail.call(thisDropzone, mockFile, baseUrlApi + props.input.value[i]);
          thisDropzone.options.complete.call(thisDropzone, mockFile);
        }
      },
    };

    this.componentConfig = {
      iconFiletypes: ['.jpg', '.png', '.gif'],
      showFiletypeIcon: true,
      postUrl: baseUrlApi + imageUriApi,
    };

    this.state = { files: [] };
  }

  success(file) {
    const temp = this.state && this.state.files ? this.state.files : [];
    temp.push(JSON.parse(file.xhr.response).id);
    this.setState({ files: temp });
    this.props.input.onChange(temp);
  }

  removedfile(file) {
    let temp = this.state && this.state.files ? this.state.files : [];
    let id;
    if (file.xhr != undefined) {
      id = JSON.parse(file.xhr.response).id;
    } else {
      id = file.name;
    }

    for (let i = 0; i < temp.length; i++) {
      if (temp[i] == id) {
        temp = temp.slice(i, 1);
      }
    }
    this.setState({ files: temp });
    this.props.input.onChange(temp);
    axios.delete(
      `${baseUrlApi}${imageUriApi}/${id}`
    ).then(() => {
    });
  }

  render() {
    const config = this.componentConfig;
    const djsConfig = this.djsConfig;

    const eventHandlers = {
      success: this.success.bind(this),
      removedfile: this.removedfile.bind(this),
    };

    return <DropzoneComponent config={config} eventHandlers={eventHandlers} djsConfig={djsConfig} />;
  }
}

FileInput.propTypes = {
  input: PropTypes.object,
};

export default props => <Field {...props} component={FileInput} />;

