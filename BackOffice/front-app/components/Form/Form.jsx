import React, { PropTypes } from 'react';
import { connect } from 'react-redux';

import String from '../edit/String';
import Text from '../edit/Text';
import CheckBox from '../edit/CheckBox';
import Select from '../edit/Select';
import Image from '../edit/Image';
import Date from '../edit/Date';


const Form = ({ dataStruct, error }) => (
  <div>
    {(() => {
      if (error && error.length > 0) {
        return (
          <div className="alert alert-danger">
            <ul>
              {error.map((t, i) => (<li key={`error${i}`}>{t.msg}</li>))}
            </ul>
          </div>
        );
      }
      return (<div></div>);
    })()}
    {
      dataStruct.map((t, i) => {
        switch (t.type) {
          case 'string':
            return (<div key={`input${i}`}><String name={t.name} display={t.display} disabled={t.editable == false} required={t.required != false} /></div>);
          case 'text':
            return (<div key={`input${i}`}><Text name={t.name} display={t.display} disabled={t.editable == false} required={t.required != false} /></div>);
          case 'boolean':
            return (<div key={`input${i}`}><CheckBox name={t.name} display={t.display} disabled={t.editable == false} required={t.required != false} /></div>);
          case 'select':
            return (<div key={`input${i}`}><Select name={t.name} display={t.display} values={t.values} disabled={t.editable == false} required={t.required != false} /></div>);
          case 'images':
            return (<div key={`input${i}`}><Image name={t.name} display={t.display} disabled={t.editable == false} required={t.required != false} /></div>);
          case 'youtubeLink':
            return (<div key={`input${i}`}><String name={t.name} display={t.display} disabled={t.editable == false} required={t.required != false} /></div>);
          case 'date':
            return (<div key={`input${i}`}><Date name={t.name} display={t.display} disabled={t.editable == false} required={t.required != false} /></div>);
          default:
            return (<div key={`input${i}`}></div>);
        }
      })
    }
  </div>
);

Form.propTypes = {
  dataStruct: PropTypes.array,
  error: PropTypes.array,
};

const mapStateToProps = (state) => ({
  error: state.data.error,
});


export default connect(mapStateToProps)(Form);

