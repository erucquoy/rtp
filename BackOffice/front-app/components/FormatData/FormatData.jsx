import React, { PropTypes } from 'react';
import YouTube from 'react-youtube';

import { baseUrlApi } from '../../config/config';

const FormatData = ({ data, struct }) => {
  const regexYoutube = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&\?]*).*/;
  let matchYoutube;

  switch (struct.type) {
    case 'boolean':
      return (<span>{data ? 'Oui' : 'Non'}</span>);
    case 'select':
      for (let i = 0; i < struct.values.length; i++) {
        if (struct.values[i].value == data) {
          return (<span>{struct.values[i].display}</span>);
        }
      }
      return (<span>{data}</span>);
    case 'youtubeLink':
      if (!data) {
        return (<span></span>);
      }
      matchYoutube = data.match(regexYoutube);
      return (
        <span>
          <YouTube
            videoId={(matchYoutube && matchYoutube[7].length == 11) ? matchYoutube[7] : false}
            opts={{
              height: '390',
              width: '640',
            }}
          />
        </span>);
    case 'images':
      if (!data || data.length == 0) {
        return (<span></span>);
      }

      return (
        <span>
          {data.map((t, i) => <img key={`image${i}`} src={baseUrlApi + t} className="display-image" alt="item" />)}
        </span>
      );
    default:
      if (!data) {
        return (<span></span>);
      }
      return (<span dangerouslySetInnerHTML={{ __html: data.replace('\n', '<br />') }}></span>);
  }
};

FormatData.propTypes = {
  data: PropTypes.any,
  struct: PropTypes.object,
};

export default FormatData;

