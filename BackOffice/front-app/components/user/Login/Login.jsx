import React, { PropTypes } from 'react';
import { Field, reduxForm } from 'redux-form';

const Login = (props) => (
  <div className="container center-login">
    <div className="row">
      <form className="form-signin well" onSubmit={props.handleSubmit(props.onSubmitHandler)}>
        <h2 className="form-signin-heading">Formulaire de connexion</h2>
        <br />
        {(() => {
          if (props.user && props.user.fail) {
            return (
              <div className="alert alert-danger">
                <p>{props.user.message}</p>
              </div>
            );
          }
          return (<div></div>);
        })()}
        <label htmlFor="inputEmail" className="sr-only">E-mail</label>
        <Field
          component="input"
          name="email"
          type="text"
          className="form-control"
          id="inputEmail"
          placeholder="E-mail"
          required
          autoFocus
        />
        <br />
        <label htmlFor="inputPassword" className="sr-only">Mot de passe</label>
        <Field
          component="input"
          name="password"
          type="password"
          className="form-control"
          id="inputPassword"
          placeholder="Mot de passe"
          required
        />
        <br />
        <button className="btn btn-lg btn-primary btn-block" type="submit">Connexion</button>
      </form>
    </div>
  </div>
);

Login.propTypes = {
  handleFacebook: PropTypes.func,
  slug: PropTypes.string,
  handleSubmit: PropTypes.func,
  onSubmitHandler: PropTypes.func,
  user: PropTypes.object,
  fields: PropTypes.array,
};

export default reduxForm({
  fields: ['email', 'password'],
  form: 'login',
})(Login);

