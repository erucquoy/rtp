/**
 *			RoadToPermis!
 *	./api-router.js
 * 	Routes definitions
 */

const router = require('express').Router();

const 	accountRoutes 	          = require('./app/routes/account'),
		    authRoutes		            = require('./app/routes/authentication'),
		    payRoutes		              = require('./app/routes/payment'),
        articleRoutes		          = require('./app/routes/article'),
        videoRoutes		            = require('./app/routes/video'),
        imageRoutes		            = require('./app/routes/image'),
        temoignageRoutes          = require('./app/routes/temoignage'),
        centreRoutes		          = require('./app/routes/centre'),
        confianceRoutes		        = require('./app/routes/confiance'),
        pageRoutes		            = require('./app/routes/page'),
        centreCommentaireRoutes   = require('./app/routes/centreCommentaire'),
        ficheRoutes               = require('./app/routes/fiche'),
        schoolRoutes              = require('./app/routes/school'),
		    AuthMiddlewares           = require('./app/middlewares/AuthMiddlewares'),
		    requireActive	            = AuthMiddlewares.requireActiveAccount,
	  	  stripeOnly		            = AuthMiddlewares.stripeOnly,
        multer                    = require('multer');

/**
 *	Admin access
 **/

require('./admin-route')(router);

/**
 *	Free access
 **/
// POST /api/v1/account
router.route('/account').post(accountRoutes.create);

// POST /api/v1/authentication
router.route('/authenticate').post(authRoutes.auth);

// POST /api/v1/activate
router.route('/activate').post(accountRoutes.activate);

// POST /api/v1/resetpassword
router.route('/resetpassword').post(accountRoutes.pwResetRequest);

// POST /api/v1/doresetpassword
router.route('/doresetpassword').post(accountRoutes.pwDoReset);

// GET /api/v1/temoignage
router.route('/temoignage').get(temoignageRoutes.list);

// GET /api/v1/page
router.route('/page').get(pageRoutes.list);

// GET /api/v1/admin/image/:_id
router.route('/image/:_id').get(imageRoutes.get);

// GET /api/v1/admin/article
router.route('/article').get(articleRoutes.list);

/**
 *	Requires a token and an active account
 */

// GET /api/v1/token
router.route('/token').get(requireActive, authRoutes.check);

// GET /api/v1/user
router.route('/user').get(requireActive, authRoutes.get);

// GET /api/v1/subscription
router.route('/subscription').get(requireActive, payRoutes.getSubscription);
// POST /api/v1/subscription
router.route('/subscription').post(requireActive, payRoutes.postSubscription);
// DELETE /api/v1/subscription
router.route('/subscription').delete(requireActive, payRoutes.deleteSubscription);

// GET /api/v1/payment
router.route('/payment').get(requireActive, payRoutes.getpayment);
// POST /api/v1/payment
router.route('/payment').post(requireActive, payRoutes.postpayment);

// POST /api/v1/stripe
router.route('/stripe').post(stripeOnly, payRoutes.stripeHooks);

// GET /api/v1/school
router.route('/school').get(requireActive, schoolRoutes.get);

// DELETE /api/v1/school/monitor
router.route('/school/monitor').post(requireActive, schoolRoutes.addMonitor);

// DELETE /api/v1/school/monitor/:_id
router.route('/school/monitor/:_id').delete(requireActive, schoolRoutes.deleteMonitor);

// GET /api/v1/fiche/:_id
router.route('/fiche/:_id').get(requireActive, ficheRoutes.get);

// GET /api/v1/fiche/user:_id
router.route('/fiche/user/:_id').get(requireActive, ficheRoutes.getByUser);

// POST /api/v1/fiche
router.route('/fiche').post(requireActive, ficheRoutes.create);

// PUT /api/v1/fiche/:_id
router.route('/fiche/:_id').put(requireActive, ficheRoutes.update);

// PUT /api/v1/fiche/:_id
router.route('/fiche/:_id').delete(requireActive, ficheRoutes.deleteFiche);


// All the rest => 404.
var notFoundResponse = function(req, res) {
	return res.status(404).json({
		error: true, 
		message: 'Not found.'
	});
}
router.route('*')	.get(notFoundResponse)
					.post(notFoundResponse)
					.delete(notFoundResponse)
					.put(notFoundResponse);

module.exports = router;