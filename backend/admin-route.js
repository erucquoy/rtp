/**
 *			RoadToPermis!
 *	./admin-router.js
 * 	Routes admin definitions
 */

function admin(router) {

  const accountRoutes = require('./app/routes/account'),
    articleRoutes = require('./app/routes/article'),
    videoRoutes = require('./app/routes/video'),
    imageRoutes = require('./app/routes/image'),
    temoignageRoutes = require('./app/routes/temoignage'),
    centreRoutes = require('./app/routes/centre'),
    confianceRoutes = require('./app/routes/confiance'),
    pageRoutes = require('./app/routes/page'),
    centreCommentaireRoutes = require('./app/routes/centreCommentaire'),
    ficheRoutes = require('./app/routes/fiche'),
    schoolRoutes = require('./app/routes/school'),
    AuthMiddlewares = require('./app/middlewares/AuthMiddlewares'),
    requireAdminAccount = AuthMiddlewares.requireAdminAccount,
    multer = require('multer'),
    upload = multer({
      storage: multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, 'uploads')
        },
        filename: function (req, file, cb) {
          cb(null, Date.now() + '.' + file.originalname.split('.').pop())
        }
      })
    });

  /**
   *  Admin access
   */
  // POST /api/v1/admin/article
  router.route('/admin/article').post(requireAdminAccount, articleRoutes.create);

  // GET /api/v1/admin/article
  router.route('/admin/article').get(requireAdminAccount, articleRoutes.listAdmin);

  // GET /api/v1/admin/article/:_id
  router.route('/admin/article/:_id').get(requireAdminAccount, articleRoutes.get);

  // UPDATE /api/v1/admin/article/:_id
  router.route('/admin/article/:_id').put(requireAdminAccount, articleRoutes.update);

  // DELETE /api/v1/admin/article/:_id
  router.route('/admin/article/:_id').delete(requireAdminAccount, articleRoutes.deleteArticle);

  // POST /api/v1/admin/video
  router.route('/admin/video').post(requireAdminAccount, videoRoutes.create);

  // GET /api/v1/admin/video
  router.route('/admin/video').get(requireAdminAccount, videoRoutes.list);

  // GET /api/v1/admin/video/:_id
  router.route('/admin/video/:_id').get(requireAdminAccount, videoRoutes.get);

  // UPDATE /api/v1/admin/video/:_id
  router.route('/admin/video/:_id').put(requireAdminAccount, videoRoutes.update);

  // DELETE /api/v1/admin/video/:_id
  router.route('/admin/video/:_id').delete(requireAdminAccount, videoRoutes.deleteVideo);

  // GET /api/v1/admin/image/:_id
  router.route('/admin/image/:_id').get(imageRoutes.get);

  // OPTIONS /api/v1/admin/image
  router.route('/admin/image').options(imageRoutes.options);

  // POST /api/v1/admin/image
  router.route('/admin/image').post(upload.single('file'), imageRoutes.create);

  // DELETE /api/v1/admin/image/:_id
  router.route('/admin/image/:_id').delete(requireAdminAccount, imageRoutes.deleteImage);

  // POST /api/v1/admin/temoignage
  router.route('/admin/temoignage').post(requireAdminAccount, temoignageRoutes.create);

  // GET /api/v1/admin/temoignage
  router.route('/admin/temoignage').get(requireAdminAccount, temoignageRoutes.listAdmin);

  // GET /api/v1/admin/temoignage/:_id
  router.route('/admin/temoignage/:_id').get(requireAdminAccount, temoignageRoutes.get);

  // UPDATE /api/v1/admin/temoignage/:_id
  router.route('/admin/temoignage/:_id').put(requireAdminAccount, temoignageRoutes.update);

  // DELETE /api/v1/admin/temoignage/:_id
  router.route('/admin/temoignage/:_id').delete(requireAdminAccount, temoignageRoutes.deleteTemoignage);

  // POST /api/v1/admin/centre
  router.route('/admin/centre').post(requireAdminAccount, centreRoutes.create);

  // GET /api/v1/admin/centre
  router.route('/admin/centre').get(requireAdminAccount, centreRoutes.list);

  // GET /api/v1/admin/centre/:_id
  router.route('/admin/centre/:_id').get(requireAdminAccount, centreRoutes.get);

  // UPDATE /api/v1/admin/centre/:_id
  router.route('/admin/centre/:_id').put(requireAdminAccount, centreRoutes.update);

  // DELETE /api/v1/admin/centre/:_id
  router.route('/admin/centre/:_id').delete(requireAdminAccount, centreRoutes.deleteCentre);

  // POST /api/v1/admin/confiance
  router.route('/admin/confiance').post(requireAdminAccount, confianceRoutes.create);

  // GET /api/v1/admin/confiance
  router.route('/admin/confiance').get(requireAdminAccount, confianceRoutes.list);

  // GET /api/v1/admin/confiance/:_id
  router.route('/admin/confiance/:_id').get(requireAdminAccount, confianceRoutes.get);

  // UPDATE /api/v1/admin/confiance/:_id
  router.route('/admin/confiance/:_id').put(requireAdminAccount, confianceRoutes.update);

  // DELETE /api/v1/admin/confiance/:_id
  router.route('/admin/confiance/:_id').delete(requireAdminAccount, confianceRoutes.deleteConfiance);

  // GET /api/v1/admin/page
  router.route('/admin/page').get(requireAdminAccount, pageRoutes.list);

  // GET /api/v1/admin/page/:_id
  router.route('/admin/page/:_id').get(requireAdminAccount, pageRoutes.get);

  // UPDATE /api/v1/admin/page/:_id
  router.route('/admin/page/:_id').put(requireAdminAccount, pageRoutes.update);

  // GET /api/v1/admin/account
  router.route('/admin/account').get(accountRoutes.list);

  // GET /api/v1/admin/account/:_id
  router.route('/admin/account/:_id').get(requireAdminAccount, accountRoutes.get);

  // UPDATE /api/v1/admin/account/:_id
  router.route('/admin/account/:_id').put(accountRoutes.update);

  // GET /api/v1/admin/centrecommentaire
  router.route('/admin/centrecommentaire').get(requireAdminAccount, centreCommentaireRoutes.list);

  // GET /api/v1/admin/centrecommentaire/:_id
  router.route('/admin/centrecommentaire/:_id').get(requireAdminAccount, centreCommentaireRoutes.get);

  // UPDATE /api/v1/admin/centrecommentaire/:_id
  router.route('/admin/centrecommentaire/:_id').put(requireAdminAccount, centreCommentaireRoutes.update);

  // DELETE /api/v1/admin/centrecommentaire/:_id
  router.route('/admin/centrecommentaire/:_id').delete(requireAdminAccount, centreCommentaireRoutes.deleteCentreCommentaire);

  // GET /api/v1/admin/fiche
  router.route('/admin/fiche').get(requireAdminAccount, ficheRoutes.list);

  // GET /api/v1/admin/fiche/:_id
  router.route('/admin/fiche/:_id').get(requireAdminAccount, ficheRoutes.get);

  // UPDATE /api/v1/admin/fiche/:_id
  router.route('/admin/fiche/:_id').put(requireAdminAccount, ficheRoutes.updateAdmin);

  // DELETE /api/v1/admin/fiche/:_id
  router.route('/admin/fiche/:_id').delete(requireAdminAccount, ficheRoutes.deleteFiche);

  // POST /api/v1/admin/school
  router.route('/admin/school').post(requireAdminAccount, schoolRoutes.create);

  // GET /api/v1/admin/school
  router.route('/admin/school').get(requireAdminAccount, schoolRoutes.list);

  // GET /api/v1/admin/fiche/:_id
  router.route('/admin/school/:_id').get(requireAdminAccount, schoolRoutes.getAdmin);

  // UPDATE /api/v1/admin/fiche/:_id
  router.route('/admin/school/:_id').put(requireAdminAccount, schoolRoutes.update);

  // DELETE /api/v1/admin/fiche/:_id
  router.route('/admin/school/:_id').delete(requireAdminAccount, schoolRoutes.deleteSchool);
}

module.exports = admin;