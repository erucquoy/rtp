const 	account 	        = require("../app/models/account"),
        page 	            = require("../app/models/page"),
        mongoose				  = require('mongoose'),
        config 		 			  = require('../app/config');

const toCreatePage = [
  'homepage-slideshow',
  'homepage-app-left',
  'homepage-app-right',
  'homepage-apprentis-1',
  'homepage-apprentis-2',
  'homepage-apprentis-3',
  'homepage-apprentis-4',
  'homepage-autoecole-1',
  'homepage-autoecole-2',
  'homepage-autoecole-3',
  'homepage-autoecole-4',
  'homepage-parle-1',
  'homepage-parle-2',
  'homepage-parle-3',
  'application',
  'services-why-1',
  'services-why-2',
  'services-why-3',
  'services-why-4',
  'services-why-5',
  'services-apprentis-1',
  'services-apprentis-2',
  'services-apprentis-3',
  'services-apprentis-4',
  'services-autoecole-1',
  'services-autoecole-2',
  'services-autoecole-3',
  'services-autoecole-4',
  'services-app-slideshow',
  'services-youtube-link',
  'offer-3days-1',
  'offer-3days-2',
  'offer-3days-3',
  'offer-3days-4',
  'offer-1month-1',
  'offer-1month-2',
  'offer-1month-3',
  'offer-1month-4',
  'offer-6months-1',
  'offer-6months-2',
  'offer-6months-3',
  'offer-6months-4',
  'offer-solution-1',
  'offer-solution-2',
  'offer-solution-3',
  'offer-solution-4',
];

module.exports = function () {
  if(process.env.NODE_ENV === 'test') {
    account.findOne({email: 'test@test'}, function (err, ac) {
      if (err || !ac) {
        var ac = new account({
          email: 'test@test',
          password: 'test',
          profile: {
            firstName: 'bastien',
            lastName: 'robert'
          },
        }).save(function (err, us) {
        });
      }
    });
  }

  toCreatePage.forEach(function(t){
    page.findOne({title: t}, function (err, ac) {
      if (err || !ac) {
        var ac = new page({
          title: t,
          content: 'A personnaliser dans le back-office',
        }).save(function (err, us) {
        });
      }
    });
  });
};

