/**
 *			RoadToPermis!
 *	./app/config.js
 * 	Configuration for the application
 */

if(process.env.NODE_ENV === 'test') {
	module.exports = {
	    'secret': 'secret',
	    'mongouri': 'mongodb://testbackend:R9BR0BaH1BIwknht@rtp-shard-00-00-r6cbr.mongodb.net:27017,rtp-shard-00-01-r6cbr.mongodb.net:27017,rtp-shard-00-02-r6cbr.mongodb.net:27017/backendtest?ssl=true&replicaSet=RTP-shard-0&authSource=admin',
	    'port': 9982,
	    'bcryptrounds': 2,
	    'tokensExpiry': 24*3600,
	    'tokens': {
	    	"mailgunpublic" : "pubkey-7245a9855fd8a64caf7f7439a187dc34",
	    	'mailgunprivate': 'key-4028e0150985773459f53d853bdfe4c3',
	    	'domain'        : 'brodev.fr',
	    	'stripepub'	: 'pk_test_kit77CNN3RDwvDnJSYwEUcZE',
	    	'stripepriv'	: 'sk_test_OlyFL2DViL7o9ABz4iD0i6ay'
	    }, stripeIPs: [
			'54.187.174.169', '54.187.205.235', '54.187.216.72', '54.241.31.99', '54.241.31.102', '54.241.34.107'
		], stripeSecret: 'whsec_RIx88SufgWroWwCG464aeV6pArTEorIV'
	}
} else {
	module.exports = {
	    'secret': 'secret',
	    'mongouri': 'mongodb://testbackend:R9BR0BaH1BIwknht@rtp-shard-00-00-r6cbr.mongodb.net:27017,rtp-shard-00-01-r6cbr.mongodb.net:27017,rtp-shard-00-02-r6cbr.mongodb.net:27017/backend?ssl=true&replicaSet=RTP-shard-0&authSource=admin',
	    'port': 9982,
	    'bcryptrounds': 14,
	    'tokensExpiry': 24*3600,
	    'tokens': {
	    	"mailgunpublic" : "pubkey-7245a9855fd8a64caf7f7439a187dc34",
	    	'mailgunprivate': 'key-4028e0150985773459f53d853bdfe4c3',
	    	'domain'        : 'brodev.fr',
	    	'stripepub'	: 'pk_test_kit77CNN3RDwvDnJSYwEUcZE',
	    	'stripepriv'	: 'sk_test_OlyFL2DViL7o9ABz4iD0i6ay'
	    }, stripeIPs: [
			'54.187.174.169', '54.187.205.235', '54.187.216.72', '54.241.31.99', '54.241.31.102', '54.241.34.107'
		], stripeSecret: 'whsec_RIx88SufgWroWwCG464aeV6pArTEorIV'
	}
}