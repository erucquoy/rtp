/**
 *			RoadToPermis!
 *	./app/routes/page.js
 * 	Article routes
 */

const 	page 	= require("../models/page"),
        image 	    = require("../models/image"),
        sanitizer 	= require('sanitizer'),
        config		  = require('../config'),
        fs          = require('fs');



/**
 * GET /api/v1/admin/page AND /api/v1/page
 * Get all the page
 */
var list = function(req, res) {
  var offset = req.query.offset;
  if (typeof(offset) !== 'number') {
    offset = 0;
  }
  page.find({}).skip(parseInt(offset)).limit(parseInt(req.query.limit)).exec((err, pages) => {
      if(err || !page) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

    page.count({}, function(err, c) {
      return res.status(200).json({ count: c, data: pages});
    });
  });
};

/**
 * GET /api/v1/admin/page/:_id
 * Get one page depending of the id in parameters
 */
var get = function(req, res) {
  page.findOne({_id: sanitizer.escape(req.param('_id'))}).populate('images').exec((err, page) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      for (let i = 0; i < page.images.length; i++) {
        page.images[i] = 'image/' + page.images[i]._id;
      }

      return res.status(200).json(page);
  });
};

/**
 * PUT /api/v1/admin/page/:_id
 * update an page for the given data
 */
var update = function(req, res) {
  page.findOne({_id: sanitizer.escape(req.param('_id'))}, (err, page) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      if(!page) {
        return res.status(404).json({error: true, message: 'Page non trouvé'});
      }
      // Mandatory body parameters
      req.checkBody({
      'content': {
        notEmpty: {
          errorMessage: "Le contenu ne peut être vide"
        }
      },
    });

    // Is body ok ?
    req.getValidationResult().then(function (result) {
      result.useFirstErrorOnly();
      if (!result.isEmpty()) { // It isn't => bad request
        return res.status(400).json({
          error: true,
          message: result.array()
        });
      } else { // Body is OK.
        // Input sanitization
        req.body.content  = sanitizer.escape(req.body.content);

        if (req.body.images == undefined)
          req.body.images = [];

        page.content  = req.body.content;
        page.save(function (err, us) {
          if (err) {
            console.log(err);
            return res.status(500).json({error: true, message: 'Internal server error'});
          }

          req.body.images.forEach(t => {
            image.findOne({_id: sanitizer.escape(t)}, (err, image) => {
              if (err || !image)
                return;
              us.images.push(image);
              us.save();
            });
          });

          return res.status(201).json({
            error: false,
            message: ''
          });
        });
      }
    });
  });
};

module.exports = {
  list,
  get,
  update,
};