/**
 *			RoadToPermis!
 *	./app/routes/video.js
 * 	Video routes
 */

const 	video 	= require("../models/video"),
  sanitizer 	= require('sanitizer'),
  config		= require('../config');


/**
 * POST /api/v1/video
 * Creates a new video for the given data
 */
var create = function(req, res) {
  // Mandatory body parameters
  req.checkBody({
    'title': {
      notEmpty: {
        errorMessage: "Le titre ne peut être vide"
      }
    },
    'category': {
      notEmpty: {
        errorMessage: "La catégorie ne peut être vide"
      }
    },
  });

  // Is body ok ?
  req.getValidationResult().then(function(result) {
    result.useFirstErrorOnly();
    if (!result.isEmpty()) { // It isn't => bad request
      return res.status(400).json({
        error: true,
        message: result.array()
      });
    } else { // Body is OK.
      // Input sanitization
      req.body.title 		= sanitizer.escape(req.body.title);
      req.body.youtubeVideo = sanitizer.escape(req.body.youtubeVideo);
      req.body.category = sanitizer.escape(req.body.category);
      req.body.isActive = req.body.isActive ? true : false;

      var ac = new video({
        title: req.body.title,
        youtubeVideo: req.body.youtubeVideo,
        category: req.body.category,
        isActive: req.body.isActive,
      }).save(function(err, us) {
        if (err) {
          console.log(err);
          return res.status(500).json({error: true, message: 'Internal server error'});
        }
        return res.status(201).json({
          error: false,
          message: ''
        });
      });
    }
  });
};

/**
 * GET /api/v1/video
 * Get all the video
 */
var list = function(req, res) {
  var offset = req.query.offset;
  if (typeof(offset) !== 'number') {
    offset = 1;
  }
  video.find({}).skip(parseInt(offset)).limit(parseInt(req.query.limit)).exec((err, videos) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    video.count({}, function(err, c) {
      return res.status(200).json({ count: c, data: videos});
    });
  });
};

/**
 * GET /api/v1/video/:_id
 * Get one video depending of the id in parameters
 */
var get = function(req, res) {
  video.findOne({_id: sanitizer.escape(req.param('_id'))}, (err, video) => {
    if(err || !video) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    return res.status(200).json(video);
  });
};

/**
 * PUT /api/v1/video/:_id
 * update a video for the given data
 */
var update = function(req, res) {
  video.findOne({_id: sanitizer.escape(req.param('_id'))}, (err, video) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    if(!video) {
    return res.status(404).json({error: true, message: 'Video non trouvé'});
  }
  // Mandatory body parameters
  req.checkBody({
    'title': {
      notEmpty: {
        errorMessage: "Le titre ne peut être vide"
      }
    },
    'category': {
      notEmpty: {
        errorMessage: "La catégorie ne peut être vide"
      }
    },
  });

  // Is body ok ?
  req.getValidationResult().then(function (result) {
    result.useFirstErrorOnly();
    if (!result.isEmpty()) { // It isn't => bad request
      return res.status(400).json({
        error: true,
        message: result.array()
      });
    } else { // Body is OK.
      // Input sanitization
      req.body.title = sanitizer.escape(req.body.title);
      req.body.youtubeVideo = sanitizer.escape(req.body.youtubeVideo);
      req.body.category = sanitizer.escape(req.body.category);
      req.body.isActive = req.body.isActive ? true : false;

      video.title = req.body.title;
      video.youtubeVideo = req.body.youtubeVideo;
      video.category = req.body.category;
      video.isActive = req.body.isActive;
      video.save(function (err, us) {
        if (err) {
          console.log(err);
          return res.status(500).json({error: true, message: 'Internal server error'});
        }
        return res.status(201).json({
          error: false,
          message: ''
        });
      });
    }
  });
});
};

/**
 * DELETE /api/v1/video/:_id
 * delete one video depending of the id in parameters
 */
var deleteVideo = function(req, res) {
  video.findOneAndRemove({_id: sanitizer.escape(req.param('_id'))}, (err, video) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    return res.status(200).json({
      error: false,
      message: ''
    });
});
};

module.exports = {
  create,
  list,
  get,
  update,
  deleteVideo,
};