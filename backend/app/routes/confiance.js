/**
 *			RoadToPermis!
 *	./app/routes/confiance.js
 * 	Article routes
 */

const 	confiance 	= require("../models/confiance"),
        image 	    = require("../models/image"),
        sanitizer 	= require('sanitizer'),
        config		  = require('../config'),
        fs          = require('fs');


/**
 * POST /api/v1/confiance
 * Creates a new confiance for the given data
 */
var create = function(req, res) {
  // Mandatory body parameters
  req.checkBody({
    'name': {
      notEmpty: {
        errorMessage: "Le nom ne peut être vide"
      }
    },
  });

  // Is body ok ?
  req.getValidationResult().then(function(result) {
    result.useFirstErrorOnly();
    if (!result.isEmpty()) { // It isn't => bad request
      return res.status(400).json({
        error: true,
        message: result.array()
      });
    } else { // Body is OK.
      // Input sanitization
      req.body.name 		= sanitizer.escape(req.body.name);
      req.body.isActive = req.body.isActive ? true : false;

      var ac = new confiance({
        name: req.body.name,
        isActive: req.body.isActive,
      }).save(function(err, us) {
        if (err) {
          console.log(err);
          return res.status(500).json({error: true, message: 'Internal server error'});
        }

        req.body.images.forEach(t => {
            image.findOne({_id: sanitizer.escape(t)}, (err, image) => {
              if (err || !image)
                return;
              us.images.push(image);
              us.save();
          });
        });
        return res.status(201).json({
          error: false,
          message: ''
        });
      });
    }
  });
};

/**
 * GET /api/v1/confiance
 * Get all the confiance
 */
var list = function(req, res) {
  var offset = req.query.offset;
  if (typeof(offset) !== 'number') {
    offset = 0;
  }
  confiance.find({}).skip(parseInt(offset)).limit(parseInt(req.query.limit)).exec((err, confiances) => {
      if(err ) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

    confiance.count({}, function(err, c) {
      return res.status(200).json({ count: c, data: confiances});
    });
  });
};

/**
 * GET /api/v1/confiance/:_id
 * Get one confiance depending of the id in parameters
 */
var get = function(req, res) {
  confiance.findOne({_id: sanitizer.escape(req.param('_id'))}).populate('images').exec((err, confiance) => {
      if(err || !confiance) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      for (let i = 0; i < confiance.images.length; i++) {
        confiance.images[i] = 'image/' + confiance.images[i]._id;
      }

      return res.status(200).json(confiance);
  });
};

/**
 * PUT /api/v1/confiance/:_id
 * update an confiance for the given data
 */
var update = function(req, res) {
  confiance.findOne({_id: sanitizer.escape(req.param('_id'))}, (err, confiance) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      if(!confiance) {
        return res.status(404).json({error: true, message: 'Article non trouvé'});
      }
      // Mandatory body parameters
      req.checkBody({
      'name': {
        notEmpty: {
          errorMessage: "Le nom ne peut être vide"
        }
      },
    });

    // Is body ok ?
    req.getValidationResult().then(function (result) {
      result.useFirstErrorOnly();
      if (!result.isEmpty()) { // It isn't => bad request
        return res.status(400).json({
          error: true,
          message: result.array()
        });
      } else { // Body is OK.
        // Input sanitization
        req.body.name = sanitizer.escape(req.body.name);
        req.body.isActive = req.body.isActive ? true : false;

        if (req.body.images == undefined)
          req.body.images = [];

        confiance.name = req.body.name;
        confiance.isActive = req.body.isActive;
        confiance.save(function (err, us) {
          if (err) {
            console.log(err);
            return res.status(500).json({error: true, message: 'Internal server error'});
          }

          req.body.images.forEach(t => {
            image.findOne({_id: sanitizer.escape(t)}, (err, image) => {
              if (err || !image)
                return;
              us.images.push(image);
              us.save();
            });
          });

          return res.status(201).json({
            error: false,
            message: ''
          });
        });
      }
    });
  });
};

/**
 * DELETE /api/v1/confiance/:_id
 * delete one confiance depending of the id in parameters
 */
var deleteConfiance = function(req, res) {
  confiance.findOneAndRemove({_id: sanitizer.escape(req.param('_id'))}, (err, confiance) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    for (let i = 0; i < confiance.images.length; i++) {
      image.findOneAndRemove({_id: confiance.images[i]}, (err, image) => {
        if (fs.existsSync(image.source)) {
          fs.unlinkSync(image.source);
        }
      });
    }

    return res.status(200).json({
      error: false,
      message: ''
    });
});
};

module.exports = {
  create,
  list,
  get,
  update,
  deleteConfiance,
};