/**
 *			RoadToPermis!
 *	./app/routes/centreCommentaire.js
 * 	centre commentaire routes
 */

const 	centrecommentaire 	= require("../models/centrecommentaire"),
        sanitizer 	        = require('sanitizer'),
        config		          = require('../config'),
        fs                  = require('fs');


/**
 * GET /api/v1/centrecommentaire
 * Get all the centre commentaire
 */
var list = function(req, res) {
  var offset = req.query.offset;
  if (typeof(offset) !== 'number') {
    offset = 0;
  }
  centrecommentaire.find({centre: req.query.centre}).skip(parseInt(offset)).limit(req.query.limit).exec((err, centrecommentaires) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

    centrecommentaire.count({}, function(err, c) {
      return res.status(200).json({ count: c, data: centrecommentaires});
    });
  });
};

/**
 * GET /api/v1/centrecommentaire/:_id
 * Get one centre commentaire depending of the id in parameters
 */
var get = function(req, res) {
  centrecommentaire.findOne({_id: sanitizer.escape(req.param('_id'))}).exec((err, centrecommentaire) => {
      if(err || !centrecommentaire) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      return res.status(200).json(centrecommentaire);
  });
};

/**
 * PUT /api/v1/centrecommentaire/:_id
 * update an centre commentaire for the given data
 */
var update = function(req, res) {
  centrecommentaire.findOne({_id: sanitizer.escape(req.param('_id'))}, (err, centrecommentaire) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      if(!centrecommentaire) {
        return res.status(404).json({error: true, message: 'Commentaire non trouvé'});
      }
      // Mandatory body parameters
      req.checkBody({
      'content': {
        notEmpty: {
          errorMessage: "Le contenu ne peut être vide"
        }
      },
    });

    // Is body ok ?
    req.getValidationResult().then(function (result) {
      result.useFirstErrorOnly();
      if (!result.isEmpty()) { // It isn't => bad request
        return res.status(400).json({
          error: true,
          message: result.array()
        });
      } else { // Body is OK.
        // Input sanitization
        req.body.content = sanitizer.escape(req.body.content);

        centrecommentaire.content = req.body.content;
        centrecommentaire.save(function (err, us) {
          if (err) {
            console.log(err);
            return res.status(500).json({error: true, message: 'Internal server error'});
          }

          return res.status(201).json({
            error: false,
            message: ''
          });
        });
      }
    });
  });
};

/**
 * DELETE /api/v1/centrecommentaire/:_id
 * delete one centre commentaire depending of the id in parameters
 */
var deleteCentreCommentaire = function(req, res) {
  centrecommentaire.findOneAndRemove({_id: sanitizer.escape(req.param('_id'))}, (err, centrecommentaire) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    return res.status(200).json({
      error: false,
      message: ''
    });
});
};

module.exports = {
  list,
  get,
  update,
  deleteCentreCommentaire,
};