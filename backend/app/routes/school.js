/**
 *			RoadToPermis!
 *	./app/routes/school.js
 * 	school routes
 */

const 	school 	            = require("../models/school"),
        account 	          = require("../models/account"),
        fiche 	            = require("../models/fiche"),
        sanitizer 	        = require('sanitizer'),
        config		          = require('../config'),
        fs                  = require('fs');


/**
 * DELETE /api/v1/school/monitor/:_id
 * delete a monitor from a school
 */
var deleteMonitor = function(req, res) {
  account.findOne({_id: req.decoded._id, $or: [{drivingSchool: true}]}).exec(function(err, userSchool) {
    if (err || !userSchool) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    account.findOne({_id: sanitizer.escape(req.param('_id')), school: userSchool.school}).exec((err, user) => {
      if (err || !user) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      user.monitor = false;
      user.save(function (err, us) {
        if (err) {
          console.log(err);
          return res.status(500).json({error: true, message: 'Internal server error'});
        }

        return res.status(201).json({
          error: false,
          message: ''
        });
      });
    });
  });
};

/**
 * POST /api/v1/school/monitor
 * add a monitor from a school
 */
var addMonitor = function(req, res) {
  account.findOne({_id: req.decoded._id, $or: [{drivingSchool: true}]}).exec(function(err, userSchool) {
    if (err || !userSchool) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    account.findOne({email: sanitizer.escape(req.body.email)}).exec((err, user) => {
      if (err || !user) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Utilisateur non trouvé.'});
      }

      user.monitor = true;
      user.school = userSchool.school;
      user.save(function (err, us) {
        if (err) {
          console.log(err);
          return res.status(500).json({error: true, message: 'Internal server error'});
        }

        return res.status(201).json({
          error: false,
          message: ''
        });
      });
    });
  });
};


/**
 * GET /api/v1/admin/school
 * Get all the centre commentaire
 */
var list = function(req, res) {
  var offset = req.query.offset;
  if (typeof(offset) !== 'number') {
    offset = 0;
  }
  school.find({}).skip(parseInt(offset)).limit(parseInt(req.query.limit)).exec((err, schools) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

    school.count({}, function(err, c) {
      return res.status(200).json({ count: c, data: schools});
    });
  });
};

/**
 * GET /api/v1/school/
 * Get the school of the user
 */
var get = function(req, res) {
  account.findOne({_id: req.decoded._id, /*$or: [{drivingSchool: true}, {monitor: true}]*/}).populate('school').exec(function(err, user) {
    if(err || !user) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    account.find({school: user.school._id, monitor: true}).exec((err, monitors) => {
      if(err || !school) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      account.find({school: user.school._id, monitor: false, drivingSchool: false}).exec((err, students) => {
        if(err || !school) {
          console.log(err);
          return res.status(500).json({error: true, message: 'Internal server error'});
        }

        var date = new Date();

        if (req.query.date) {
          date = new Date(req.query.date);
        }

        var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 20);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 7);

        fiche.find({school: user.school._id, date: {$gte: firstDay, $lt: lastDay} }).populate('monitor').populate('student').exec((err, events) => {
          if(err || !school) {
            console.log(err);
            return res.status(500).json({error: true, message: 'Internal server error'});
          }

          return res.status(200).json({
            data: {
              _id: user.school._id,
              name: user.school.name,
              monitors: monitors,
              students: students,
              events: events,
            },
          });
        });
      });
    });

  });
};

/**
 * GET /api/v1/admin/school/:_id
 * Get one centre commentaire depending of the id in parameters
 */
var getAdmin = function(req, res) {
  school.findOne({_id: sanitizer.escape(req.param('_id'))}).exec((err, school) => {
      if(err || !school) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      return res.status(200).json(school);
  });
};

/**
 * POST /api/v1/admin/school
 * Creates a new school for the given data
 */
var create = function(req, res) {
  // Mandatory body parameters
  req.checkBody({
    'name': {
      notEmpty: {
        errorMessage: "Le nom ne peut être vide"
      }
    },
  });

  // Is body ok ?
  req.getValidationResult().then(function(result) {
    result.useFirstErrorOnly();
    if (!result.isEmpty()) { // It isn't => bad request
      return res.status(400).json({
        error: true,
        message: result.array()
      });
    } else { // Body is OK.
      // Input sanitization
      req.body.name 		= sanitizer.escape(req.body.name);
      req.body.isActive = req.body.isActive ? true : false;

      var ac = new school({
        name: req.body.name,
        isActive: req.body.isActive,
      }).save(function(err, us) {
        if (err) {
          console.log(err);
          return res.status(500).json({error: true, message: 'Internal server error'});
        }

        return res.status(201).json({
          error: false,
          message: ''
        });
      });
    }
  });
};

/**
 * PUT /api/v1/admin/school/:_id
 * update an centre commentaire for the given data
 */
var update = function(req, res) {
  school.findOne({_id: sanitizer.escape(req.param('_id'))}, (err, school) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      if(!school) {
        return res.status(404).json({error: true, message: 'Ecole non trouvé'});
      }
      // Mandatory body parameters
      req.checkBody({
      'name': {
        notEmpty: {
          errorMessage: "Le nom ne peut être vide"
        }
      },
    });

    // Is body ok ?
    req.getValidationResult().then(function (result) {
      result.useFirstErrorOnly();
      if (!result.isEmpty()) { // It isn't => bad request
        return res.status(400).json({
          error: true,
          message: result.array()
        });
      } else { // Body is OK.
        // Input sanitization
        req.body.name = sanitizer.escape(req.body.name);
        req.body.isActive = req.body.isActive ? true : false;

        school.name = req.body.name;
        school.save(function (err, us) {
          if (err) {
            console.log(err);
            return res.status(500).json({error: true, message: 'Internal server error'});
          }

          return res.status(201).json({
            error: false,
            message: ''
          });
        });
      }
    });
  });
};

/**
 * DELETE /api/v1/admin/school/:_id
 * delete one centre commentaire depending of the id in parameters
 */
var deleteSchool = function(req, res) {
  school.findOneAndRemove({_id: sanitizer.escape(req.param('_id'))}, (err, school) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    return res.status(200).json({
      error: false,
      message: ''
    });
});
};

module.exports = {
  addMonitor,
  deleteMonitor,
  list,
  get,
  getAdmin,
  create,
  update,
  deleteSchool,
};