/**
 *			RoadToPermis!
 *	./app/routes/payment.js
 * 	payment, subscription, ... 
 */

const 	account 		= require('../models/account'),
		config			= require('../config'),
		stripe 			= require("stripe")(config.tokens.stripepriv),
	  	mailgun			= require('mailgun-js')({	apiKey:config.tokens.mailgunprivate, domain: config.tokens.domain });

/**
 * 	GET /api/v1/subscription
 * 	Get subscription status
 */
var getSubscription = function(req, res) {
	account.findOne({ email: req.decoded.email }, (err, usr) => {
		if(err || !usr) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }
		
		stripe.customers.retrieve(usr.stripeAcc, function(err, customer) {
			if(customer.subscriptions.total_count === 0) {
				return res.status(200).json({
					error: false, 
					message: '',
					subscription: {
						active: false,
						cancelled: false,
						plan: null,
						start_date: null,
						end_date: null
					}
				});
			} else {
				var sID = customer.subscriptions.data[0].id;
				
				stripe.subscriptions.retrieve(sID, (err, sub) => {
					console.log(err);
					return res.status(200).json({
						error: false, 
						message: '',
						subscription: {
							active: true, 
							cancelled: sub.cancel_at_period_end,
							plan: sub.items.data[0].plan.id,
							start_date: sub.current_period_start,
							end_date: sub.current_period_end
						}
					});	
				});				
			}
		});		
	});
};

/**
 *	POST /api/v1/subscription
 * 	Buys a new subscription
 */
var postSubscription = function(req, res) {
	// Mandatory body parameters
	req.checkBody({
		'plan': {
			notEmpty: {
				errorMessage: "La formule ne peut être vide."
			}
		}
	});

	// Is body ok ? 
	req.getValidationResult().then(function(result) {
		result.useFirstErrorOnly();
	    if (!result.isEmpty()) { // It isn't => bad request
			return res.status(400).json({
				error: true,
				message: result.array()
			});
		} else {
			if(req.body.plan !== 'premium_6months' && req.body.plan !== 'premium_1months') {
				return res.status(400).json({
					error: true, 
					message: "La formule choisie n'existe pas."
				});
			}

			// Body ok. Is there a payment source attached to this user ?
			account.findOne( { email: req.decoded.email }, (err, usr) => {
				if(err) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }

				// Retrive stripe data for user
				stripe.customers.retrieve(usr.stripeAcc, (err, cus) => {
					if(err || !cus) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }

					// Customer has no payment source ?
					if(cus.sources.data[0] === undefined) {
						return res.status(400).json({
							error: true, 
							message: "Aucun moyement de paiement n'est enregistré sur votre compte."
						});
					}

					// Already subscribed ? 
					if(cus.subscriptions.total_count !== 0) {
						return res.status(400).json({
							error: true, 
							message: "Vous êtes déjà abonné."
						});	
					}

					// All is well. Subscribe.
					stripe.subscriptions.create({
						customer: usr.stripeAcc, 
						plan: req.body.plan
					}, (err, subscription) => {
						// Subscription was created. At this point we're still waiting for payment confirmation, so we're in the 'pending' state.
						
						return res.status(200).json({
							error: false,
							message: 'Votre demande a été prise en compte. Merci.'
						});
					});

				});
			});
		}
	});
};

/**
 * 	DELETE /api/v1/subscription
 *	Cancels the current subscription
 */
var deleteSubscription = function(req, res) {
	account.findOne( { email: req.decoded.email }, (err, usr) => {
		if(err || !usr) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }
		
		stripe.customers.retrieve(usr.stripeAcc, (err, cus) => {
			if(cus.subscriptions.total_count === 0) {
				return res.status(400).json({
					error: true, 
					message: "Vous n'êtes pas abonné."
				});	
			}
			
			// ID of the user's subscription
			var subID = cus.subscriptions.data[0].id;
			
			stripe.customers.cancelSubscription(usr.stripeAcc, subID, { at_period_end: true }, function(err, data) {
				var mailData = {
					from: 'RoadToPermis <ne.pas.repondre@' + config.tokens.domain + '>',
					to: usr.email,
					subject: 'Annulation de votre abonement RoadToPermis',
					text: 'Annulé. Actif jusque ' + data.current_period_end //@todo meilleurs mails
				};

				mailgun.messages().send(mailData, (err, body) => {
					if(err) { return res.status(500).json({}); }
					return res.status(200).json({ error: false, message: '' });
				});
			});
		});
	});
};

/**
 *	GET /api/v1/payment
 *	Get payment information for user, ie returns the last 4 digits of the card
 */
var getpayment = function(req, res) {
	account.findOne({email: req.decoded.email}, (err, usr) => {
		if(err || !usr) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }

		// Fetches the stripe customer linked to this user
		stripe.customers.retrieve(usr.stripeAcc, (err, customer) => {
			if(err || !customer) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }

			// User has no payment method 
			if(customer.sources.data[0] === undefined) {
				return res.status(200).json({
					error: false, 
					message: '', 
					payment: {}
				});
			} else {
				return res.status(200).json({
					error: false, 
					message: '', 
					payment: {
						brand: customer.sources.data[0].brand,
						exp_month: customer.sources.data[0].exp_month, 
						exp_year: customer.sources.data[0].exp_year,
						last4: customer.sources.data[0].last4
					}
				});
			}			
		});
	});
}

/**
 *	POST /api/v1/payment
 *	Sets/updates the user's payment information
 */
var postpayment = function(req, res) {
	// Mandatory body parameters
	req.checkBody({
		'source': {
			notEmpty: {
				errorMessage: "La source de paiement ne peut être vide."
			}
		}
	});

	// Is body ok ? 
	req.getValidationResult().then(function(result) {
		result.useFirstErrorOnly();
	    if (!result.isEmpty()) { // It isn't => bad request
			return res.status(400).json({
				error: true,
				message: result.array()
			});
		} else {
			// Retrive user's account
			account.findOne({ email: req.decoded.email }, (err, usr) => {
				// If the request contains null in the source field, reset the customers's payment method
				if(req.body.source === "null") {
					// Retrive the customer
					stripe.customers.retrieve(usr.stripeAcc, (err, cus) => {
						if(err) { console.log(err); throw err;}

						// Does he have a card attached ? 
						if(cus.sources.data[0] === undefined) {
							// Nope. Error.
							return res.status(400).json({
								error: true, 
								message: 'Aucune carte enregistrée sur ce compte'
							});
						}

						// He does. Grab the card ID and remove it.
						var cardID = cus.sources.data[0].id;
						stripe.customers.deleteCard(usr.stripeAcc, cardID, (err, confirmation) => {
							// Error ? 
							if(err || !confirmation.deleted ) {
								console.log(err);
								return res.status(500).json({
									error: true, 
									message: ''
								});
							} else {
								// All is good. Card was removed.
								return res.status(200).json({
									error: false, 
									message: ''
								});
							}
						});
					});
				} else { // Otherwise, update the customer with the new source
					stripe.customers.update(usr.stripeAcc, {
						source: req.body.source
					}, (err, customer) => {
						if(err) { 
							return res.status(400).json({error: true, message: 'Impossible de traiter votre requête, votre source de paiement ne peut être reconnue. Merci de réeesayer.' }); 
						}

						return res.status(200).json({
							error: false,
							message: ''
						});
					});
				}
			});
		}
	});
}

/**
 *	POST /api/v1/stripe
 *	Outlet for stripe webhooks
 */
var stripeHooks = function(req, res) {
	// We can't trust 100% what's coming from the webhooks. So we only use the event ID
	var eventID = req.body.id;
	stripe.events.retrieve(eventID, function(err, event) {
		console.log(event.type);
		if(err) { console.log(err); return res.status(500).json({}); }
		
		// charge.succeeded event
		if(event.type === 'charge.succeeded') {
			account.findOne({ stripeAcc: event.data.object.customer }, (err, usr) => {
				if(err || !usr) { console.log(usr); return res.status(500).json({}); }// @todo ?					
				var mailData = {
					from: 'RoadToPermis <ne.pas.repondre@' + config.tokens.domain + '>',
					to: usr.email,
					subject: 'Validation de votre abonement RoadToPermis',
					text: 'Activé.' //@todo meilleurs mails
				};

				mailgun.messages().send(mailData, (err, body) => {
					if(err) { return res.status(500).json({}); }
					return res.status(200).json({});
				});
			});
		} else if(event.type === 'charge.failed') {
			account.findOne({ stripeAcc: event.data.object.customer }, (err, usr) => {
				if(err || !usr) { console.log(usr); return res.status(500).json({}); } // @todo ?
				var mailData = {
					from: 'RoadToPermis <ne.pas.repondre@' + config.tokens.domain + '>',
					to: usr.email,
					subject: 'Erreur de paiment',
					text: 'Impossible de valider le paiment.' //@todo meilleurs mails
				};

				mailgun.messages().send(mailData, (err, body) => {
					if(err) { return res.status(500).json({}); }
					return res.status(200).json({});
				});
			});	
		} else if(event.type === 'customer.subscription.deleted') {
			account.findOne({ stripeAcc: event.data.object.customer }, (err, usr) => {
				if(err || !usr) { console.log(usr); return res.status(500).json({}); } // @todo ?
				var mailData = {
					from: 'RoadToPermis <ne.pas.repondre@' + config.tokens.domain + '>',
					to: usr.email,
					subject: 'Votre abonement RoadToPermis est terminé',
					text: 'La fin.' //@todo meilleurs mails
				};

				mailgun.messages().send(mailData, (err, body) => {
					if(err) { return res.status(500).json({}); }
					return res.status(200).json({});
				});
			});	
		} else {
			return res.status(200).json({});	
		}
	});
	
};


module.exports = {
	getSubscription, postSubscription, getpayment, postpayment, stripeHooks, deleteSubscription
}