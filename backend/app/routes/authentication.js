/**
 *			RoadToPermis!
 *	./app/routes/authentication.js
 * 	Authentication routes
 */

const 	account 	= require("../models/account"),
		sanitizer 	= require('sanitizer'),
		jwt			= require('jsonwebtoken'),
		config		= require('../config'),
		uuid 		= require('uuid/v4'),
		mailgun		= require('mailgun-js')({	apiKey:config.tokens.mailgunprivate, domain: config.tokens.domain });


var truncateUser = function(u) {
	return {
		email: u.email,
		usernonce: u.userNonce,
    _id: u._id,
		vendorNonce: uuid()
	}
};

/**
 * POST /api/v1/authenticate
 * Authenticates a user using its credentials. Returns an token.
 */
var auth = function(req, res) {
	req.checkBody({
		'email': {
			notEmpty: {
				errorMessage: "L'adresse email ne peut être vide"
			}
		}, 'password': {
			notEmpty: {
				errorMessage: "Le mot de passe ne peut être vide"
			}
		}
	});

	// Check missing parameters in request body
	req.getValidationResult().then(function(result) {
		result.useFirstErrorOnly();
	    if (!result.isEmpty()) {
	    	// Bad request
			return res.status(400).json({
				error: true,
				message: result.array()
			});
		} else {
			// Body sanitization 
			req.body.email 		= sanitizer.escape(req.body.email);

			// Attemtp to fetch the user
			account.findOne({	email: req.body.email	}, (err, user) => {
				if(err) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }

				if(!user) { // User don't exist => error 
					return res.status(401).json({
						error: true,
						message: "Les identifiants fournis sont invalides."
					});
				}

				user.comparePassword(req.body.password, (err, isMatch) =>  { // Is password OK ? 
					if(err) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }

					if(!isMatch) { // => it isn't, => error 
						return res.status(401).json({
							error: true,
							message: "Les identifiants fournis sont invalides."
						});
					}

					// Sign a new JWT using the app's secret
					var token = jwt.sign(truncateUser(user), config.secret, {
						expiresIn: config.tokensExpiry
					});

					var expiry = new Date();
					expiry.setSeconds(expiry.getSeconds() + config.tokensExpiry);

					return res.status(200).json({ // All is good => return the JWT
						error: false, 
						message: {
		  					access_token: token,
		  					expiry_date: expiry,
		  					userid: user._id.toString()
		  				}
	  				});
				});
			});
		}
	});
};

/**
 * GET /api/v1/token
 * Returns the token's status
 */
var check = function(req, res) {
	return res.status(200).json({error: false, message: ''});
};

var get = function(req, res) {
  account.findOne({_id: req.decoded._id}).populate('school').exec(function(err, user) {
    if(err) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }

    return res.status(200).json({error: false, message: {
      email: user.email,
      profile: user.profile,
      admin: user.admin,
      drivingSchool: user.drivingSchool,
      monitor: user.monitor,
      school: user.school,
      id: user.id,
    }});
  });
};

module.exports = {
	auth, check, get
};