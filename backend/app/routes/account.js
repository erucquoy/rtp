/**
 *			RoadToPermis!
 *	./app/routes/account.js
 * 	Account routes
 */

const 	account 	= require("../models/account"),
		sanitizer 	= require('sanitizer'),
		config		= require('../config'),
		uuid 		= require('uuid/v4')
		mailgun		= require('mailgun-js')({	apiKey:config.tokens.mailgunprivate, domain: config.tokens.domain }),
		stripe 		= require("stripe")(config.tokens.stripepriv);

var truncateUser = function(u) {
  return {
    _id: u._id,
    email: u.email,
    firstName: u.profile.firstName,
    lastName: u.profile.lastName,
    active: u.active,
		admin: u.admin,
		monitor: u.monitor,
    school: u.school,
  }
};

/**
 * POST /api/v1/account
 * Creates a new account for the given credentials
 */
var create = function(req, res) {
	// Mandatory body parameters
	req.checkBody({
		'email': {
			notEmpty: {
				errorMessage: "L'adresse email ne peut être vide"
			}
		}, 'password': {
			notEmpty: {
				errorMessage: "Le mot de passe ne peut être vide"
			}, minPwLength: {
				errorMessage: "Le mot de passe doit contenir au moins 4 caractères"
			}
		}, 'firstName': {
			notEmpty: {
				errorMessage: "Le prénom ne peut être vide"
			}
		}, 'lastName': {
			notEmpty: {
				errorMessage: "Le nom de famille ne peut être vide"
			}
		}
	});

	// Is body ok ? 
	req.getValidationResult().then(function(result) {
		result.useFirstErrorOnly();
	    if (!result.isEmpty()) { // It isn't => bad request
			return res.status(400).json({
				error: true,
				message: result.array()
			});
		} else { // Body is OK. 
			// Input sanitization
			req.body.email 		= sanitizer.escape(req.body.email);
			req.body.firstName  = sanitizer.escape(req.body.firstName);
			req.body.lastName  	= sanitizer.escape(req.body.lastName);

        // Email already taken ?
        account.findOne({
          email: req.body.email
        }, function(err, user) {
          if(err) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }

          if(user) {
            return res.status(400).json({
              error: true,
              message: "Cette adresse email est déjà utilisée"
            });
          } else {
            // Create the account, send the confirmation email
            var ac = new account({
              email: req.body.email,
              password: req.body.password,
              active: true,
              profile: {
                firstName: req.body.firstName,
                lastName: req.body.lastName
              }, confirmationToken: uuid()
            }).save(function(err, us) {
              if(err) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }
              return res.status(201).json({
                error: false,
                message: ''
              });
            });
          }
        });
		}
	});
};

/**
 * POST /api/v1/resetpassword
 * Request a password reset
 **/
var pwResetRequest = function(req, res) {
	req.checkBody({
		'email': {
			notEmpty: {
				errorMessage: "L'adresse email ne peut être vide"
			}
		}
	});

	// Check missing parameters in request body
	req.getValidationResult().then(function(result) {
		result.useFirstErrorOnly();
	    if (!result.isEmpty()) {
	    	// Bad request
			return res.status(400).json({
				error: true,
				message: result.array()
			});
		} else {
			// Body sanitization 
			req.body.email 		= sanitizer.escape(req.body.email);

			// Find the user 
			account.findOne({
				email: req.body.email
			}, (err, user) => {
				if(err) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }

				// If the user don't exist, we pretend it does, and return a "if an account exists, then check your email"
				// For obvious reasons, i hope.
				if(!user) {
					return res.status(200).json({
						error: false,
						message: 'ok' // @todo meilleur message
					});
				} else {
					// User exists, set a reset token, send an email, all that good stuff

					var expiry 			= new Date();
					expiry.setHours(expiry.getHours() + 2); // The request will expire in 2 hours

					user.resetToken 	= uuid();
					user.resetLimitDate = expiry;

					user.save((err, newUser) => {
						if(err) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }

						// @todo better emails
						var mailData = {
							from: 'RoadToPermis <ne.pas.repondre@' + config.tokens.domain + '>',
							to: newUser.email,
							subject: 'Réinitialisation de votre mot de passe RoadToPermis',
							text: user.resetToken
						};

						mailgun.messages().send(mailData, (err, body) => {
							if(err) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }

							return res.status(200).json({
								error: false,
								message: 'ok' // @todo meilleur message
							});
						});
					});
				}
			});
		}
	});
};

/**
 * POST /api/v1/doresetpassword
 * Resets the user's password
 **/
var pwDoReset = function(req, res) {
	req.checkBody({
		'email': {
			notEmpty: {
				errorMessage: "L'adresse email ne peut être vide"
			}
		}, 'token': {
			notEmpty: {
				errorMessage: "Le token ne peut être vide"
			}
		}, 'newpw': {
			notEmpty: {
				errorMessage: "Le mot de passe ne peut être vide"
			}, minPwLength: {
				errorMessage: "Le mot de passe doit contenir au moins 4 caractères"
			}
		}
	});

	// Check missing parameters in request body
	req.getValidationResult().then(function(result) {
		result.useFirstErrorOnly();
	    if (!result.isEmpty()) {
	    	// Bad request
			return res.status(400).json({
				error: true,
				message: result.array()
			});
		} else {
			req.body.email = sanitizer.escape(req.body.email);
			req.body.token = sanitizer.escape(req.body.token);

			account.findOne({ email: req.body.email }, (err, usr) => {
				if(err) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }

				// User exists ? 
				if(!usr) {
					return res.status(400).json({
						error: true, 
						message: 'Votre demande ne peut être traîtée. Veuillez refaire une requête de réinitialisation de mot de passe. Si le problème persiste, merci de contacter le support.'
					});
				}

				// Token ok ? 
				if(usr.resetToken !== req.body.token) {
					return res.status(400).json({
						error: true, 
						message: 'Votre demande ne peut être traîtée. Veuillez refaire une requête de réinitialisation de mot de passe. Si le problème persiste, merci de contacter le support.'
					});
				}

				// Token expired ?
				// @todo

				// Token is ok, user exists, update the password.
				usr.password	= req.body.newpw;
				usr.resetToken	= '';

				usr.save((err, nusr) => {
					if(err) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); }
					return res.status(200).json({error: false, message: ''});
				});
			});
		}
	});
};

/** 
 * POST /api/v1/activate 
 * Activate an account 
 */ 
var activate = function(req, res) { 
	// Mandatory body parameters 
	req.checkBody({ 
		'email': { 
			notEmpty: { 
				errorMessage: "L'adresse email ne peut être vide" 
			} 
		}, 'token': { 
			notEmpty: { 
				errorMessage: "Le token d'activation ne peut pas être vide" 
			} 
		} 
	}); 

	// Is body ok ?  
	req.getValidationResult().then(function(result) { 
		result.useFirstErrorOnly(); 
		if (!result.isEmpty()) { // It isn't => bad request 
			return res.status(400).json({ 
				error: true, 
				message: result.array() 
			}); 
		} else { 
			req.body.email     = sanitizer.escape(req.body.email); 
			req.body.token     = sanitizer.escape(req.body.token); 

			account.findOne(  {email: req.body.email}   , (err, user) => { 
				if(err) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); } 

				// User exists ? 
				if(!user) return res.status(401).json({error: true, message: "Une erreur s'est produite. Veuillez vérifier l'email et le token."}); 

				// Is the token ok ?  
				if(user.confirmationToken !== req.body.token) return res.status(401).json({error: true, message: "Une erreur s'est produite. Veuillez vérifier l'email et le token."}); 

				// User exists, token is ok, activate the account. 
				user.active = true; 
				user.save((err, newUser) => { 
					if(err) { console.log(err); return res.status(500).json({error: true, message: 'Internal server error'}); } 
					return res.status(200).json({error: false, message: ''}); 
				}); 
			}); 
		} 
	}); 
};


/***  ADMIN PART ***/
/**
 * GET /api/v1/account
 * Get all the account
 */
var list = function(req, res) {
	var offset = req.query.offset;
  if (typeof(offset) !== 'number') {
    offset = 0;
  }
  account.find({}).skip(parseInt(offset)).limit(parseInt(req.query.limit)).exec((err, accounts) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    for (let i = 0; i < accounts.length; i++) {
      accounts[i] = truncateUser(accounts[i]);
    }
    account.count({}, function(err, c) {
      return res.status(200).json({ count: c, data: accounts});
    });
});
};


/**
 * GET /api/v1/account/:_id
 * Get one account depending of the id in parameters
 */
var get = function(req, res) {
  account.findOne({_id: sanitizer.escape(req.param('_id'))}, (err, account) => {
    if(err || !account) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    return res.status(200).json(truncateUser(account));
});
};

/**
 * PUT /api/v1/account/:_id
 * update a account for the given data
 */
var update = function(req, res) {
  account.findOne({_id: sanitizer.escape(req.params._id)}, (err, account) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    if(!account) {
    return res.status(404).json({error: true, message: 'Compte non trouvé'});
  }
  // Mandatory body parameters
  req.checkBody({
    'email': {
      notEmpty: {
        errorMessage: "L'adresse email ne peut être vide"
      }, isEmail: {
        errorMessage: "L'adresse email semble être invalide."
      }, isValidEmail: {
        errorMessage: "L'adresse email semble être invalide."
      }
    },
    'firstName': {
      notEmpty: {
        errorMessage: "Le prénom ne peut être vide"
      }
    },
    'lastName': {
      notEmpty: {
        errorMessage: "Le nom de famille ne peut être vide"
      }
    }
  });

  // Is body ok ?
  req.getValidationResult().then(function (result) {
    result.useFirstErrorOnly();
    if (!result.isEmpty()) { // It isn't => bad request
      return res.status(400).json({
        error: true,
        message: result.array()
      });
    } else { // Body is OK.
      // Input sanitization
      req.body.email 		= sanitizer.escape(req.body.email);
      req.body.firstName  = sanitizer.escape(req.body.firstName);
      req.body.lastName  	= sanitizer.escape(req.body.lastName);
      req.body.active = req.body.active ? true : false;
			req.body.admin = req.body.admin ? true : false;
			req.body.monitor = req.body.monitor ? true : false;
      req.body.school  	= sanitizer.escape(req.body.school);

      account.email = req.body.email;
      account.profile.firstName = req.body.firstName;
      account.profile.lastName = req.body.lastName;
      account.active = req.body.active;
			account.admin = req.body.admin;
			account.monitor = req.body.monitor;
      account.school = req.body.school;
      account.save(function (err, us) {
        if (err) {
          console.log(err);
          return res.status(500).json({error: true, message: 'Internal server error'});
        }
        return res.status(201).json({
          error: false,
          message: ''
        });
      });
    }
  });
});
};

module.exports = {
	create, pwResetRequest, pwDoReset, activate, list, get, update
}