/**
 *			RoadToPermis!
 *	./app/routes/centre.js
 * 	Article routes
 */

const 	centre 	= require("../models/centre"),
        image 	    = require("../models/image"),
        sanitizer 	= require('sanitizer'),
        config		= require('../config'),
        fs          = require('fs');


/**
 * POST /api/v1/centre
 * Creates a new centre for the given data
 */
var create = function(req, res) {
  // Mandatory body parameters
  req.checkBody({
    'city': {
      notEmpty: {
        errorMessage: "La ville ne peut être vide"
      }
    },
    'content': {
      notEmpty: {
        errorMessage: "Le contenu ne peut être vide"
      }
    },
  });

  // Is body ok ?
  req.getValidationResult().then(function(result) {
    result.useFirstErrorOnly();
    if (!result.isEmpty()) { // It isn't => bad request
      return res.status(400).json({
        error: true,
        message: result.array()
      });
    } else { // Body is OK.
      // Input sanitization
      req.body.city 		= sanitizer.escape(req.body.city);
      req.body.content  = sanitizer.escape(req.body.content);
      req.body.isActive = req.body.isActive ? true : false;

      var ac = new centre({
        city: req.body.city,
        content: req.body.content,
        isActive: req.body.isActive,
      }).save(function(err, us) {
        if (err) {
          console.log(err);
          return res.status(500).json({error: true, message: 'Internal server error'});
        }

        req.body.images.forEach(t => {
            image.findOne({_id: sanitizer.escape(t)}, (err, image) => {
              if (err || !image)
                return;
              us.images.push(image);
              us.save();
          });
        });
        return res.status(201).json({
          error: false,
          message: ''
        });
      });
    }
  });
};

/**
 * GET /api/v1/centre
 * Get all the centre
 */
var list = function(req, res) {
  var offset = req.query.offset;
  if (typeof(offset) !== 'number') {
    offset = 0;
  }
  centre.find({}).skip(parseInt(offset)).limit(parseInt(req.query.limit)).exec((err, centres) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

    centre.count({}, function(err, c) {
      return res.status(200).json({ count: c, data: centres});
    });
  });
};

/**
 * GET /api/v1/centre/:_id
 * Get one centre depending of the id in parameters
 */
var get = function(req, res) {
  centre.findOne({_id: sanitizer.escape(req.param('_id'))}).populate('images').exec((err, centre) => {
      if(err || !centre) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      for (let i = 0; i < centre.images.length; i++) {
        centre.images[i] = 'image/' + centre.images[i]._id;
      }

      return res.status(200).json(centre);
  });
};

/**
 * PUT /api/v1/centre/:_id
 * update an centre for the given data
 */
var update = function(req, res) {
  centre.findOne({_id: sanitizer.escape(req.param('_id'))}, (err, centre) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      if(!centre) {
        return res.status(404).json({error: true, message: 'Article non trouvé'});
      }
      // Mandatory body parameters
      req.checkBody({
      'city': {
        notEmpty: {
          errorMessage: "La ville ne peut être vide"
        }
      },
      'content': {
        notEmpty: {
          errorMessage: "Le contenu ne peut être vide"
        }
      },
    });

    // Is body ok ?
    req.getValidationResult().then(function (result) {
      result.useFirstErrorOnly();
      if (!result.isEmpty()) { // It isn't => bad request
        return res.status(400).json({
          error: true,
          message: result.array()
        });
      } else { // Body is OK.
        // Input sanitization
        req.body.city = sanitizer.escape(req.body.city);
        req.body.content = sanitizer.escape(req.body.content);
        req.body.isActive = req.body.isActive ? true : false;

        if (req.body.images == undefined)
          req.body.images = [];

        centre.city = req.body.city;
        centre.content = req.body.content;
        centre.isActive = req.body.isActive;
        centre.save(function (err, us) {
          if (err) {
            console.log(err);
            return res.status(500).json({error: true, message: 'Internal server error'});
          }

          req.body.images.forEach(t => {
            image.findOne({_id: sanitizer.escape(t)}, (err, image) => {
              if (err || !image)
                return;
              us.images.push(image);
              us.save();
            });
          });

          return res.status(201).json({
            error: false,
            message: ''
          });
        });
      }
    });
  });
};

/**
 * DELETE /api/v1/centre/:_id
 * delete one centre depending of the id in parameters
 */
var deleteCentre = function(req, res) {
  centre.findOneAndRemove({_id: sanitizer.escape(req.param('_id'))}, (err, centre) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    for (let i = 0; i < centre.images.length; i++) {
      image.findOneAndRemove({_id: centre.images[i]}, (err, image) => {
        if (fs.existsSync(image.source)) {
          fs.unlinkSync(image.source);
        }
      });
    }

    return res.status(200).json({
      error: false,
      message: ''
    });
});
};

module.exports = {
  create,
  list,
  get,
  update,
  deleteCentre,
};