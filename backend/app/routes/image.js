/**
 *			RoadToPermis!
 *	./app/routes/video.js
 * 	Video routes
 */

const 	image 	    = require("../models/image"),
        article	    = require("../models/article"),
        sanitizer 	= require('sanitizer'),
        path        = require('path'),
        config		  = require('../config'),
        fs          = require('fs');


/**
 * GET /api/v1/image/:_id
 * Get one image depending of the id in parameters
 */
var get = function(req, res) {
  image.findOne({_id: sanitizer.escape(req.param('_id'))}, (err, image) => {
    if(err || !image) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }
    if (fs.existsSync(path.join(__dirname, '../../', image.source))) {
      res.sendFile(path.join(__dirname, '../../', image.source));
    } else {
      return res.status(404).json({error: true, message: 'Image not found'});
    }
  });
};

/**
 * OPTIONS /api/v1/image
 * Return a confirm for the preflight
 */
var options = function(req, res) {
    return res.status(200).send({
      error: false,
      message: ''
    });
};


/**
 * POST /api/v1/image
 * Creates a new image for the uploaded image
 */
var create = function(req, res) {
  if (!req.file)
    return res.status(400).send({
      error: true,
      message: 'Aucun fichier uploadé'
    });

  var ac = new image({
    source: req.file.path,
  }).save(function(err, us) {
    if (err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    return res.status(201).json({
      error: false,
      message: '',
      id: us._id,
    });
  });
};

/**
 * DELETE /api/v1/image/:_id
 * delete one image depending of the id in parameters
 */
var deleteImage = function(req, res) {
  image.findOneAndRemove({_id: sanitizer.escape(req.param('_id'))}, (err, image) => {
      if(err || !image) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      if (fs.existsSync(image.source)) {
      fs.unlinkSync(image.source);
    }

      article.update({images: req.param('_id')}, { $pull: {images: req.param('_id')}}).exec();

      return res.status(200).json({
        error: false,
        message: ''
      });
  });
};


module.exports = {
  get,
  options,
  create,
  deleteImage,
};