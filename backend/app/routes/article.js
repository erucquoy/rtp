/**
 *			RoadToPermis!
 *	./app/routes/article.js
 * 	Article routes
 */

const 	article 	= require("../models/article"),
        image 	    = require("../models/image"),
        sanitizer 	= require('sanitizer'),
        config		= require('../config'),
        fs          = require('fs');

/**
 * GET /api/v1/article
 * Get all the article
 */
var list = function(req, res) {
  var offset = req.query.offset;
  if (typeof(offset) !== 'number') {
    offset = 1;
  }
  console.log("LSQKDHJQLSKJDLKQSJDLQJKSDLKJQSD");
  article.find({isActive: true}).skip(offset).limit(parseInt(req.query.limit)).exec((err, articles) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    article.count({isActive: true}, function(err, c) {
      return res.status(200).json({ count: c, data: articles});
    });
  });
};

/**
 * POST /api/v1/adminarticle
 * Creates a new article for the given data
 */
var create = function(req, res) {
  // Mandatory body parameters
  req.checkBody({
    'title': {
      notEmpty: {
        errorMessage: "Le titre ne peut être vide"
      }
    },
    'content': {
      notEmpty: {
        errorMessage: "Le contenu ne peut être vide"
      }
    },
    'category': {
      notEmpty: {
        errorMessage: "La catégorie ne peut être vide"
      }
    },
    'typeClient': {
      notEmpty: {
        errorMessage: "Le type de client ne peut être vide"
      }
    }
  });

  // Is body ok ?
  req.getValidationResult().then(function(result) {
    result.useFirstErrorOnly();
    if (!result.isEmpty()) { // It isn't => bad request
      return res.status(400).json({
        error: true,
        message: result.array()
      });
    } else { // Body is OK.
      // Input sanitization
      req.body.title 		= sanitizer.escape(req.body.title);
      req.body.content  = sanitizer.escape(req.body.content);
      req.body.youtubeVideo = sanitizer.escape(req.body.youtubeVideo);
      req.body.category = sanitizer.escape(req.body.category);
      req.body.typeClient = sanitizer.escape(req.body.typeClient);
      req.body.isActive = req.body.isActive ? true : false;

      var ac = new article({
        title: req.body.title,
        content: req.body.content,
        youtubeVideo: req.body.youtubeVideo,
        category: req.body.category,
        typeClient: req.body.typeClient,
        isActive: req.body.isActive,
      }).save(function(err, us) {
        if (err) {
          console.log(err);
          return res.status(500).json({error: true, message: 'Internal server error'});
        }

        req.body.images.forEach(t => {
            image.findOne({_id: sanitizer.escape(t)}, (err, image) => {
              if (err || !image)
                return;
              us.images.push(image);
              us.save();
          });
        });
        return res.status(201).json({
          error: false,
          message: ''
        });
      });
    }
  });
};

/**
 * GET /api/v1/adminarticle
 * Get all the article
 */
var listAdmin = function(req, res) {
  article.find({}).skip(parseInt(req.query.offset)).limit(parseInt(req.query.limit)).exec((err, articles) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

    article.count({}, function(err, c) {
      return res.status(200).json({ count: c, data: articles});
    });
  });
};

/**
 * GET /api/v1/adminarticle/:_id
 * Get one article depending of the id in parameters
 */
var get = function(req, res) {
  article.findOne({_id: sanitizer.escape(req.param('_id'))}).populate('images').exec((err, article) => {
      if(err || !article) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      for (let i = 0; i < article.images.length; i++) {
        article.images[i] = 'image/' + article.images[i]._id;
      }

      return res.status(200).json(article);
  });
};

/**
 * PUT /api/v1/adminarticle/:_id
 * update an article for the given data
 */
var update = function(req, res) {
  article.findOne({_id: sanitizer.escape(req.param('_id'))}, (err, article) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      if(!article) {
        return res.status(404).json({error: true, message: 'Article non trouvé'});
      }
      // Mandatory body parameters
      req.checkBody({
      'title': {
        notEmpty: {
          errorMessage: "Le titre ne peut être vide"
        }
      },
      'content': {
        notEmpty: {
          errorMessage: "Le contenu ne peut être vide"
        }
      },
      'category': {
        notEmpty: {
          errorMessage: "La catégorie ne peut être vide"
        }
      },
      'typeClient': {
        notEmpty: {
          errorMessage: "Le type de client ne peut être vide"
        }
      }
    });

    // Is body ok ?
    req.getValidationResult().then(function (result) {
      result.useFirstErrorOnly();
      if (!result.isEmpty()) { // It isn't => bad request
        return res.status(400).json({
          error: true,
          message: result.array()
        });
      } else { // Body is OK.
        // Input sanitization
        req.body.title = sanitizer.escape(req.body.title);
        req.body.content = sanitizer.escape(req.body.content);
        req.body.youtubeVideo = sanitizer.escape(req.body.youtubeVideo);
        req.body.category = sanitizer.escape(req.body.category);
        req.body.typeClient = sanitizer.escape(req.body.typeClient);
        req.body.isActive = req.body.isActive ? true : false;

        if (req.body.images == undefined)
          req.body.images = [];

        article.title = req.body.title;
        article.content = req.body.content;
        article.youtubeVideo = req.body.youtubeVideo;
        article.category = req.body.category;
        article.typeClient = req.body.typeClient;
        article.isActive = req.body.isActive;
        article.save(function (err, us) {
          if (err) {
            console.log(err);
            return res.status(500).json({error: true, message: 'Internal server error'});
          }

          req.body.images.forEach(t => {
            image.findOne({_id: sanitizer.escape(t)}, (err, image) => {
              if (err || !image)
                return;
              us.images.push(image);
              us.save();
            });
          });

          return res.status(201).json({
            error: false,
            message: ''
          });
        });
      }
    });
  });
};

/**
 * DELETE /api/v1/adminarticle/:_id
 * delete one article depending of the id in parameters
 */
var deleteArticle = function(req, res) {
  article.findOneAndRemove({_id: sanitizer.escape(req.param('_id'))}, (err, article) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    for (let i = 0; i < article.images.length; i++) {
      image.findOneAndRemove({_id: article.images[i]}, (err, image) => {
        if (fs.existsSync(image.source)) {
          fs.unlinkSync(image.source);
        }
      });
    }

    return res.status(200).json({
      error: false,
      message: ''
    });
});
};

module.exports = {
  create,
  list,
  listAdmin,
  get,
  update,
  deleteArticle,
};