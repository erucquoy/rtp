/**
 *			RoadToPermis!
 *	./app/routes/fiche.js
 * 	fiche routes
 */

const 	fiche 	= require("../models/fiche"),
        account 	= require("../models/account"),
        sanitizer 	        = require('sanitizer'),
        config		          = require('../config'),
        fs                  = require('fs');


/**
 * GET /api/v1/admin/fiche
 * Get all the centre commentaire
 */
var list = function(req, res) {
  var offset = req.query.offset;
  if (typeof(offset) !== 'number') {
    offset = 0;
  }
  fiche.find({student: req.query.account}).skip(parseInt(offset)).limit(parseInt(req.query.limit)).exec((err, fiches) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

    fiche.count({}, function(err, c) {
      return res.status(200).json({ count: c, data: fiches});
    });
  });
};

/**
 * GET /api/v1/admin/fiche/:_id OR GET /api/v1/fiche/:_id
 * Get one centre commentaire depending of the id in parameters
 */
var get = function(req, res) {
  fiche.findOne({_id: sanitizer.escape(req.param('_id'))}).exec((err, fiche) => {
      if(err || !fiche) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      return res.status(200).json(fiche);
  });
};

/**
 * GET /api/v1/fiche/user/:_id
 * Get one centre commentaire depending of the id in parameters
 */
var getByUser = function(req, res) {
  account.findOne({_id: req.decoded._id, /*$or: [{ drivingSchool: true }, { monitor: true }]*/}).populate('school').exec(function (err, user) {
    if (err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }
    if (typeof(user.school) === 'undefined') {
      return res.status(500).json({error: true, message: 'Internal server error'});
    }
    fiche.find({student: sanitizer.escape(req.body._id), school: user.school._id}).populate('monitor').sort({date: 'asc'}).exec((err, fiches) => {
      if (err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      if (!fiches) {
        return res.status(404).json({error: true, message: 'Leçons non trouvé'});
      }

      fiche.count({student: sanitizer.escape(req.param('_id')), school: user.school._id}, function(err, c) {
        return res.status(200).json({ count: c, data: fiches});
      });

    });
  });
};

/**
 * POST /api/v1/fiche
 * Creates a new fiche for the given data
 */
var create = function(req, res) {
  // Mandatory body parameters
  req.checkBody({
    'time': {
      notEmpty: {
        errorMessage: "Le temps ne peut être vide"
      }
    },
    'date': {
      notEmpty: {
        errorMessage: "La date ne peut être vide"
      }
    },
    'school': {
      notEmpty: {
        errorMessage: "L'école ne peut être vide"
      }
    },
    'student': {
      notEmpty: {
        errorMessage: "L'élève ne peut être vide"
      }
    },
    'monitor': {
      notEmpty: {
        errorMessage: "Le moniteur ne peut être vide"
      }
    },
  });

  // Is body ok ?
  req.getValidationResult().then(function(result) {
    result.useFirstErrorOnly();
    if (!result.isEmpty()) { // It isn't => bad request
      return res.status(400).json({
        error: true,
        message: result.array()
      });
    } else { // Body is OK.
      // Input sanitization
      req.body.name 		= sanitizer.escape(req.body.name);
      req.body.content 		= sanitizer.escape(req.body.content);
      req.body.mark 		= sanitizer.escape(req.body.mark);
      req.body.school 		= sanitizer.escape(req.body.school);
      req.body.student 		= sanitizer.escape(req.body.student);
      req.body.monitor 		= sanitizer.escape(req.body.monitor);
      req.body.time 		= sanitizer.escape(req.body.time);
      console.log(req.body.date)
      req.body.date 		= new Date(req.body.date);
      req.body.toWork = req.body.toWork ? sanitizer.escape(req.body.toWork).split("\n") : '';
      req.body.achievements = req.body.achievements ? sanitizer.escape(req.body.achievements).split("\n") : '';

      var ac = new fiche({
        name: req.body.name,
        content: req.body.content,
        mark: req.body.mark,
        school: req.body.school,
        student: req.body.student,
        monitor: req.body.monitor,
        time: req.body.time,
        date: req.body.date,
        achievements: req.body.achievements,
        toWork: req.body.toWork
      }).save(function(err, us) {
        if (err) {
          console.log(err);
          return res.status(500).json({error: true, message: 'Internal server error'});
        }

        return res.status(201).json({
          error: false,
          message: ''
        });
      });
    }
  });
};

/**
 * PUT /api/v1/fiche/:_id
 * update an centre commentaire for the given data
 */
var update = function(req, res) {
  account.findOne({_id: req.decoded._id, $or: [{ drivingSchool: true }, { monitor: true }]}).populate('school').exec(function (err, user) {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }
    fiche.findOne({_id: sanitizer.escape(req.param('_id')), school: user.school._id}, (err, fiche) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      if(!fiche) {
        return res.status(404).json({error: true, message: 'Leçon non trouvé'});
      }
      // Mandatory body parameters
      req.checkBody({
        'time': {
          notEmpty: {
            errorMessage: "Le temps ne peut être vide"
          }
        },
        'date': {
          notEmpty: {
            errorMessage: "La date ne peut être vide"
          }
        },
        'school': {
          notEmpty: {
            errorMessage: "L'école ne peut être vide"
          }
        },
        'student': {
          notEmpty: {
            errorMessage: "L'élève ne peut être vide"
          }
        },
        'monitor': {
          notEmpty: {
            errorMessage: "Le moniteur ne peut être vide"
          }
        },
      });

      // Is body ok ?
      req.getValidationResult().then(function (result) {
        result.useFirstErrorOnly();
        if (!result.isEmpty()) { // It isn't => bad request
          return res.status(400).json({
            error: true,
            message: result.array()
          });
        } else { // Body is OK.
          // Input sanitization
          req.body.name 		= sanitizer.escape(req.body.name);
          req.body.content 		= sanitizer.escape(req.body.content);
          req.body.mark 		= sanitizer.escape(req.body.mark);
          if (user.drivingSchool || user.admin) {
            req.body.school 		= sanitizer.escape(req.body.school);
            req.body.student 		= sanitizer.escape(req.body.student);
            req.body.monitor 		= sanitizer.escape(req.body.monitor);
          }
          req.body.time 		= sanitizer.escape(req.body.time);
          req.body.date 		= sanitizer.escape(req.body.date);
          req.body.toWork = typeof req.body.toWork == 'object' ? sanitizer.escape(req.body.toWork).split("\n") : sanitizer.escape(req.body.toWork);
          req.body.achievements = typeof req.body.achievements == 'object' ? sanitizer.escape(req.body.achievements).split("\n") : sanitizer.escape(req.body.achievements);

          fiche.name = req.body.name;
          fiche.content = req.body.content;
          fiche.mark = req.body.mark;
          fiche.school = req.body.school;
          fiche.student = req.body.student;
          fiche.monitor = req.body.monitor;
          fiche.time = req.body.time;
          fiche.date = req.body.date;
          fiche.toWork = req.body.toWork;
          fiche.achievements = req.body.achievements;

          fiche.save(function (err, us) {
            if (err) {
              console.log(err);
              return res.status(500).json({error: true, message: 'Internal server error'});
            }

            return res.status(201).json({
              error: false,
              message: ''
            });
          });
        }
      });
    });
  });
};


/**
 * PUT /api/v1/admin/fiche/:_id
 * update an centre commentaire for the given data
 */
var updateAdmin = function(req, res) {
  fiche.findOne({_id: sanitizer.escape(req.param('_id'))}, (err, fiche) => {
      if(err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      if(!fiche) {
        return res.status(404).json({error: true, message: 'Commentaire non trouvé'});
      }
      // Mandatory body parameters
      req.checkBody({
      'content': {
        notEmpty: {
          errorMessage: "Le contenu ne peut être vide"
        }
      },
    });

    // Is body ok ?
    req.getValidationResult().then(function (result) {
      result.useFirstErrorOnly();
      if (!result.isEmpty()) { // It isn't => bad request
        return res.status(400).json({
          error: true,
          message: result.array()
        });
      } else { // Body is OK.
        // Input sanitization
        req.body.content = sanitizer.escape(req.body.content);

        fiche.content = req.body.content;
        fiche.save(function (err, us) {
          if (err) {
            console.log(err);
            return res.status(500).json({error: true, message: 'Internal server error'});
          }

          return res.status(201).json({
            error: false,
            message: ''
          });
        });
      }
    });
  });
};

/**
 * DELETE /api/v1/admin/fiche/:_id OR DELETE /api/v1/fiche/:_id
 * delete one centre commentaire depending of the id in parameters
 */
var deleteFiche = function(req, res) {
  account.findOne({_id: req.decoded._id, $or: [{ drivingSchool: true }, { monitor: true }, { admin: true }]}).populate('school').exec(function (err, user) {
    if (err || !user) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    let params = { _id: sanitizer.escape(req.param('_id')) };

    if (!user.admin && user.drivingSchool) {
      params.school = user.school._id;
    } else if (!user.admin && user.monitor) {
      params.monitor = user._id;
      params.school = user.school._id;
    }

    fiche.findOneAndRemove(params, (err, fiche) => {
      if (err) {
        console.log(err);
        return res.status(500).json({error: true, message: 'Internal server error'});
      }

      return res.status(200).json({
        error: false,
        message: ''
      });
    });
  });
};

module.exports = {
  list,
  get,
  getByUser,
  create,
  update,
  updateAdmin,
  deleteFiche,
};