/**
 *			RoadToPermis!
 *	./app/routes/temoignage.js
 * 	Temoignage routes
 */

const 	temoignage 	    = require("../models/temoignage"),
        image           = require("../models/image"),
        sanitizer 	    = require('sanitizer'),
        config		      = require('../config');


/**
 * GET /api/v1/temoignage
 * Get all the temoignage whoch are active
 */
var list = function(req, res) {
  temoignage.find({isActive: true}).skip(parseInt(req.query.offset)).limit(parseInt(req.query.limit)).exec((err, temoignages) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    temoignage.count({isActive: true}, function(err, c) {
      return res.status(200).json({ count: c, data: temoignages});
    });
  });
};

/**
 * POST /api/v1/admin/temoignage
 * Creates a new temoignage for the given data
 */
var create = function(req, res) {
  // Mandatory body parameters
  req.checkBody({
    'firstName': {
      notEmpty: {
        errorMessage: "Le titre ne peut être vide"
      }
    },
    'content': {
      notEmpty: {
        errorMessage: "Le contenu ne peut être vide"
      }
    },
  });

  // Is body ok ?
  req.getValidationResult().then(function(result) {
    result.useFirstErrorOnly();
    if (!result.isEmpty()) { // It isn't => bad request
      return res.status(400).json({
        error: true,
        message: result.array()
      });
    } else { // Body is OK.
      // Input sanitization
      req.body.firstName 		= sanitizer.escape(req.body.firstName);
      req.body.content = sanitizer.escape(req.body.content);
      req.body.date = sanitizer.escape(req.body.date);
      req.body.isActive = req.body.isActive ? true : false;

      var ac = new temoignage({
        firstName: req.body.firstName,
        content: req.body.content,
        date: req.body.date,
        isActive: req.body.isActive,
      }).save(function(err, us) {
        if (err) {
          console.log(err);
          return res.status(500).json({error: true, message: 'Internal server error'});
        }

        req.body.images.forEach(t => {
          image.findOne({_id: sanitizer.escape(t)}, (err, image) => {
            if (err || !image)
              return;
            us.images.push(image);
            us.save();
          });
        });

        return res.status(201).json({
          error: false,
          message: ''
        });
      });
    }
  });
};

/**
 * GET /api/v1/admin/temoignage
 * Get all the temoignage
 */
var listAdmin = function(req, res) {
  var offset = req.query.offset;
  if (typeof(offset) !== 'number') {
    offset = 0;
  }
  temoignage.find({}).skip(parseInt(offset)).limit(parseInt(req.query.limit)).exec((err, temoignages) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    temoignage.count({}, function(err, c) {
      return res.status(200).json({ count: c, data: temoignages});
    });
  });
};

/**
 * GET /api/v1/admin/temoignage/:_id
 * Get one temoignage depending of the id in parameters
 */
var get = function(req, res) {
  temoignage.findOne({_id: sanitizer.escape(req.param('_id'))}).populate('images').exec((err, temoignage) => {
    if(err || !temoignage) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    for (let i = 0; i < temoignage.images.length; i++) {
      temoignage.images[i] = 'image/' + temoignage.images[i]._id;
    }

    return res.status(200).json(temoignage);
  });
};

/**
 * PUT /api/v1/admin/temoignage/:_id
 * update a temoignage for the given data
 */
var update = function(req, res) {
  temoignage.findOne({_id: sanitizer.escape(req.param('_id'))}, (err, temoignage) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    if(!temoignage) {
    return res.status(404).json({error: true, message: 'Video non trouvé'});
  }
  // Mandatory body parameters
  req.checkBody({
    'firstName': {
      notEmpty: {
        errorMessage: "Le titre ne peut être vide"
      }
    },
    'content': {
      notEmpty: {
        errorMessage: "Le contenu ne peut être vide"
      }
    },
  });

  // Is body ok ?
  req.getValidationResult().then(function (result) {
    result.useFirstErrorOnly();
    if (!result.isEmpty()) { // It isn't => bad request
      return res.status(400).json({
        error: true,
        message: result.array()
      });
    } else { // Body is OK.
      // Input sanitization
      req.body.firstName = sanitizer.escape(req.body.firstName);
      req.body.content = sanitizer.escape(req.body.content);
      req.body.date = sanitizer.escape(req.body.date);
      req.body.isActive = req.body.isActive ? true : false;

      if (req.body.images == undefined)
        req.body.images = [];

      temoignage.firstName = req.body.firstName;
      temoignage.content = req.body.content;
      temoignage.date = req.body.date;
      temoignage.isActive = req.body.isActive;
      temoignage.save(function (err, us) {
        if (err) {
          console.log(err);
          return res.status(500).json({error: true, message: 'Internal server error'});
        }

        us.images = [];

        req.body.images.forEach(t => {
          image.findOne({_id: sanitizer.escape(t)}, (err, image) => {
            if (err || !image)
              return;
            us.images.push(image);
            us.save();
          });
        });

        return res.status(201).json({
          error: false,
          message: ''
        });
      });
    }
  });
});
};

/**
 * DELETE /api/v1/admin/temoignage/:_id
 * delete one temoignage depending of the id in parameters
 */
var deleteTemoignage = function(req, res) {
  temoignage.findOneAndRemove({_id: sanitizer.escape(req.param('_id'))}, (err, temoignage) => {
    if(err) {
      console.log(err);
      return res.status(500).json({error: true, message: 'Internal server error'});
    }

    for (let i = 0; i < temoignage.images.length; i++) {
      image.findOneAndRemove({_id: temoignage.images[i]}, (err, image) => {
        if (fs.existsSync(image.source)) {
          fs.unlinkSync(image.source);
        }
      });
    }

    return res.status(200).json({
      error: false,
      message: ''
    });
});
};

module.exports = {
  create,
  list,
  listAdmin,
  get,
  update,
  deleteTemoignage,
};