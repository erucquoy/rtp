/**
 *			RoadToPermis!
 *	./app/middlewares/AuthMiddlewares.js
 * 	Authentication-related middlewares
 */

var jwt			= require('jsonwebtoken'),
	config 		= require('../config'),
	account		= require('../models/account'),
	util		= require('util'),
	crypto		= require('crypto');


/**
 *	This middleware ensures that incoming webhooks are indeed coming from Stripe @todo sig check
 */
var stripeOnly = function(req, res, next) {
	var ip = req.connection.remoteAddress.replace(/^.*:/, '');
	
	// Time check
	var d 			= new Date(), 
		epoch	 	= Math.round(d.getTime() / 1000),
		timeSince 	= Math.abs(epoch - req.body.created);
	
	if(timeSince > 60*3) {
		console.log("REFUSED - TOO OLD : " + req.body.created + ' - ' + epoch);
		return res.status(403).json({
			error: true, 
			message: 'Accès refusé.'
		});
	}
	
	// IP check
	if(config.stripeIPs.indexOf(ip) === -1) {
		return res.status(403).json({
			error: true, 
			message: 'Accès refusé.'
		});
	}
	
	
	// All good
	return next();
};

/**
 *	This middleware ensures the user is signed-in AND its account is active
 */
var requireActiveAccount = function(req, res, next) {
	var token = req.headers['authorization']; // The access_token should be in the request's headers.

	// Token missing ? 
	if(!token || token === null || token === '' ) {
		return res.status(403).json({
			error: true, 
			message: 'Accès refusé.'
		});
	}

	// Check the JWT's signature
	jwt.verify(token, config.secret, function(err, decoded) {
		if(err) {
			return res.status(403).json({
				error: true, 
				message: 'Accès refusé.',
				err: err
			});		
		}

		// The signature is valid. Check the user nonce against the database to make that the token was not revoked.
		account.findOne({ email: decoded.email }, (err, usr) => {
			// User not found, it was probably deleted since the token was issued. Refuse to proceed.
			if(err || !usr) {
				return res.status(403).json({
					error: true, 
					message: 'Accès refusé.'
				});
			}

			// Nonce check
			if(decoded.usernonce !== usr.userNonce) { // Token was revoked. Refuse.
				return res.status(403).json({
					error: true, 
					message: 'Accès refusé.'
				});
			}

			// Is the account active ? 
			if(!usr.active) {
				return res.status(403).json({
					error: true, 
					message: 'Accès refusé - votre compte n\'est pas activé.'
				});	
			}

			// All is fine at this point. Proceed. @todo add group check
			req.decoded = decoded;
			return next();
		});
	});
}

var requireAdminAccount = function(req, res, next) {
  var token = req.headers['authorization']; // The access_token should be in the request's headers.
  // Token missing ?
  if(!token || token === null || token === '' ) {
    return res.status(403).json({
      error: true,
      message: 'Accès refusé.'
    });
  }

  // Check the JWT's signature
  jwt.verify(token, config.secret, function(err, decoded) {
    if(err) {
      return res.status(403).json({
        error: true,
        message: 'Accès refusé.'
      });
    }

    // The signature is valid. Check the user nonce against the database to make that the token was not revoked.
    account.findOne({ email: decoded.email, admin: true }, (err, usr) => {
      // User not found, it was probably deleted since the token was issued. Refuse to proceed.
      if(err || !usr) {
      return res.status(403).json({
        error: true,
        message: 'Accès refusé.'
      });
    }

    // Nonce check
    if(decoded.usernonce !== usr.userNonce) { // Token was revoked. Refuse.
      return res.status(403).json({
        error: true,
        message: 'Accès refusé.'
      });
    }

    // Is the account active ?
    if(!usr.active) {
      return res.status(403).json({
        error: true,
        message: 'Accès refusé - votre compte n\'est pas activé.'
      });
    }

    // All is fine at this point. Proceed. @todo add group check
    req.decoded = decoded;
    return next();
  });
  });
};


module.exports = {
  requireAdminAccount, requireActiveAccount, stripeOnly
}