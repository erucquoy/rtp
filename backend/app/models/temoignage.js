/**
 *			RoadToPermis!
 *	./app/models/temoignage.js
 * 	Temoignage model
 */

const	mongoose     = require('mongoose'),
      config 		 = require('../config');

var temoignage = new mongoose.Schema ({
  firstName: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  isActive: {
    type: Boolean,
    default: false,
  },
  images: [{ type: mongoose.Schema.Types.ObjectId, ref: 'image' }],
});

module.exports = mongoose.model('temoignage', temoignage);