/**
 *			RoadToPermis!
 *	./app/models/image.js
 * 	Image model
 */

const	mongoose     = require('mongoose'),
  config 		 = require('../config');

var image = new mongoose.Schema ({
  source: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model('image', image);