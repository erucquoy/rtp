/**
 *			RoadToPermis!
 *	./app/models/fiche.js
 * 	fiche model
 */

const	mongoose      = require('mongoose'),
      config 		    = require('../config');

var fiche = new mongoose.Schema ({
  school: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'school'
  },
  student: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'account'
  },
  monitor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'account'
  },
  content: {
    type: String,
  },
  mark: {
    type: Number,
  },
  time: {
    type: Number,
    required: true,
  },
  achievements: [{type: String}],
  toWork: [{type: String}],
  date: {
    type: Date,
    default: Date.now,
    required: true,
  },
});

module.exports = mongoose.model('fiche', fiche);