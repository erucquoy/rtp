/**
 *			RoadToPermis!
 *	./app/models/centreCommentaire.js
 * 	centre commentaire model
 */

const	mongoose      = require('mongoose'),
      config 		    = require('../config');

var centreCommentaire = new mongoose.Schema ({
  centre: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'centre'
  },
  content: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model('centreCommentaire', centreCommentaire);