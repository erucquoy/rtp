/**
 *			RoadToPermis!
 *	./app/models/page.js
 * 	Temoignage model
 */

const	mongoose     = require('mongoose'),
      config 		 = require('../config');

var page = new mongoose.Schema ({
  title: {
    type: String,
    required: true,
    unique: true,
  },
  content: {
    type: String,
    required: true,
  },
  images: [{ type: mongoose.Schema.Types.ObjectId, ref: 'image' }],
});

module.exports = mongoose.model('page', page);