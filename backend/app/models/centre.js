/**
 *			RoadToPermis!
 *	./app/models/centre.js
 * 	Article model
 */

const	mongoose      = require('mongoose'),
  config 		    = require('../config'),
  image         = require('./image');

var centre = new mongoose.Schema ({
  city: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  isActive: {
    type: Boolean,
    default: false,
  },
  images: [{ type: mongoose.Schema.Types.ObjectId, ref: 'image' }],
});

module.exports = mongoose.model('centre', centre);