/**
 *			RoadToPermis!
 *	./app/models/article.js
 * 	Article model
 */

const	mongoose      = require('mongoose'),
      config 		    = require('../config'),
      image         = require('./image');

var article = new mongoose.Schema ({
  title: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
  youtubeVideo: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  isActive: {
    type: Boolean,
    default: false,
  },
  category: {
    type: String,
    required: true,
  },
  typeClient: {
    type: String,
    required: true,
  },
  images: [{ type: mongoose.Schema.Types.ObjectId, ref: 'image' }],
});

module.exports = mongoose.model('article', article);