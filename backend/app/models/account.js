/**
 *			RoadToPermis!
 *	./app/models/account.js
 * 	Account model
 */

const	mongoose     = require('mongoose'),
		bcrypt		 = require('bcryptjs'),
		uuid		 = require('uuid/v4'),
		config 		 = require('../config');

var account = new mongoose.Schema ({
	email: {
		type: String,
		required: true,
		unique: true
	},
  password: {
		type: String,
		required: true
	},
  userNonce: {
		type: String
	},
  profile: {
		firstName: {
			type: String,
			required: true
		},

    lastName: {
			type: String,
			required: true
		}
	},
  active: {
		type: Boolean,
		default: false
	},
  confirmationToken: {
		type: String, 
		default: ''
	},
  resetToken: {
		type: String,
		default: ''
	},
  resetLimitDate: {
		type: String,
		defaul: new Date()
	},
  stripeAcc: {
		type: String,
		default: '', 
	},
  admin: {
    type: Boolean,
    default: false
  },
  drivingSchool: {
    type: Boolean,
    default: false
  },
  monitor: {
    type: Boolean,
    default: false
  },
  school: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'school'
  }
});

account.pre('save', function(next) {
	const user = this;
	if (!user.isModified('password')) return next();
	bcrypt.hash(user.password, config.bcryptrounds, function(err, hash) {
		if(err) throw err;
		user.userNonce = uuid();
		user.password = hash;
		next();
	});
});

account.methods.comparePassword = function(candidatePassword, cb) {  
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) { return cb(err); }
        return cb(null, isMatch);
    });
}

account.methods.updateUUID = function(cb) {
	this.userNonce = uuid();
	this.save((err, user) => {
		if(err) throw err;
		cb();	
	});	
}

account.options.toJSON = {
  transform: function(doc, ret, options) {
    delete ret.password;
    delete ret.userNonce;
    delete ret.confirmationToken;
    delete ret.resetToken;
    delete ret.resetLimitDate;
    delete ret.stripeAcc;
    delete ret.__v;
    return ret;
  }
};

module.exports = mongoose.model('account', account);