/**
 *			RoadToPermis!
 *	./app/models/school.js
 * 	School model
 */

const	mongoose      = require('mongoose'),
  config 		    = require('../config');

var school = new mongoose.Schema ({
  name: {
    type: String,
    required: true,
  },
  isActive: {
    type: Boolean,
    default: true,
  },
});

module.exports = mongoose.model('school', school);