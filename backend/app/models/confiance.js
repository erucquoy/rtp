/**
 *			RoadToPermis!
 *	./app/models/confiance.js
 * 	Temoignage model
 */

const	mongoose     = require('mongoose'),
      config 		 = require('../config');

var confiance = new mongoose.Schema ({
  name: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  isActive: {
    type: Boolean,
    default: false,
  },
  images: [{ type: mongoose.Schema.Types.ObjectId, ref: 'image' }],
});

module.exports = mongoose.model('confiance', confiance);