/**
 *			RoadToPermis!
 *	./app/models/video.js
 * 	Video model
 */

const	mongoose     = require('mongoose'),
  config 		 = require('../config');

var video = new mongoose.Schema ({
  title: {
    type: String,
    required: true,
  },
  youtubeVideo: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  isActive: {
    type: Boolean,
    default: false,
  },
  category: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('video', video);