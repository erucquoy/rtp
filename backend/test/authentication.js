/**
 *			RoadToPermis!
 *	./test/account.js
 * 	Account tests
 */

process.env.NODE_ENV = 'test';

const 	chai 		= require('chai'),
		chaiHttp 	= require('chai-http'),
		server 		= require('../index'),
		should 		= chai.should(),
		account 	= require('../app/models/account'),
		config		= require('../app/config');

var 	userToken 	= '';

chai.use(chaiHttp);

describe('Authentication', () => {
	describe('POST /authenticate', () => {
		before((done) => {
			account.remove({}, done);
		});

		it('it should not let you authenticate with an empty body', (done) => {
			var postBody = {}

			chai.request(server)
				.post('/api/v1/authenticate')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(2);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('email');
					res.body.message[0].should.have.property('msg').eql("L'adresse email ne peut être vide");

					res.body.message[1].should.be.a("object");
					res.body.message[1].should.have.property('param').eql('password');
					res.body.message[1].should.have.property('msg').eql("Le mot de passe ne peut être vide");
	
					done();
				});
		});

		it('it should not let you authenticate without an email address', (done) => {
			var postBody = {
				password: 'password'
			}

			chai.request(server)
				.post('/api/v1/authenticate')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(1);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('email');
					res.body.message[0].should.have.property('msg').eql("L'adresse email ne peut être vide");
	
					done();
				});
		});

		it('it should not let you authenticate without a password', (done) => {
			var postBody = {
				email: 'test@mail.ru'
			};

			chai.request(server)
				.post('/api/v1/authenticate')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(1);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('password');
					res.body.message[0].should.have.property('msg').eql("Le mot de passe ne peut être vide");
	
					// create a debug user for the rest of the tests
					var postBody = {
						email: 'validbuttookbynobodyinthewholeworldlolololol@mail.ru',
						password: 'superpassword',
						firstName: 'first name',
						lastName: 'last name'
					}

					chai.request(server)
						.post('/api/v1/account')
						.send(postBody)
						.end((err, res) => {
							res.should.have.status(201);
							res.body.should.be.a('object');
			
							done();
						});
				});
		});

		it("it should not let you authenticate with an email that don't exist", (done) => {
			var postBody = {
				email: 'validbutdontexist@mail.ru',
				password: 'superpassword'
			}

			chai.request(server)
				.post('/api/v1/authenticate')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(401);
					res.body.should.be.a('object');

					res.body.should.have.property('error').eql(true);
					res.body.should.have.property('message').eql('Les identifiants fournis sont invalides.');
	
					done();
				});
		});

		it('it should not let you authenticate with an invalid password', (done) => {
			var postBody = {
				email: 'validbuttookbynobodyinthewholeworldlolololol@mail.ru',
				password: 'superpasswordbutwrong'
			}

			chai.request(server)
				.post('/api/v1/authenticate')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(401);
					res.body.should.be.a('object');

					res.body.should.have.property('error').eql(true);
					res.body.should.have.property('message').eql('Les identifiants fournis sont invalides.');
	
					done();
				});
		});

		it('it should let you authenticate with a valid password', (done) => {
			var postBody = {
				email: 'validbuttookbynobodyinthewholeworldlolololol@mail.ru',
				password: 'superpassword'
			}

			chai.request(server)
				.post('/api/v1/authenticate')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');

					account.findOne({	email: 'validbuttookbynobodyinthewholeworldlolololol@mail.ru'	}, (err, u) => {
						res.body.should.have.property('access_token');
						res.body.should.have.property('expiry_date');
						res.body.should.have.property('userid').eql(u._id.toString());

						userToken = res.body.access_token;

						done();
					});
	
					
				});
		});
	});

	describe('GET /token', () => {
		it('it should not let you access private routes without being signed-in', (done) => {
			chai.request(server)
				.get('/api/v1/token')
				.end((err, res) => {
					res.should.have.status(403);
					res.body.should.be.a('object');

					res.body.should.have.property('error').eql(true);
					res.body.should.have.property('message').eql('Accès refusé. auth');

					done();
				});
		});
	});
});