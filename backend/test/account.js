/**
 *			RoadToPermis!
 *	./test/account.js
 * 	Account tests
 */

process.env.NODE_ENV = 'test';

const 	chai 		= require('chai'),
		chaiHttp 	= require('chai-http'),
		server 		= require('../index'),
		should 		= chai.should(),
		account 	= require('../app/models/account'),
		config		= require('../app/config');

var		userToken	= "", resettoken = "", userNonce = "";

chai.use(chaiHttp);

var createTempUserForActivation = function(cb) {
	var postBody = {
		email: 'otherothervalidbuttookbynobodyinthewholewor@mail.ru',
		password: 'superpassword',
		firstName: 'first name',
		lastName: 'last name'
	}

	chai.request(server)
		.post('/api/v1/account')
		.send(postBody)
		.end((err, res) => {
			account.findOne({email: 'otherothervalidbuttookbynobodyinthewholewor@mail.ru'}, (err, usr) => {
				if(err) throw err;

				userToken = usr.confirmationToken;
				userNonce = usr.userNonce;
				cb();
			});
		});
}

describe('Account', () => {
	before((done) => {
		account.remove({}, done);
	});

	describe('POST /account', () => {
		it('it should not let you create an account with an empty body', (done) => {
			var postBody = {}

			chai.request(server)
				.post('/api/v1/account')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(4);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('email');
					res.body.message[0].should.have.property('msg').eql("L'adresse email ne peut être vide");

					res.body.message[1].should.be.a("object");
					res.body.message[1].should.have.property('param').eql('password');
					res.body.message[1].should.have.property('msg').eql("Le mot de passe ne peut être vide");

					res.body.message[2].should.be.a("object");
					res.body.message[2].should.have.property('param').eql('firstName');
					res.body.message[2].should.have.property('msg').eql("Le prénom ne peut être vide");

					res.body.message[3].should.be.a("object");
					res.body.message[3].should.have.property('param').eql('lastName');
					res.body.message[3].should.have.property('msg').eql("Le nom de famille ne peut être vide");

					done();
				});
		});

		it('it should not let you create an account without an email', (done) => {
			var postBody = {
				password: 'testpw',
				firstName: 'first name',
				lastName: 'last name'
			}

			chai.request(server)
				.post('/api/v1/account')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(1);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('email');
					res.body.message[0].should.have.property('msg').eql("L'adresse email ne peut être vide");

					done();
				});
		});

		it('it should not let you create an account without a password', (done) => {
			var postBody = {
				email: 'zmlfzemlfhzefhzefz@mail.ru',
				firstName: 'first name',
				lastName: 'last name'
			}

			chai.request(server)
				.post('/api/v1/account')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(1);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('password');
					res.body.message[0].should.have.property('msg').eql("Le mot de passe ne peut être vide");

					done();
				});
		});

		it('it should not let you create an account with a really invalid email', (done) => {
			var postBody = {
				email: 'thatsnotanemail',
				password: 'fefzefzef',
				firstName: 'first name',
				lastName: 'last name'
			}

			chai.request(server)
				.post('/api/v1/account')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(1);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('email');
					res.body.message[0].should.have.property('msg').eql("L'adresse email semble être invalide.");

					done();
				});
		});

		it('it should not let you create an account with an invalid email', (done) => {
			var postBody = {
				email: 'email@provider.tld',
				password: 'fefzefzef',
				firstName: 'first name',
				lastName: 'last name'
			}

			chai.request(server)
				.post('/api/v1/account')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(1);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('email');
					res.body.message[0].should.have.property('msg').eql("L'adresse email semble être invalide.");

					done();
				});
		});

		it('it should not let you create an account with a password that is less than 4 characters', (done) => {
			var postBody = {
				email: 'validbuttookbynobodyinthewholeworldlolololol@mail.ru',
				password: 'fef',
				firstName: 'first name',
				lastName: 'last name'
			}

			chai.request(server)
				.post('/api/v1/account')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(1);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('password');
					res.body.message[0].should.have.property('msg').eql("Le mot de passe doit contenir au moins 4 caractères");

					done();
				});
		});	

		it('it should let you create an account with a valid request', (done) => {
			var postBody = {
				email: 'validbuttookbynobodyinthewholeworldlolololol@mail.ru',
				password: 'superpassword',
				firstName: 'first name',
				lastName: 'last name'
			}

			chai.request(server)
				.post('/api/v1/account')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(201);
					res.body.should.be.a('object');

					done();
				});
		});	

		it('it should not let you create an account with an email that is already taken', (done) => {
			var postBody = {
				email: 'validbuttookbynobodyinthewholeworldlolololol@mail.ru',
				password: 'superpassword',
				firstName: 'first name',
				lastName: 'last name'
			}

			chai.request(server)
				.post('/api/v1/account')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property("error").eql(true);
					res.body.should.have.property("message").eql('Cette adresse email est déjà utilisée');

					done();
				});
		});	

		it('it should create a valid user', (done) => {
			account.findOne({
				email: 'validbuttookbynobodyinthewholeworldlolololol@mail.ru'
			}, (err, user) => {
				should.not.exist(err);

				user.should.be.a('object');
				user.should.have.property('email').eql('validbuttookbynobodyinthewholeworldlolololol@mail.ru');
				user.should.have.property('password').not.eql('superpassword');
				user.should.have.property('active').eql(false);
				user.should.have.property('profile');

				user.profile.should.be.a('object');
				user.profile.should.have.property('firstName').eql('first name');
				user.profile.should.have.property('lastName').eql('last name');

				done();
			});
		});

		it('it should remove all unsafe HTML characters from all possible fields', (done) => {
			var postBody = {
				email: 'othervalidbuttookbynobodyinthewholeworldlolololol@mail.ru',
				password: 'superpassword',
				firstName: '<script>test</script><b><i>',
				lastName: '<script>test</script><b><i>'
			}

			chai.request(server)
				.post('/api/v1/account')
				.send(postBody)
				.end((err, res) => {
					if(err) throw err;

					account.findOne({
						email: 'othervalidbuttookbynobodyinthewholeworldlolololol@mail.ru'
					}, (err, user) => {
						if(err) throw err;

						user.profile.firstName.should.eql('&lt;script&gt;test&lt;/script&gt;&lt;b&gt;&lt;i&gt;');
						user.profile.lastName.should.eql('&lt;script&gt;test&lt;/script&gt;&lt;b&gt;&lt;i&gt;');

						done();
					});
				});
		});
	});

	describe('POST /activate', () => {
		it('it should not let you activate your account with an empty body', (done) => {
			var postBody = {}

			chai.request(server)
				.post('/api/v1/activate')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(2);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('email');
					res.body.message[0].should.have.property('msg').eql("L'adresse email ne peut être vide");

					res.body.message[1].should.be.a("object");
					res.body.message[1].should.have.property('param').eql('token');
					res.body.message[1].should.have.property('msg').eql("Le token d'activation ne peut pas être vide");

					done();
				});
		});

		it('it should not let you activate your account without an email', (done) => {
			var postBody = {
				token: "random token"
			}

			chai.request(server)
				.post('/api/v1/activate')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(1);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('email');
					res.body.message[0].should.have.property('msg').eql("L'adresse email ne peut être vide");

					done();
				});
		});

		it('it should not let you activate your account without a token', (done) => {
			var postBody = {
				email: 'randomemail@mail.ru'
			};

			chai.request(server)
				.post('/api/v1/activate')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error').eql(true);
					res.body.should.have.property('message');

					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(1);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('token');
					res.body.message[0].should.have.property('msg').eql("Le token d'activation ne peut pas être vide");

					done();
				});
		});

		it('it should not let you activate a non-existing account', (done) => {
			var postBody = {
				email: 'randomemail@mail.ru',
				token: 'randomtoken'
			};

			chai.request(server)
				.post('/api/v1/activate')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(401);
					res.body.should.be.a('object');

					res.body.should.have.property('error').eql(true);
					res.body.should.have.property('message').eql("Une erreur s'est produite. Veuillez vérifier l'email et le token.");

					done();
				});
		});

		it('it should not let you activate an account with an invalid token', (done) => {
			// Create a temp user, and grab its activation token
			createTempUserForActivation(() => {
				var postBody = {
					email: 'otherothervalidbuttookbynobodyinthewholewor@mail.ru',
					token: 'invalid.'
				};

				chai.request(server)
					.post('/api/v1/activate')
					.send(postBody)
					.end((err, res) => {
						res.should.have.status(401);
						res.body.should.be.a('object');

						res.body.should.have.property('error').eql(true);
						res.body.should.have.property('message').eql("Une erreur s'est produite. Veuillez vérifier l'email et le token.");
		
						done();
					});
			});
		});

		it('it should let you activate an account with a valid token', (done) => {
			// Create a temp user, and grab its activation token
			createTempUserForActivation(() => {
				var postBody = {
					email: 'otherothervalidbuttookbynobodyinthewholewor@mail.ru',
					token: userToken
				};

				chai.request(server)
					.post('/api/v1/activate')
					.send(postBody)
					.end((err, res) => {
						res.should.have.status(200);
						res.body.should.be.a('object');

						res.body.should.have.property('error').eql(false);
						res.body.should.have.property('message').eql("");
		
						done();
					});
			});
		});
	});

	describe('POST /resetpassword', () => {
		it('it should not let you request a pw reset with an empty body', (done) => {
			var postBody = {}

			chai.request(server)
				.post('/api/v1/resetpassword')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(1);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('email');
					res.body.message[0].should.have.property('msg').eql("L'adresse email ne peut être vide");

					done();
				});
		});

		it('it should pretend to accept a request for an invalid user', (done) => {
			var postBody = {
				email: 'nopnop@dontexist.nope'
			}

			chai.request(server)
				.post('/api/v1/resetpassword')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');

					res.body.should.have.property('error').eql(false);
					res.body.should.have.property('message').eql('ok');

					done();
				});
		});

		it('it should accept a request for a valid user', (done) => {
			var postBody = {
				email: 'otherothervalidbuttookbynobodyinthewholewor@mail.ru'
			}

			account.findOne({	email: 'otherothervalidbuttookbynobodyinthewholewor@mail.ru' 	}, (err, oldUsr) => {
				chai.request(server)
					.post('/api/v1/resetpassword')
					.send(postBody)
					.end((err, res) => {
						res.should.have.status(200);
						res.body.should.be.a('object');

						res.body.should.have.property('error').eql(false);
						res.body.should.have.property('message').eql('ok');

						account.findOne({
							email: 'otherothervalidbuttookbynobodyinthewholewor@mail.ru'
						}, (err, newUsr) => {
							oldUsr.resetToken.should.not.eql(newUsr.resetToken);
							resettoken = newUsr.resetToken;
							done();
						});
					});
			});
		});
	});

	describe('POST /doresetpassword', () => {
		it('it should not let you reset a password with an empty body', (done) => {
			var postBody = {}

			chai.request(server)
				.post('/api/v1/doresetpassword')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(3);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('email');
					res.body.message[0].should.have.property('msg').eql("L'adresse email ne peut être vide");

					res.body.message[1].should.be.a("object");
					res.body.message[1].should.have.property('param').eql('token');
					res.body.message[1].should.have.property('msg').eql("Le token ne peut être vide");

					res.body.message[2].should.be.a("object");
					res.body.message[2].should.have.property('param').eql('newpw');
					res.body.message[2].should.have.property('msg').eql("Le mot de passe ne peut être vide");

					done();
				});
		});

		it('it should not let you reset a password without an email', (done) => {
			var postBody = {
				token: "rfefefefe"
			}

			chai.request(server)
				.post('/api/v1/doresetpassword')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(2);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('email');
					res.body.message[0].should.have.property('msg').eql("L'adresse email ne peut être vide");

					res.body.message[1].should.be.a("object");
					res.body.message[1].should.have.property('param').eql('newpw');
					res.body.message[1].should.have.property('msg').eql("Le mot de passe ne peut être vide");

					done();
				});
		});

		it('it should not let you reset a password without a token', (done) => {
			var postBody = {
				email: 'thatrandomemail@provider.tld'
			};

			chai.request(server)
				.post('/api/v1/doresetpassword')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(2);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('token');
					res.body.message[0].should.have.property('msg').eql("Le token ne peut être vide");

					res.body.message[1].should.be.a("object");
					res.body.message[1].should.have.property('param').eql('newpw');
					res.body.message[1].should.have.property('msg').eql("Le mot de passe ne peut être vide");

					done();
				});
		});

		it('it should not let your reset a password with a weak password', (done) => {
			var postBody = {
				email: 'othezzzdzervalidbuttookbynobodyinthewholewor@mail.ru',
				token: 'resettoken', 
				newpw: 'as'
			};

			chai.request(server)
				.post('/api/v1/doresetpassword')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(1);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('newpw');
					res.body.message[0].should.have.property('msg').eql("Le mot de passe doit contenir au moins 4 caractères");

					done();
				});
		});

		it('it should not let your reset the password of a non-existing account', (done) => {
			var postBody = {
				email: 'random@mail.ru',
				token: 'resettoken', 
				newpw: 'asasas'
			};

			chai.request(server)
				.post('/api/v1/doresetpassword')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error').eql(true);
					res.body.should.have.property('message').eql('Votre demande ne peut être traîtée. Veuillez refaire une requête de réinitialisation de mot de passe. Si le problème persiste, merci de contacter le support.');

					done();
				});
		});

		it('it should not let your reset a password with an invalid token', (done) => {
			var postBody = {
				email: 'otherothervalidbuttookbynobodyinthewholewor@mail.ru',
				token: 'bad-resettoken', 
				newpw: 'asasas'
			};

			chai.request(server)
				.post('/api/v1/doresetpassword')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error').eql(true);
					res.body.should.have.property('message').eql('Votre demande ne peut être traîtée. Veuillez refaire une requête de réinitialisation de mot de passe. Si le problème persiste, merci de contacter le support.');

					done();
				});
		});

		it('it should let you update a password with a valid request', (done) => {
			var postBody = {
				email: 'otherothervalidbuttookbynobodyinthewholewor@mail.ru',
				token: resettoken, 
				newpw: 'asasasas'
			};

			chai.request(server)
				.post('/api/v1/doresetpassword')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');

					res.body.should.have.property('error').eql(false);
					res.body.should.have.property('message').eql('');

					// Check the new PW => should match
					account.findOne({email: 'otherothervalidbuttookbynobodyinthewholewor@mail.ru'}, (err, usr) => {
						usr.comparePassword('asasasas', (err, isMatch) => {
							if(err) throw err;
							isMatch.should.eql(true);

							// Check the old PW => should NOT match
							usr.comparePassword('superpassword', (err, isMatchOld) => {
								if(err) throw err;

								isMatchOld.should.eql(false);
								done(); 
							});
						});
					});

				});
		});

		it('it should not let your reuse a valid token', (done) => {
			var postBody = {
				email: 'otherothervalidbuttookbynobodyinthewholewor@mail.ru',
				token: resettoken, 
				newpw: 'asasasas'
			};

			chai.request(server)
				.post('/api/v1/doresetpassword')
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error').eql(true);
					res.body.should.have.property('message').eql('Votre demande ne peut être traîtée. Veuillez refaire une requête de réinitialisation de mot de passe. Si le problème persiste, merci de contacter le support.');

					done();					

				});
		});

		it('updating your password should reset your userNonce', (done) => {
			account.findOne({ email: 'otherothervalidbuttookbynobodyinthewholewor@mail.ru' }, (err, usr) => {
				if(err) throw err;

				usr.userNonce.should.not.eql(userNonce);
				done();
			});
		});
	});
});