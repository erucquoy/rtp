const 	account 	= require("../app/models/account"),
  school 	= require("../app/models/school"),
  mongoose				  = require('mongoose'),
  config 		 			  = require('../app/config');

mongoose.connect(config.mongouri);

account.findOneAndRemove({email: 'bastien.robert@gmail.com'}, function(err, s){});

var ac = new account({
  email: 'bastien.robert@gmail.com',
  password: 'test',
  profile: {
    firstName: 'bastien',
    lastName: 'robert'
  },
  active: true,
  admin: true,
}).save(function(err, us) {});


var ac2 = new school({
  name: 'Ecole SEPEFREI',
}).save(function (err, us) {
  console.log('school : ', us, 'error : ', err);
  var ac3 = new account({
    email: 'moniteur@gmail.com',
    password: 'test',
    profile: {
      firstName: 'moniteur',
      lastName: 'robert'
    },
    active: true,
    monitor: true,
    school: us['_id'],
  }).save(function(err, us) {
    console.log('moniteur : ', us, 'error : ', err);
  });

  var ac4 = new account({
    email: 'ecole@gmail.com',
    password: 'test',
    profile: {
      firstName: 'ecole',
      lastName: 'robert'
    },
    active: true,
    monitor: true,
    drivingSchool: true,
    school: us._id,
  }).save(function(err, us) {});

  var ac5 = new account({
    email: 'eleve1@gmail.com',
    password: 'test',
    profile: {
      firstName: 'eleve',
      lastName: '1'
    },
    active: true,
    school: us._id,
  }).save(function(err, us) {});

  var ac6 = new account({
    email: 'eleve2@gmail.com',
    password: 'test',
    profile: {
      firstName: 'eleve',
      lastName: '2'
    },
    active: true,
    school: us._id,
  }).save(function(err, us) {});
});