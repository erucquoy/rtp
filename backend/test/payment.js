/**
 *			RoadToPermis!
 *	./test/payment.js
 * 	payment tests
 */

process.env.NODE_ENV = 'test';

const 	chai 		= require('chai'),
		chaiHttp 	= require('chai-http'),
		server 		= require('../index'),
		should 		= chai.should(),
		account 	= require('../app/models/account'),
		config		= require('../app/config'),
		stripe 		= require("stripe")(config.tokens.stripepriv);

chai.use(chaiHttp);

var		debugEmail 	= 'otherothervalidbuttookbynobodyinthewholewor@mail.ru',
		debugSource	= '',
		debugToken	= '';

var createTempUserForActivation = function(cb) {
	var postBody = {
		email: debugEmail,
		password: 'superpassword',
		firstName: 'first name',
		lastName: 'last name'
	}

	chai.request(server)
		.post('/api/v1/account')
		.send(postBody)
		.end((err, res) => {
			account.findOne({email: debugEmail }, (err, usr) => {
				if(err) throw err;

				usr.active = true;
				usr.save(() => {
					chai.request(server)
						.post('/api/v1/authenticate')
						.send(postBody)
						.end((err, res) => {
							debugToken = res.body.access_token;

							cb();
						});
				});
			});
		});
}

describe('payment', () => {
	before((done) => {
		createTempUserForActivation(done);
	});

	describe('POST /api/v1/payment', () => {
		it('it should refuse to set a payment method with an empty body', (done) => {
			var postBody = {}

			chai.request(server)
				.post('/api/v1/payment')
				.set('Authorization', debugToken)
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error');
					res.body.should.have.property('message');

					res.body.error.should.eql(true);
					res.body.message.should.be.a("array");
					res.body.message.length.should.eql(1);

					res.body.message[0].should.be.a("object");
					res.body.message[0].should.have.property('param').eql('source');
					res.body.message[0].should.have.property('msg').eql("La source de paiement ne peut être vide.");

					done();
				});
		});

		it('it should refuse to set a payment method with an invalid token', (done) => {
			var postBody = {
				source: 'randomsource'
			}

			chai.request(server)
				.post('/api/v1/payment')
				.set('Authorization', debugToken)
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(400);
					res.body.should.be.a('object');

					res.body.should.have.property('error').eql(true);
					res.body.should.have.property('message').eql('Impossible de traiter votre requête, votre source de paiement ne peut être reconnue. Merci de réeesayer.');

					done();
				});
		});

		it('it should accept a payment method with a valid request', (done) => {
			// First, let's create a payment source.
			stripe.tokens.create({
				card: {
					number: '4242424242424242',
					cvc: '123',
					exp_month: '10', 
					exp_year: '2018'
				}
			}, (err, source) => {
				if(err) throw err;

				// The actual request.

				var postBody = {
					source: source.id
				}

				chai.request(server)
					.post('/api/v1/payment')
					.set('Authorization', debugToken)
					.send(postBody)
					.end((err, res) => {
						res.should.have.status(200);
						res.body.should.be.a('object');

						res.body.should.have.property('error').eql(false);
						res.body.should.have.property('message').eql('');

						done();
					});
			});
		});

		it('it should remove the payment source with a null source', (done) => {
			var postBody = {
				source: 'null'
			};

			chai.request(server)
				.post('/api/v1/payment')
				.set('Authorization', debugToken)
				.send(postBody)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');

					res.body.should.have.property('error').eql(false);
					res.body.should.have.property('message').eql('');

					account.findOne({email: debugEmail}, (err, usr) => {
						stripe.customers.retrieve(usr.stripeAcc, (err, cus) => {
							cus.sources.data.should.be.a('array');
							cus.sources.data.length.should.eql(0);
							done();
						});
					});					
				});
		});
	});

	describe('GET /api/v1/payment', () => {
		it('it should return nothing if the user doesn\'t have a payment source', (done) => {
			chai.request(server)
				.get('/api/v1/payment')
				.set('Authorization', debugToken)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('object');

					res.body.should.have.property('error').eql(false);
					res.body.should.have.property('message').eql('');
					res.body.should.have.property('payment').eql({});

					done();
				});
		});

		it('it should return the payment details of a user if he has one', (done) => {
			// First, let's create a payment source.
			stripe.tokens.create({
				card: {
					number: '4242424242424242',
					cvc: '123',
					exp_month: '10', 
					exp_year: '2018'
				}
			}, (err, source) => {
				if(err) throw err;

				// The actual request.

				var postBody = {
					source: source.id
				}

				chai.request(server)
					.post('/api/v1/payment')
					.set('Authorization', debugToken)
					.send(postBody)
					.end((err, res) => {
						res.should.have.status(200);
						res.body.should.be.a('object');

						res.body.should.have.property('error').eql(false);
						res.body.should.have.property('message').eql('');

						chai.request(server)
							.get('/api/v1/payment')
							.set('Authorization', debugToken)
							.end((err, res) => {
								res.should.have.status(200);
								res.body.should.be.a('object');

								res.body.should.have.property('error').eql(false);
								res.body.should.have.property('message').eql('');
								res.body.should.have.property('payment');

								res.body.payment.should.be.a('object');
								res.body.payment.should.have.property('brand').eql('Visa');
								res.body.payment.should.have.property('exp_month').eql(10);
								res.body.payment.should.have.property('exp_year').eql(2018);
								res.body.payment.should.have.property('last4').eql('4242');


								done();
							});
					});
			});
		});
	});
});