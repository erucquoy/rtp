/**
 *			RoadToPermis!
 *	./index.js
 * 	Main server file
 */

const 	express 				  = require('express'),
		    APIapp					  = express(),
		    bodyParser			  = require('body-parser'),
        mongoose				  = require('mongoose'),
        morgan					  = require('morgan'),
        fs						    = require('fs'),
        http					    = require('http'),
        config 		 			  = require('./app/config'),
        init 		 			    = require('./app/init'),
        https					    = require('https'),
        expressValidator  = require('express-validator'),
        emailValidator 		= require('mailgun-validate-email')(config.tokens.mailgunpublic),
        apiRouter				  = require('./api-router.js');

// Configuration
var sslCredentials = {
	key:  fs.readFileSync('./ssl/server.key' , 'utf-8'),
	cert: fs.readFileSync('./ssl/server.cert', 'utf-8')
};

// Express middlewares
APIapp.use(bodyParser.json());
APIapp.use(bodyParser.urlencoded({extended: true}));
APIapp.use(expressValidator({
	customValidators: {
		isValidEmail: function(value) {
			return new Promise(function(resolve, reject) {
				emailValidator(value, function(err, result) {
					if(err || result === undefined) return reject();
					if(result.is_valid) return resolve();
					return reject();
				});
            });
		}, minPwLength: function(value) {
			return value !== undefined && value.length >= 4;
		}
	}
}));

if (process.env.NODE_ENV !== 'test')
	APIapp.use(morgan('dev'));

// CORS
APIapp.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', req.get('Origin') || '*');
	res.header('Access-Control-Allow-Credentials', 'true');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Expose-Headers', 'Content-Length');
	res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Cache-Control');
  	return next();
});

// Connect to the database
mongoose.connect(config.mongouri);

init();

// Start da app
APIapp.use('/api/v1', apiRouter);
//var httpsAPIServer = https.createServer(sslCredentials, APIapp);
var httpsAPIServer = http.createServer(APIapp);
httpsAPIServer.listen((process.env.PORT | config.port));
console.log('API Server running and listening on port ' + (process.env.PORT | config.port));

// Export the app for unit testing
module.exports = APIapp;